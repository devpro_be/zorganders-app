<?php
namespace App\Controller;

use App\Entity\ReportItem;
use App\Repository\ReportItemRepository;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\DomCrawler\Crawler;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;

class ReportsController extends Controller
{
    /**
     * @return ReportItemRepository|\Doctrine\Common\Persistence\ObjectRepository
     */
    protected function getRepository()
    {
        return $this
            ->getDoctrine()
            ->getRepository(ReportItem::class);
    }

    /**
     * @param ReportItem[] $reportItems
     * @return JsonResponse
     */
    protected static function returnResponse($reportItems) {
        $response = new JsonResponse($reportItems);

        $response->headers->set('Access-Control-Allow-Origin', '*');

        return $response;
    }

    /**
     * Load the HTML of the report
     *
     * @param integer $id
     * @Route(
     *      "/reports/load/{id}",
     *      requirements={
     *          "id" = "\d+",
     *      },
     *     defaults={
     *          "id" = 1,
     *     }
     * )
     * @return Response
     */
    public function load($id) {

        $response = new Response();

        $reportsItem = $this
            ->getRepository()
            ->find($id);

        if ($reportsItem instanceof ReportItem) {
            // TEST
            $URL = 'http://www.zorganderstv.be/' . $reportsItem->getUrl();
            $content = file_get_contents($URL);
            $crawler = new Crawler($content);

            $report = $crawler->filter('#block-system-main');

            $video = $report->filter('iframe')->attr('src');

            $content = $report->filter('.pane-node-body')->html();

            $response = new JsonResponse(array(
                'video' => 'http:' . $video,
                'content' => $content,
            ));

            $response->headers->set('Access-Control-Allow-Origin', '*');
        }

        return $response;
    }

    /**
     * @param integer $page
     * @param integer $numItems
     * @Route(
     *      "/reports/fetch/{page}/{numItems}",
     *      requirements={
     *          "page" = "\d+",
     *          "numItems" = "\d+"
     *      },
     *      defaults={
     *          "page" = 1,
     *          "numItems" = 20
     *     }
     * )
     * @return JsonResponse
     */
    public function fetch($page, $numItems)
    {
        $firstResult = ($page - 1) * $numItems;

        $query = $this
            ->getRepository()
            ->createQueryBuilder('n')
            ->orderBy('n.id', 'DESC')
            ->setFirstResult($firstResult)
            ->setMaxResults($numItems)
            ->getQuery();

        $reportsItems = $query->getResult();

        return self::returnResponse($reportsItems);
    }

    /**
     * Check if there are newer items than the passed id
     *
     * @param integer $id
     * @Route(
     *      "/reports/update/{id}/{numItems}",
     *      requirements={
     *          "id" = "\d+",
     *          "numItems" = "\d+"
     *      },
     *     defaults={
     *          "id" = 1,
     *          "numItems"=20
     *     }
     * )
     * @return JsonResponse
     */
    public function update($id, $numItems) {
        $query = $this
            ->getRepository()
            ->createQueryBuilder('n')
            ->where('n.id > :id')
            ->setParameter('id', $id)
            ->orderBy('n.id', 'DESC')
            ->setMaxResults($numItems)
            ->getQuery();

        $reportsItems = $query->getResult();

        return self::returnResponse($reportsItems);
    }

    /**
     * Resize the image
     *
     * @param integer $id
     * @param integer $maxWidth
     * @Route(
     *      "/reports/image/{id}/{maxWidth}",
     *      requirements={
     *          "id" = "\d+",
     *          "maxWidth" = "\d+"
     *      },
     *     defaults={
     *          "id" = 1,
     *          "maxWidth"=400
     *     }
     * )
     * @return Response
     */
    public function image($id, $maxWidth) {

        $response = new Response();

        $reportsItem = $this
            ->getRepository()
            ->find($id);

        if ($reportsItem instanceof ReportItem) {
            /**  $cacheDir = $this->container->getParameter('zorganderstv.imgDirReport');
            $fileExtension = self::getImgExtension($reportsItem->getImage());
            $fileName = sprintf('%s/%08d_%s.%s', $cacheDir, $id, $maxWidth, $fileExtension);

            $response->headers->set('Content-type', self::getContentType($fileExtension));

            if (!file_exists($fileName)) {
                $imageUrl = sprintf('%s', $reportsItem->getImage());

                list($width, $height) = getimagesize($imageUrl);

                $ratio = $width/$height; // width/height

                $new_width = $ratio > 1 ? $maxWidth : $maxWidth * $ratio;
                $new_height= $ratio < 1 ? $maxWidth : $maxWidth / $ratio;

                $image_p = imagecreatetruecolor($new_width, $new_height);
                list($image, $imgType) = self::imagecreatefromfile($imageUrl);
                imagecopyresampled($image_p, $image, 0, 0, 0, 0, $new_width, $new_height, $width, $height);

                switch ($imgType) {
                    case 'image/jpeg':
                        imagejpeg($image_p, $fileName, 100);
                        break;
                    case 'image/png':
                        imagepng($image_p, $fileName, 9);
                        break;
                    case 'image/gif':
                        imagegif($image_p, $fileName);
                        break;
                }

                // Free up memory
                imagedestroy($image_p);
            } */

            $response->setContent(file_get_contents($reportsItem->getImage()));

            // $response->setContent(file_get_contents($fileName));
            $response->setMaxAge(31556926);

        }

        return $response;
    }

    /**
     * @param string $imgExtension
     * @return string
     */
    protected static function getContentType($imgExtension) {
        return sprintf('image/%s', $imgExtension);
    }

    /**
     * @param string $filename
     * @return string
     */
    protected static function getImgExtension($filename) {
        return strtolower( pathinfo( $filename, PATHINFO_EXTENSION ));
    }

    /**
     * @param string $url
     * @return bool
     */
    protected static function remoteFileExists($url) {
        $headers=get_headers($url);
        return stripos($headers[0],"200 OK")?true:false;
    }


    protected static function imagecreatefromfile( $filename ) {
        if (!self::remoteFileExists($filename)) {
            throw new \InvalidArgumentException('File "'.$filename.'" not found.');
        }
        switch ( self::getImgExtension($filename) ) {
            case 'jpeg':
            case 'jpg':
                return array(imagecreatefromjpeg($filename), 'image/jpeg');
                break;

            case 'png':
                return array(imagecreatefrompng($filename), 'image/png');
                break;

            case 'gif':
                return array(imagecreatefromgif($filename), 'image/gif');
                break;

            default:
                throw new \InvalidArgumentException('File "'.$filename.'" is not valid jpg, png or gif image.');
                break;
        }
    }

}