<?php
namespace App\Controller;

use App\Firebase\ArticleCategoriesFirebase;
use App\Firebase\ArticleRootCategoriesFirebase;
use App\Firebase\ArticlesFirebase;
use App\Google\Firestore\AbstractFirestore;
use App\Google\Firestore\ArticleCategoriesFirestore;
use App\Google\Firestore\ArticlesFirestore;
use App\Model\ArticleCategoryModel;
use App\Model\ArticleModel;
use App\Model\ArticleRootCategoryModel;
use App\Scraper\ActualCare\ArticleActualCareScraper;
use App\Scraper\ActualCare\ArticleCategoryActualCareScraper;
use App\Scraper\ArticleScraper;
use App\Service\ScrapeCategory;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;

use App\Entity\NewsItem;
use App\Repository\NewsItemRepository;

use Google\Cloud\Storage\StorageClient;

class IndexController extends Controller
{

    /**
     * @return NewsItemRepository|\Doctrine\Common\Persistence\ObjectRepository
     */
    protected function getRepository()
    {
        return $this
            ->getDoctrine()
            ->getRepository(NewsItem::class);
    }

    /**
     * @Route(
     *      "/"
     * )
     */
    public function index()
    {
        return $this->render(
            'app/index.html.twig',
            [
                'title' => 'ZorgAnders App',
            ]
        );
    }

    /**
     * @Route(
     *     "/scrape"
     * )
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function scrape()
    {
        return $this->render(
            'scrape.html.twig'
        );
    }

    /**
     * @Route(
     *      "/test"
     * )
     */
    public function test()
    {

        $firestore = new ArticlesFirestore();
        $articles = $firestore->getCollectionRef()->limit(10)->documents();

        foreach ($articles as $article) {
            $articleId = AbstractFirestore::getDocumentId($article);

            $articleModel = new ArticleModel($articleId);

            ArticleActualCareScraper::getArticleImage($articleModel);

        }


        exit();



        /**$pageUrl = 'http://staging.zorganders.be/category/nl/nl-management/nl-management-ict/page/%s/';
 *
*
*
* $url = sprintf($pageUrl, 0);
        * $numberOfPages = ArticleScraper::getNumberOfPagesForUrl($url);
 *
* for ($i=15; $i <= $numberOfPages; $i++) {
            * $url = sprintf($pageUrl, $i);
            * $articles = ArticleScraper::getArticlesOnPage($url);
 *
* /**
             * @var ArticleCategoryModel $articleModel

            foreach ($articles as $articleModel) {
                * $articleFirebase->storeIntoDatabase($articleModel);
            * }
        * }
 *
* $articleFirebase = new ArticlesFirebase();
 *
* $articleModel = $articleFirebase->getFromDatabase('10-belgische-zorginstellingen');
 *
*
* var_dump($articleModel->toSet());
 *
* //       $articleModel = ArticleScraper::getArticleDetails($articleModel);
 *
* $articleFirebase->storeIntoDatabase($articleModel);
 *
* echo "Done";
 *
* /**$articleBucket = new ArticlesFirebase();
 *
* $articleBucket->writeArticle('test2', '<p>It works</p>');
 *
* echo "hoera!";
        */
        exit();
    }

    /**
    * @Route(
    *      "/scrape-test"
    * )
    */
    public function scrapeTest()
    {
        $content = ScrapeCategory::getContent();
        exit();
    }
}