<?php

namespace App\Helper;

use Symfony\Component\DomCrawler\Crawler;
use App\Entity\NewsItem;

class NewsCrawlerHelper {

    /**
     * @param Crawler $crawler
     * @return NewsItem[]
     */
    static public function crawlNewsItems(Crawler $crawler) {
        $newsItems = array();
        $viewRows = self::crawlViewRows($crawler);
        foreach ($viewRows as $viewRow) {
            $newsItems[] = NewsItemCrawlerHelper::getItem($viewRow);
        }
        return $newsItems;
    }


    static protected function crawlViewRows(Crawler $crawler) {
        $viewRows = [];
        $crawler
            ->filter('.views-row')
            ->each(
                function (Crawler $node, $i) use (&$viewRows) {
                    array_push($viewRows, $node);
                }
            );
        return $viewRows;
    }


}