<?php

namespace App\Helper;

use App\Entity\ReportItem;
use Symfony\Component\DomCrawler\Crawler;

class ReportsCrawlerHelper {

    /**
     * @param Crawler $crawler
     * @return ReportItem[]
     */
    static public function crawlReportsItems(Crawler $crawler) {
        $reportItems = array();
        $viewRows = self::crawlViewRows($crawler);
        foreach ($viewRows as $viewRow) {
            $reportItems[] = ReportsItemCrawlerHelper::getItem($viewRow);
        }
        return $reportItems;
    }


    static protected function crawlViewRows(Crawler $crawler) {
        $viewRows = [];
        $crawler
            ->filter('.views-row')
            ->each(
                function (Crawler $node, $i) use (&$viewRows) {
                    array_push($viewRows, $node);
                }
            );
        return $viewRows;
    }


}