<?php
namespace App\Helper;

use App\Entity\ReportItem;
use Symfony\Component\DomCrawler\Crawler;
use DateTime;

/**
 * Class ReportsItemCrawlerHelper
 * @package App\Helper
 */
class ReportsItemCrawlerHelper {

    /**
     * @param Crawler $crawler
     * @return ReportItem
     */
    static public function getItem(Crawler $crawler) {
        $reportsItem = new ReportItem();
        $reportsItem->setUrl(self::getUrl($crawler));
        $reportsItem->setTitle(self::getTitle($crawler));
        $reportsItem->setImage(self::getImg($crawler));
        $reportsItem->setSummary(self::getSummary($crawler));
        $reportsItem->setDate(self::getDate($crawler));
        return $reportsItem;
    }

    /**
     * @param Crawler $crawler
     * @return string
     */
    static protected function getTitle(Crawler $crawler) {
        $rawTitle = $crawler
            ->filter('.views-field-title')
            ->text();
        return trim($rawTitle);
    }

    /**
     * @param Crawler $crawler
     * @return string
     */
    static protected function getUrl(Crawler $crawler) {
        $rawUrl = $crawler
            ->filter('.views-field-title > a')
            ->attr('href');
        return trim($rawUrl);
    }

    /**
     * @param Crawler $crawler
     * @return DateTime
     */
    static protected function getDate(Crawler $crawler) {
        $rawDate = '';
        $dateDiv = $crawler
                ->filter('.date-display-single');
        if ($dateDiv->count()) {
            $rawDate = $dateDiv
                ->text();
        }

        return strlen($rawDate) ? DateTime::createFromFormat('d/m/y', trim($rawDate)) : new DateTime();
    }

    /**
     * @param Crawler $crawler
     * @return mixed
     */
    static protected function getImg(Crawler $crawler) {
        $rawImg = '';
        $imgDiv = $crawler
            ->filter('.image > a > img');
        if ($imgDiv->count()) {
            $rawImg = $imgDiv->attr('src');
            $rawImg = preg_replace ("/[\s]/", "%20", $rawImg);
        }

        return $rawImg;
    }

    /**
     * @param Crawler $crawler
     * @return string
     */
    static protected function getSummary(Crawler $crawler) {
        $rawSummary = '';
        $summaryDiv = $crawler->filter('.datebody > p');
        if ($summaryDiv->count()) {
            $rawSummary = $summaryDiv->text();
        }
        return trim($rawSummary);
    }
}