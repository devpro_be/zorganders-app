<?php
namespace App\Helper;

use App\Entity\NewsItem;
use Symfony\Component\DomCrawler\Crawler;
use DateTime;

/**
 * Class NewsItemCrawlerHelper
 * @package App\Helper
 */
class NewsItemCrawlerHelper {

    /**
     * @param Crawler $crawler
     * @return NewsItem
     */
    static public function getItem(Crawler $crawler) {
        $newsItem = new NewsItem();
        $newsItem->setUrl(self::getUrl($crawler));
        $newsItem->setTitle(self::getTitle($crawler));
        $newsItem->setImage(self::getImg($crawler));
        $newsItem->setSummary(self::getSummary($crawler));
        $newsItem->setDate(self::getDate($crawler));
        return $newsItem;
    }

    /**
     * @param Crawler $crawler
     * @return string
     */
    static protected function getTitle(Crawler $crawler) {
        $rawTitle = $crawler
            ->filter('.views-field-title')
            ->text();
        return trim($rawTitle);
    }

    /**
     * @param Crawler $crawler
     * @return string
     */
    static protected function getUrl(Crawler $crawler) {
        $rawUrl = $crawler
            ->filter('.views-field-title > a')
            ->attr('href');
        return trim($rawUrl);
    }

    /**
     * @param Crawler $crawler
     * @return DateTime
     */
    static protected function getDate(Crawler $crawler) {
        $rawDate = '';
        $dateDiv = $crawler
                ->filter('.date-display-single');
        if ($dateDiv->count()) {
            $rawDate = $dateDiv
                ->text();
        }

        return strlen($rawDate) ? DateTime::createFromFormat('d/m/y', trim($rawDate)) : new DateTime();
    }

    /**
     * @param Crawler $crawler
     * @return mixed
     */
    static protected function getImg(Crawler $crawler) {
        $rawImg = '';
        $imgDiv = $crawler
            ->filter('.image > a > img');
        if ($imgDiv->count()) {
            $rawImg = $imgDiv->attr('src');
            $rawImg = preg_replace ("/[\s]/", "%20", $rawImg);
        }

        return $rawImg;
    }

    /**
     * @param Crawler $crawler
     * @return string
     */
    static protected function getSummary(Crawler $crawler) {
        $rawSummary = '';
        $summaryDiv = $crawler->filter('.datebody > p');
        if ($summaryDiv->count()) {
            $rawSummary = $summaryDiv->text();
        }
        return trim($rawSummary);
    }
}