<?php

namespace App\Scraper\ZorgAndersTv;

use App\Firebase\ReportsFirebase;
use App\Google\Firestore\ReportCategoriesFirestore;
use App\Model\ReportCategoryModel;
use App\Model\ReportModel;
use App\Scraper\AbstractScraper;
use Symfony\Component\DomCrawler\Crawler;
use Vimeo\Vimeo;

class ReportZorgAndersTvScraper extends AbstractScraper
{

    const BASE_URL = 'https://www.zorganderstv.be';

    const HREF_REPORTAGES = '/reportages/';
    const HREF_NODE = '/node/';

    /**
     * @param ReportCategoryModel $categoryModel
     * @param int $page
     * @return Crawler
     */
    static private function getReportsPage(ReportCategoryModel $categoryModel,int $page = 0): Crawler
    {
        $formatUrl = self::BASE_URL . $categoryModel->getUrl();
        $url = sprintf($formatUrl, $page);

        $crawler = self::getPageCrawler($url);

        return $crawler;
    }

    /**
     * @param ReportModel $reportModel
     * @return Crawler
     */
    static private function getReportPage(ReportModel $reportModel): Crawler
    {
        $url = self::BASE_URL . $reportModel->getType() . $reportModel->getId();
        $crawler = self::getPageCrawler($url);

        return $crawler;
    }

    /**
     * @param ReportCategoryModel $categoryModel
     * @return int
     */
    static public function getNumberOfPagesForCategory(ReportCategoryModel $categoryModel):int
    {
        $pageCrawler = self::getReportsPage($categoryModel);

        $numberOfPages = 0;

        $pagerLastCrawler = $pageCrawler
            ->filterXPath('//li[@class="pager-last"]');

        if ($pagerLastCrawler->count()) {
            $link = $pagerLastCrawler
                ->first()
                ->filter('a')
                ->first()
                ->attr('href');

            $linkParts = explode("=", $link);

            if (count($linkParts) === 2) {
                $numberOfPages = (int) $linkParts[1];
            }
        }

        return $numberOfPages;
    }

    /**
     * @param ReportCategoryModel $categoryModel
     * @param int $page
     * @throws \Exception
     * @return array
     */
    static public function getReportsForCategoryOnPage(ReportCategoryModel $categoryModel, int $page = 0): array
    {
        $pageCrawler = self::getReportsPage($categoryModel, $page);

        $containerCrawler = $pageCrawler
            ->filterXPath('//div[@id="quicktabs-tabpage-reportages_taxonomy-0"]');

        $previousDate = '';

        if ($containerCrawler->count()) {
            $reportModels = $containerCrawler
                ->first()
                ->filterXPath('//div[contains(@class,"views-row")]')
                ->each(function(Crawler $crawlerViewsRow) use ($categoryModel, &$previousDate) {
                   $href = $crawlerViewsRow
                       ->filter('a')
                       ->first()
                       ->attr('href');

                   $type = strpos($href, ReportModel::TYPE_REPORTAGES) !== false ? ReportModel::TYPE_REPORTAGES : ReportModel::TYPE_NODE;

                   $id = str_replace($type, '', $href);

                   $title = $crawlerViewsRow
                       ->filterXPath('//div[contains(@class,"views-field-title")]')
                       ->first()
                       ->text();

                   $title = trim($title);

                   $date = self::getDate($crawlerViewsRow, $previousDate);
                   if (strlen($date)) {
                       $previousDate = $date;
                   }

                   $reportModel = new ReportModel($id);

                   $reportModel
                       ->setTitle($title)
                       ->setDate($date)
                       ->setSource(ReportCategoriesFirestore::SOURCE_ZORGANDERS)
                       ->setType($type)
                       ->setCategory($categoryModel->getId());

                   return $reportModel;
                });
        } else {
            throw new \Exception('Con quicktabs container found for category.');
        }

        return $reportModels;
    }

    /**
     * @param Crawler $crawler
     * @param string $defaultDate
     * @return string
     */
    static private function getDate(Crawler $crawler, string $defaultDate): string
    {
        $date = $defaultDate;
        $dateCrawler = $crawler
            ->filterXPath('//span[@class="date-display-single"]');

        if ($dateCrawler->count()) {
            $date = $dateCrawler
                ->first()
                ->text();
            $date = \DateTime::createFromFormat('d/m/y H:i:s', $date . ' 00:00:00')
                    ->getTimestamp() * 1000;
        }
        return $date;
    }

    /**
     * @param ReportModel $reportModel
     * @return ReportModel
     */
    static public function getReportDetails(ReportModel $reportModel): ReportModel
    {
        $reportPageContent = self::getReportPage($reportModel);

        $reportsFirebase = new ReportsFirebase();

        $reportModel = self::scrapeVideo($reportPageContent, $reportModel);
        $reportModel = self::scrapeBody($reportPageContent, $reportModel, $reportsFirebase);
        $reportModel = self::scrapeImage($reportModel, $reportsFirebase);

        return $reportModel;
    }

    /**
     * @param Crawler $reportPageCrawler
     * @param ReportModel $reportModel
     * @return ReportModel
     */
    private static function scrapeVideo(Crawler $reportPageCrawler, ReportModel $reportModel): ReportModel {

        if (!$reportModel->getVideo()) {
            try {
                $playerCrawler = $reportPageCrawler
                    ->filterXPath('//div[contains(@class,"field-name-field-vimeo-video")]');

                if (!$playerCrawler->count()) {
                    throw new \Exception('No video container found on report page.');
                }

                $iframeCrawler = $playerCrawler
                    ->first()
                    ->filter('iframe');

                if (!$iframeCrawler->count()) {
                    throw new \Exception('No video iframe found inside the video container.');
                }

                $src = $iframeCrawler
                    ->first()
                    ->attr('src');


                preg_match('/\/\/player.vimeo.com\/video\/([0-9]+)\?/', $src, $matches);
                $id = (count($matches) === 2) ? $matches[1] : null;
                $reportModel->setVideo($id);

            } catch (\Exception $e) {
                $reportModel->setVideo(null);
            }
        }

        return $reportModel;
    }

    private static function scrapeBody(Crawler $reportPageCrawler, ReportModel $reportModel, ReportsFirebase $reportsFirebase): ReportModel
    {
        if (!$reportModel->getHasBody()) {

            $bodyContent = null;

            try {
                $bodyCrawler = $reportPageCrawler
                    ->filterXPath('//div[contains(@class,"field-type-text-with-summary")]');

                if (!$bodyCrawler->count()) {
                    throw new \Exception('text with summary contianer not found on report page.');
                }

                $bodyContent = $bodyCrawler
                    ->first()
                    ->html();

                $reportsFirebase
                    ->storeIntoBucket($reportModel->getId(), $bodyContent);

                $reportModel->setHasBody(true);

            } catch (\Exception $e) {
                $reportModel->setHasBody(false);
            }
        }

        return $reportModel;
    }

    /**
     * @param ReportModel $reportModel
     * @param ReportsFirebase $reportsFirebase
     * @return ReportModel
     */
    private static function scrapeImage(ReportModel $reportModel, ReportsFirebase $reportsFirebase): ReportModel
    {
        if (!$reportModel->getHasImage() && $reportModel->getVideo()) {
            try {

                $requestUrlFormat = '/videos/%s/pictures';
                $requestUrl = sprintf($requestUrlFormat, $reportModel->getVideo());

                $vimeo = new Vimeo(
                    '57c6db051ad32bfac8f1f998c4abcee22920d58f',
                    'm1cVR82J8TjA8UbWJzBB83/csofVuOF/q1514xjivX2ffa5tNdy8M3MFS10kz47tecY9Kj8AW0VaB/uk76LkGBHHSLrtuQSVDTI76+1WTGW2rZtOOTxgPwshLIuya4DR',
                    'a807677e0cc69575a52f2dbc56b045e8'
                );

                $data = $vimeo->request($requestUrl);

                $uri = self::getPath($data, 'body.data.0.uri', false);

                if (!$uri) {
                    var_dump($data);
                    throw new \Exception('URI not found in return value');
                }

                $parts = explode('/', $uri);
                $id = array_pop($parts);
                $imageUrlFormat = 'https://i.vimeocdn.com/video/%s_140x140.jpg';
                $imageUrl = sprintf($imageUrlFormat, $id);
                $image = imagecreatefromjpeg($imageUrl);
                $reportsFirebase->storeImage($reportModel->getId(), $image);

                $reportModel->setHasImage(true);
            } catch (\Exception $e) {
                var_dump($e->getMessage());
                $reportModel->setHasImage(false);
            }
        }
        return $reportModel;
    }

    /**
     * @param array $arr
     * @param string $path
     * @param null $defaultValue
     * @return array|mixed|null
     */
    static private function getPath(array $arr, string $path, $defaultValue = null) {
        $pathParts = explode('.', $path);
        try {
            foreach ($pathParts as $pathPart) {
                if (is_array($arr) && array_key_exists($pathPart, $arr)) {
                    $arr = $arr[$pathPart];
                } else {
                    throw new \Exception('Path not found in array');
                }
            }
            $value = $arr;
        } catch(\Exception $e) {
            $value = $defaultValue;
        }

        return $value;
    }
}