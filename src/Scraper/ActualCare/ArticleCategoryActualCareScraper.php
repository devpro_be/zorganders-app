<?php
namespace App\Scraper\ActualCare;

use App\Google\Firestore\ArticlesFirestore;
use App\Model\ArticleCategoryModel;
use App\Model\ArticleModel;
use Symfony\Component\DomCrawler\Crawler;

class ArticleCategoryActualCareScraper extends AbstractActualCareScraper {

    /**
     * @param ArticleCategoryModel $categoryModel
     * @param int $page
     * @return Crawler
     */
    protected static function getCategoryPageCrawler(ArticleCategoryModel $categoryModel, int $page = 1):Crawler {
        $url = sprintf($categoryModel->getUrl(), $page);
        return self::getPageCrawler($url);
    }

    /**
     * @param ArticleCategoryModel $categoryModel
     * @return int
     */
    public static function getNumberOfPagesForCategory(ArticleCategoryModel $categoryModel):int {

        $crawler = self::getCategoryPageCrawler($categoryModel);

        $numberCrawler = $crawler
            ->filterXPath('//ul[@class="pagination"]')
            ->first()
            ->filterXPath('//a[@class="page-numbers"]');

        $numberOfPages = $numberCrawler->count() ? intval($numberCrawler->last()->text()) : 0;

        return $numberOfPages;
    }

    /**
     * @param Crawler $articlesCrawler
     * @param ArticleCategoryModel $categoryModel
     * @return ArticleModel[]
     */
    protected static function crawlArticles(Crawler $articlesCrawler, ArticleCategoryModel $categoryModel):array {
        return $articlesCrawler->each(
            function(Crawler $articleCrawler) use ($categoryModel):ArticleModel{

                $titleElement = $articleCrawler
                    ->filterXPath('//h2[@class="post-title"]')
                    ->first();

                $articleUrl = $titleElement
                    ->filterXPath('//a')
                    ->first()
                    ->attr('href');

                $title = $titleElement
                    ->text();

                $date = $articleCrawler
                    ->filterXPath('//time[contains(@class, "entry-date")]')
                    ->first()
                    ->text();

                $date = \DateTime::createFromFormat('F j, Y H:i:s', $date . ' 00:00:00')
                    ->getTimestamp() * 1000;

                $urlParts = explode('/', $articleUrl);

                $articleId = $urlParts[count($urlParts)-2];

                $articlesFirestore = new ArticlesFirestore();

                /** @var ArticleModel $articleModel */
                $articleModel = $articlesFirestore->getById($articleId);

                $articleModel
                    ->setSource('ACTUALCARE')
                    ->setTitle($title)
                    ->setDate($date)
                    ->setCategory($categoryModel->getId());

                return $articleModel;
            }
        );
    }

    /**
     * @param ArticleCategoryModel $categoryModel
     * @param int $page
     * @return array
     */
    public static function getArticlesForCategoryOnPage(ArticleCategoryModel $categoryModel, int $page):array {

        $crawler = self::getCategoryPageCrawler($categoryModel, $page);

        $articlesCrawler = $crawler
            ->filterXPath('//main')
            ->first()
            ->filterXPath('//article');

        $articles = $articlesCrawler->count() ? self::crawlArticles($articlesCrawler, $categoryModel) : [];

        return $articles;
    }
}