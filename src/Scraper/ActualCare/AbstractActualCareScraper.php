<?php

namespace App\Scraper\ActualCare;

use Symfony\Component\DomCrawler\Crawler;
use App\Scraper\AbstractScraper;

class AbstractActualCareScraper extends AbstractScraper {

    const SCRAPE_DOMAIN = 'https://www.actualcare.be';

    /**
     * @param $url
     * @param array|null $options
     * @return Crawler
     */
    protected static function getPageCrawler($url, array $options = null) {
        $content = file_get_contents(self::SCRAPE_DOMAIN . $url);
        return new Crawler($content);
    }

}