<?php

namespace App\Scraper\ActualCare;

use App\Firebase\ArticlesFirebase;
use App\Model\ArticleModel;
use Google\Cloud\Storage\StorageObject;
use Symfony\Component\DomCrawler\Crawler;

/**
 * Class ArticleActualCareScraper
 * @package App\Scraper\ActualCare
 */
class ArticleActualCareScraper extends AbstractActualCareScraper
{
    /**
     * @param ArticleModel $articleModel
     * @return string
     */
    public static function getArticleBody(ArticleModel $articleModel): string
    {
        $url = '/nl/' . $articleModel->getId();
        $crawler = self::getPageCrawler($url);

        $pageContent = $crawler
            ->filterXPath("//div[@class='post-content']")
            ->first();

        $pageContent
            ->filterXPath("//div[contains(@class,'addtoany_share_save_container')]")
            ->each(
                function (Crawler $crawler) {
                    foreach ($crawler as $node) {
                        $node->parentNode->removeChild($node);
                    }
                }
            );

        $pageContent = trim($pageContent->html());

        return $pageContent;
    }

    public static function scrapeArticleImage(ArticleModel $articleModel)
    {
        $imgSrc = false;
        $url = '/nl/' . $articleModel->getId();

        $crawler = self::getPageCrawler($url);


        $articleCrawler = $crawler
            ->filterXPath("//article[contains(@class,'post')]")
            ->first();

        if ($articleCrawler->count()) {
            $postThumbnail = $articleCrawler
                ->filterXPath("//div[contains(@class,'post-thumbnail')]")
                ->first();

            if ($postThumbnail->count()) {
                $imgSrcCrawler = $postThumbnail
                    ->filter('img')
                    ->first();

                if ($imgSrcCrawler->count()) {
                    $imgSrc = $imgSrcCrawler
                        ->attr('src');
                }
            }
        }


        $firebase = new ArticlesFirebase();

        if ($imgSrc) {

            switch (exif_imagetype($imgSrc)) {
                case IMAGETYPE_JPEG:
                    $resource = imagecreatefromjpeg($imgSrc);
                    break;
                case IMAGETYPE_PNG:
                    $resource = imagecreatefrompng($imgSrc);
                    $bg = imagecreatetruecolor(imagesx($resource), imagesy($resource));
                    imagefill($bg, 0, 0, imagecolorallocate($bg, 255, 255, 255));
                    imagealphablending($bg, TRUE);
                    imagecopy($bg, $resource, 0, 0, 0, 0, imagesx($resource), imagesy($resource));
                    imagedestroy($resource);
                    $resource = $bg;
                    break;
                default:
                    $resource = false;
            }

            if ($resource) {
                self::createThumbnail($firebase, $articleModel, $imgSrc, $resource);
                self::createLarge($firebase, $articleModel, $imgSrc, $resource);
                $articleModel->setHasImage(true);
                imagedestroy($resource);
            } else {
                $articleModel->setHasImage(false);
            }
        } else {
            $articleModel->setHasImage(false);
        }
    }

    /**
     * @param ArticlesFirebase $firebase
     * @param ArticleModel $articleModel
     * @param string $imgSrc
     * @param resource $resource
     * @return StorageObject
     */
    private static function createThumbnail(ArticlesFirebase $firebase, ArticleModel $articleModel, string $imgSrc, $resource): StorageObject
    {
        $max_height = 140;
        $max_width = 140;

        list($width, $height) = getimagesize($imgSrc);

        $dst_img = imagecreatetruecolor($max_width, $max_height);

        $width_new = $height * $max_width / $max_height;
        $height_new = $width * $max_height / $max_width;
        //if the new width is greater than the actual width of the image, then the height is too large and the rest cut off, or vice versa
        if ($width_new > $width) {
            //cut point by height
            $h_point = (($height - $height_new) / 2);
            //copy image
            imagecopyresampled($dst_img, $resource, 0, 0, 0, $h_point, $max_width, $max_height, $width, $height_new);
        } else {
            //cut point by width
            $w_point = (($width - $width_new) / 2);
            imagecopyresampled($dst_img, $resource, 0, 0, $w_point, 0, $max_width, $max_height, $width_new, $height);
        }

        return $firebase->storeImage($articleModel->getId() . '__thumbnail', $dst_img);
    }

    /**
     * @param ArticlesFirebase $firebase
     * @param ArticleModel $articleModel
     * @param string $imgSrc
     * @param resource $resource
     */
    private static function createLarge(ArticlesFirebase $firebase, ArticleModel $articleModel, string $imgSrc, $resource)
    {
        $max_height = 628;
        $max_width = 1200;

        list($width, $height) = getimagesize($imgSrc);

        $dst_img = imagecreatetruecolor($max_width, $max_height);

        $width_new = $height * $max_width / $max_height;
        $height_new = $width * $max_height / $max_width;
        //if the new width is greater than the actual width of the image, then the height is too large and the rest cut off, or vice versa
        if ($width_new > $width) {
            //cut point by height
            $h_point = (($height - $height_new) / 2);
            //copy image
            imagecopyresampled($dst_img, $resource, 0, 0, 0, $h_point, $max_width, $max_height, $width, $height_new);
        } else {
            //cut point by width
            $w_point = (($width - $width_new) / 2);
            imagecopyresampled($dst_img, $resource, 0, 0, $w_point, 0, $max_width, $max_height, $width_new, $height);
        }

        $firebase->storeImage($articleModel->getId() . '__large', $dst_img);
    }
}