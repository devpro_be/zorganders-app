<?php

namespace App\Scraper;


use App\Model\ArticleCategoryModel;

class ArticleCategoryScraper extends AbstractScraper {

    const DOMAIN = 'http://staging.zorganders.be';



    protected $categories = [];


    public function __construct()
    {
        $this->categories = [
            new ArticleCategoryModel(
                'ICT',
                '',
                self::DOMAIN . '/category/nl/nl-management/nl-management-ict/page/%s/'
            ),
        ];
    }
}