<?php

namespace App\Scraper;

use App\Model\ArticleCategoryModel;
use Symfony\Component\DomCrawler\Crawler;

class ArticleScraper extends AbstractScraper {

    const SELECTOR_ARTICLE_ELEMENT = '.post';
    const SELECTOR_ARTICLE_ELEMENT_IMAGE = '.wp-post-image';
    const SELECTOR_ARTICLE_ELEMENT_TITLE = '.tt-post-title';

    /**
     * @param string $pageUrl
     * @return Crawler
     */
    private static function filterArticlesOnPage($pageUrl) {
        $pageScraper = self::getPageCrawler($pageUrl);
        return $pageScraper->filter(self::SELECTOR_ARTICLE_ELEMENT);
    }

    private static function getIdFromUrl($articleUrl) {
        $id = substr($articleUrl, strlen('http://staging.zorganders.be/'), -1);
        return $id;
    }

    /**
     * @param Crawler $article
     * @return null|string
     */
    private static function getImageUrl(Crawler $article) {
        $imageElement = $article
            ->filter(self::SELECTOR_ARTICLE_ELEMENT_IMAGE)
            ->first();
        return $imageElement->count() > 0 ? $imageElement->attr(self::ATTR_SRC) : null;
    }

    /**
     * @param Crawler $article
     * @return string
     */
    private static function getTitle(Crawler $article) {
        $rawTitle = $article
            ->filter(self::SELECTOR_ARTICLE_ELEMENT_TITLE)
            ->first()
            ->text();
        return trim($rawTitle);
    }

    /**
     * @param Crawler $article
     * @return null|string
     */
    private static function getUrl(Crawler $article) {
        return $article
            ->filter(self::SELECTOR_ARTICLE_ELEMENT_TITLE)
            ->first()
            ->attr(self::ATTR_HREF);
    }

    /**
     * @param Crawler $article
     * @return ArticleCategoryModel
     */
    private static function getArticle(Crawler $article) {
        $url = self::getUrl($article);
        $id = self::getIdFromUrl($url);
        return new ArticleCategoryModel(
            $id,
            self::getTitle($article),
            $url,
            self::getImageUrl($article)
        );
    }

    /**
     * @param $pageUrl
     * @return ArticleCategoryModel[]
     */
    public static function getArticlesOnPage($pageUrl) {
        return self::filterArticlesOnPage($pageUrl)
            ->each(
                function(Crawler $article) {
                    return self::getArticle($article);
                }
            );
    }

    public static function getNumberOfPagesForUrl($pageUrl) {
        $numbers = self::getPageCrawler($pageUrl)
            ->filter('a.page-numbers')
            ->each(
                function ($pageNumber) {
                    return intval($pageNumber->text());
                }
            );
        return max($numbers);
    }

    /**
     * @param ArticleCategoryModel $articleModel
     * @return ArticleCategoryModel
     */
    public static function getArticleDetails(ArticleCategoryModel $articleModel): ArticleCategoryModel {
        $articleContent = self::getPageCrawler($articleModel->getUrl());

        // Get the main page element of the post

        $articleElement = $articleContent->filterXPath('//article')->first();

        $categories = $articleElement
            ->filterXPath("//div[contains(@class, 'tt-blog-category')]")
            ->first()
            ->filterXPath('//a')
            ->each(function($category) {
                /** @var Crawler $category */
                return $category->text();
            });

        $articleModel->setCategories($categories);

        $date = trim($articleElement->filterXPath('//span[@class=\'tt-post-date-single\']')->first()->html());

        $months = [
            'januari' => 'January',
            'februari' => 'February',
            'maart' => 'March',
            'april' => 'April',
            'mei' => 'May',
            'juni' => 'June',
            'juli' => 'July',
            'augustus' => 'August',
            'september' => 'September',
            'oktober' => 'October',
            'november' => 'November',
            'december' => 'December',
        ];

        foreach ($months as $month => $replace) {
            $date = str_replace($month, $replace, $date) ;
        }

        $date = \DateTime::createFromFormat('F j, Y H:i:s', $date . ' 00:00:00');

        $date = $date->getTimestamp() * 1000;

        $articleModel->setDate($date);


        $content = trim($articleElement->filterXPath('//div[contains(@class, \'tt-content\')]')->first()->html());
        $articleModel->setContent($content);


        $tags = $articleContent
            ->filterXPath('//ul[@class=\'tt-tags\']')
            ->first()
            ->filterXPath('//li')
            ->each(
                function($tag) {
                    return $tag->text();
                }
            );

        $articleModel->setTags($tags);

        return $articleModel;

    }
}