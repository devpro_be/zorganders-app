<?php

namespace App\Scraper;

use Symfony\Component\DomCrawler\Crawler;

class AbstractScraper {

    const ATTR_HREF = 'href';
    const ATTR_SRC = 'src';

    /**
     * @param $url
     * @param array|null $options
     * @return Crawler
     */
    protected static function getPageCrawler($url, array $options = null) {
        $context = $options ? stream_context_create($options) : null;
        $content = file_get_contents($url, false, $context);
        return new Crawler($content);
    }

}