<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use DateTime;

/**
 * @ORM\Entity(repositoryClass="App\Repository\NewsCategoryRepository")
 */
class NewsCategory
{
    /**
     * @var integer $id
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    // add your own fields

    /**
     * @var string $title
     * @ORM\Column(type="string")
     */
    private $title;

    /**
     * @var int $taxonomyTerm
     * @ORM\Column(type="smallint")
     */
    private $taxonomyTerm;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @param string $title
     */
    public function setTitle(string $title)
    {
        $this->title = $title;
    }

    /**
     * @return int
     */
    public function getTaxonomyTerm(): int
    {
        return $this->taxonomyTerm;
    }

    /**
     * @param int $taxonomyTerm
     */
    public function setTaxonomyTerm(int $taxonomyTerm)
    {
        $this->taxonomyTerm = $taxonomyTerm;
    }


}
