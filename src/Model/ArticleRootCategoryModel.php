<?php

namespace App\Model;


class ArticleRootCategoryModel extends AbstractModel {

    protected $setProperties = [
        'name',
    ];

    protected $setRelations = [
        'children',
    ];

    /**
     * @var string
     */
    protected $name;

    /**
     * @var array
     */
    protected $children = [];

    /**
     * ArticleCategoryModel constructor.
     * @param string $id
     * @param string $name
     */
    public function __construct($id, $name)
    {
        $this->id = $id;
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return ArticleRootCategoryModel
     */
    public function setName(string $name): ArticleRootCategoryModel
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return array
     */
    public function getChildren(): array
    {
        return $this->children;
    }

    /**
     * @param array $children
     * @return ArticleRootCategoryModel
     */
    public function setChildren(array $children): ArticleRootCategoryModel
    {
        $this->children = $children;
        return $this;
    }

    /**
     * @param string $child
     * @return ArticleRootCategoryModel
     */
    public function addChild(string $child): ArticleRootCategoryModel
    {
        $this->children[] = $child;
        return $this;
    }

}