<?php

namespace App\Model;

/**
 * Class ArticleModel
 * @package App\Model
 */
class ArticleModel extends AbstractModel
{

    protected $setProperties = [
        'title',
        'source',
        'date',
        'hasImage',
        'hasBody',
        'category',
    ];

    protected $setArrayProperties = [
        'tags',
        'rootCategories',
        'magazines',
    ];

    /**
     * @var string
     */
    protected $title;
    /**
     * @var string
     */
    protected $source;

    /**
     * @var string|null
     */
    protected $date;

    /**
     * @var boolean
     */
    protected $hasImage = false;

    /**
     * @var boolean
     */
    protected $hasBody = false;

    /**
     * @var string|null
     */
    protected $category;

    /**
     * @var array
     */
    protected $rootCategories = [];

    /**
     * @var array
     */
    protected $magazines = [];

    /**
     * @var array
     */
    protected $tags = [];

    /**
     * ArticleModel constructor.
     * @param string|null $id
     * @param string|null $title
     */
    public function __construct(
        string $id = null,
        string $title = null
    )
    {
        $this->id = $id;
        $this->title = $title;
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @param string $title
     * @return ArticleModel
     */
    public function setTitle(string $title): ArticleModel
    {
        $this->title = $title;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getSource(): ?string
    {
        return $this->source;
    }

    /**
     * @param string $source
     * @return ArticleModel
     */
    public function setSource(string $source): ArticleModel
    {
        $this->source = $source;
        return $this;
    }

    /**
     * @return string
     */
    public function getDate(): ?string
    {
        return $this->date;
    }

    /**
     * @param string|null $date
     * @return ArticleModel
     */
    public function setDate(string $date = null): ArticleModel
    {
        $this->date = $date;
        return $this;
    }

    /**
     * @return bool
     */
    public function getHasImage(): bool
    {
        return $this->hasImage;
    }

    /**
     * @param bool $hasImage
     * @return ArticleModel
     */
    public function setHasImage(bool $hasImage): ArticleModel
    {
        $this->hasImage = $hasImage;
        return $this;
    }

    /**
     * @return bool
     */
    public function getHasBody(): bool
    {
        return $this->hasBody;
    }

    /**
     * @param bool $hasBody
     * @return ArticleModel
     */
    public function setHasBody(bool $hasBody): ArticleModel
    {
        $this->hasBody = $hasBody;
        return $this;
    }

    /**
     * @return string
     */
    public function getCategory(): string
    {
        return $this->category;
    }

    /**
     * @param string $category
     * @return ArticleModel
     */
    public function setCategory(string $category): ArticleModel
    {
        $this->category = $category;
        return $this;
    }

    /**
     * @return array
     */
    public function getTags(): array
    {
        return $this->tags;
    }

    /**
     * @param array $tags
     * @return ArticleModel
     */
    public function setTags(array $tags): ArticleModel
    {
        $this->tags = $tags;
        return $this;
    }

    /**
     * @param string $tag
     * @return ArticleModel
     */
    public function addTag(string $tag): ArticleModel
    {
        $this->tags[] = $tag;
    }

    /**
     * @return array
     */
    public function getRootCategories(): array
    {
        return $this->rootCategories;
    }

    /**
     * @param array $rootCategories
     * @return ArticleModel
     */
    public function setRootCategories(array $rootCategories): ArticleModel
    {
        $this->rootCategories = $rootCategories;
        return $this;
    }

    /**
     * @param string $rootCategory
     * @return ArticleModel
     */
    public function addRootCategory(string $rootCategory): ArticleModel
    {
        if (!$this->hasRootCategory($rootCategory)) {
            $this->rootCategories[] = $rootCategory;
        }
        return $this;
    }

    /**
     * @param string $rootCategory
     * @return bool
     */
    public function hasRootCategory(string $rootCategory): bool
    {
        return in_array($rootCategory, $this->rootCategories);
    }

    /**
     * @return array
     */
    public function getMagazines(): array
    {
        return $this->magazines;
    }

    /**
     * @param array $magazines
     * @return ArticleModel
     */
    public function setMagazines(array $magazines): ArticleModel
    {
        $this->magazines = $magazines;
        return $this;
    }

    /**
     * @param string $magazine
     * @return ArticleModel
     */
    public function addMagazine(string $magazine): ArticleModel
    {
        if (!$this->hasMagazine($magazine)) {
            $this->magazines[] = $magazine;
        }
        return $this;
    }

    /**
     * @param string $magazine
     * @return bool
     */
    public function hasMagazine(string $magazine): bool
    {
        return in_array($magazine, $this->magazines);
    }

}