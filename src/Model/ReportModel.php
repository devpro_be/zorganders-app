<?php

namespace App\Model;

/**
 * Class ReportModel
 * @package App\Model
 */
class ReportModel extends AbstractModel
{

    const TYPE_REPORTAGES = '/reportages/';
    const TYPE_NODE = '/node/';

    protected $setProperties = [
        'title',
        'source',
        'type',
        'video',
        'date',
        'hasImage',
        'hasBody',
        'category',
    ];

    protected $setArrayProperties = [
        'tags',
        'rootCategories',
    ];

    /**
     * @var string
     */
    protected $title;
    
    /**
     * @var string
     */
    protected $source;

    /**
     * @var string
     */
    protected $type;

    /**
     * @var string
     */
    protected $video;

    /**
     * @var string|null
     */
    protected $date;

    /**
     * @var boolean
     */
    protected $hasImage = false;

    /**
     * @var boolean
     */
    protected $hasBody = false;

    /**
     * @var string|null
     */
    protected $category;

    /**
     * @var array
     */
    protected $rootCategories = [];

    /**
     * @var array
     */
    protected $tags = [];

    /**
     * ReportModel constructor.
     * @param string|null $id
     * @param string|null $title
     */
    public function __construct(
        string $id = null,
        string $title = null
    )
    {
        $this->id = $id;
        $this->title = $title;
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @param string $title
     * @return ReportModel
     */
    public function setTitle(string $title): ReportModel
    {
        $this->title = $title;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getSource(): ?string
    {
        return $this->source;
    }

    /**
     * @param string $source
     * @return ReportModel
     */
    public function setSource(string $source): ReportModel
    {
        $this->source = $source;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getType(): ?string
    {
        return $this->type;
    }

    /**
     * @param string $type
     * @return ReportModel
     */
    public function setType(string $type): ReportModel
    {
        $this->type = $type;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getVideo(): ?string
    {
        return $this->video;
    }

    /**
     * @param null|string $video
     * @return ReportModel
     */
    public function setVideo(?string $video = null): ReportModel
    {
        $this->video = $video;
        return $this;
    }

    /**
     * @return string
     */
    public function getDate(): ?string
    {
        return $this->date;
    }

    /**
     * @param string|null $date
     * @return ReportModel
     */
    public function setDate(string $date = null): ReportModel
    {
        $this->date = $date;
        return $this;
    }

    /**
     * @return bool
     */
    public function getHasImage(): bool
    {
        return $this->hasImage;
    }

    /**
     * @param bool $hasImage
     * @return ReportModel
     */
    public function setHasImage(bool $hasImage): ReportModel
    {
        $this->hasImage = $hasImage;
        return $this;
    }

    /**
     * @return bool
     */
    public function getHasBody(): bool
    {
        return $this->hasBody;
    }

    /**
     * @param bool $hasBody
     * @return ReportModel
     */
    public function setHasBody(bool $hasBody): ReportModel
    {
        $this->hasBody = $hasBody;
        return $this;
    }

    /**
     * @return string
     */
    public function getCategory(): string
    {
        return $this->category;
    }

    /**
     * @param string $category
     * @return ReportModel
     */
    public function setCategory(string $category): ReportModel
    {
        $this->category = $category;
        return $this;
    }

    /**
     * @return array
     */
    public function getTags(): array
    {
        return $this->tags;
    }

    /**
     * @param array $tags
     * @return ReportModel
     */
    public function setTags(array $tags): ReportModel
    {
        $this->tags = $tags;
        return $this;
    }

    /**
     * @param string $tag
     * @return ReportModel
     */
    public function addTag(string $tag): ReportModel
    {
        $this->tags[] = $tag;
    }

    /**
     * @return array
     */
    public function getRootCategories(): array
    {
        return $this->rootCategories;
    }

    /**
     * @param array $rootCategories
     * @return ReportModel
     */
    public function setRootCategories(array $rootCategories): ReportModel
    {
        $this->rootCategories = $rootCategories;
        return $this;
    }

    /**
     * @param string $rootCategory
     * @return ReportModel
     */
    public function addRootCategory(string $rootCategory): ReportModel
    {
        if (!$this->hasRootCategory($rootCategory)) {
            $this->rootCategories[] = $rootCategory;
        }
        return $this;
    }

    /**
     * @param string $rootCategory
     * @return bool
     */
    public function hasRootCategory(string $rootCategory): bool
    {
        return in_array($rootCategory, $this->rootCategories);
    }

}