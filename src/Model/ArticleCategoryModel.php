<?php

namespace App\Model;


class ArticleCategoryModel extends AbstractModel {

    const SOURCE_ZORGANDERS = 'ZORGANDERS';
    const SOURCE_ACTUALCARE = 'ACTUALCARE';

    protected $setProperties = [
        'name',
        'source',
        'url',
        'numberOfPages',
    ];

    /**
     * @var string
     */
    protected $name;

    /**
     * @var string
     */
    protected $source;

    /**
     * @var string
     */
    protected $url;

    /**
     * @var int
     */
    protected $numberOfPages = 0;

    /**
     * ArticleCategoryModel constructor.
     * @param string|null $id
     * @param string|null $name
     * @param string|null $url
     */
    public function __construct(string $id = null, string $name = null, string $url = null)
    {
        $this->id = $id;
        $this->name = $name;
        $this->url = $url;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return ArticleCategoryModel
     */
    public function setName(string $name): ArticleCategoryModel
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return string
     */
    public function getSource(): string
    {
        return $this->source;
    }

    /**
     * @param string $source
     * @return ArticleCategoryModel
     */
    public function setSource(string $source): ArticleCategoryModel
    {
        $this->source = $source;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getUrl(): ?string
    {
        return $this->url;
    }

    /**
     * @param string $url
     * @return ArticleCategoryModel
     */
    public function setUrl(string $url): ArticleCategoryModel
    {
        $this->url = $url;
        return $this;
    }

    /**
     * @return int
     */
    public function getNumberOfPages(): int
    {
        return $this->numberOfPages;
    }

    /**
     * @param int $numberOfPages
     */
    public function setNumberOfPages(int $numberOfPages)
    {
        $this->numberOfPages = $numberOfPages;
    }
}