<?php

namespace App\Model;

/**
 * Class AbstractModel
 *
 * Add basic functionality to each model :
 *  - Add properties from and to the sets
 *  - Add relations from and to the set
 *  - Add timestamps from and to the set
 *
 * @author Jonas Verhaert <jonas.verhaert@gmail.com>
 * @version 1.0
 * @created 6th october 2018
 * @updated 6th october 2018
 * @package App\Model
 */
abstract class AbstractModel {

    const SET_PROPERTY_TIMESTAMP_CREATED = 'ts_created';
    const SET_PROPERTY_TIMESTAMP_UPDATED = 'ts_updated';

    /**
     * @var string|null
     */
    protected $id;

    /**
     * The name of properties that should be read and written to the set.
     * @var array
     */
    protected $setProperties = [];

    /**
     * The name of properties that should be read and written as an array to the set.
     * @var array
     */
    protected $setArrayProperties = [];

    /**
     * The names of the relation properties. e.g. tags & categories.
     * @var array
     */
    protected $setRelations = [];

    /**
     * Setting if timestamps should be added automatic to set data
     * Default set to true.
     * @var bool
     */
    protected $timestampsActive = true;

    /**
     * Timestamp of the the first time fromSet or toSet call.
     * @var string|null
     */
    protected $timestampCreated = null;

    /**
     * Timestamp of the latest toSet call.
     * @var string|null
     */
    protected $timestampCurrent = null;

    /**
     * @return null|string
     */
    public function getId(): ?string
    {
        return $this->id;
    }

    /**
     * @param null|string $id
     * @return AbstractModel
     */
    public function setId(string $id = null)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @param array $setData
     * @return AbstractModel
     */
    public function fromSet(array $setData) {
        $this->propertiesFromSet($setData);
        $this->arrayFromSet($setData);
        $this->relationsFromSet($setData);
        $this->timestampsFromSet($setData);
        return $this;
    }

    /**
     * @return array
     */
    public function toSet():array {
        $setData = [];
        $setData = $this->propertiesToSet($setData);
        $setData = $this->arrayToSet($setData);
        $setData = $this->relationsToSet($setData);
        $setData = $this->timestampsToSet($setData);
        return $setData;
    }

    /**
     * =========================
     * PROPERTIES
     * =========================
     */

    /**
     * @param array $setData
     */
    private function propertiesFromSet(array $setData) {
        foreach ($this->setProperties as $property) {
            if (array_key_exists($property, $setData)) {
                $this->{'set'.ucfirst($property)}($setData[$property]);
            }
        }
    }

    /**
     * @param array $setData
     * @return array
     */
    private function propertiesToSet(array $setData):array {
        foreach ($this->setProperties as $property) {
            $setData[$property] = $this->{ 'get' . ucfirst($property)}();
        }
        return $setData;
    }

    /**
     * =========================
     * ARRAYS
     * =========================
     */

    /**
     * @param array $setData
     */
    private function arrayFromSet(array $setData) {
        foreach ($this->setArrayProperties as $arrayProperty) {
            if (array_key_exists($arrayProperty, $setData) && is_array($setData[$arrayProperty])) {
                $data = [];
                foreach ($setData[$arrayProperty] as $key => $value) {
                    $data[] = $value;
                }
                $this->{'set'.ucfirst($arrayProperty)}($data);
            }
        }
    }

    /**
     * @param array $setData
     * @return array
     */
    private function arrayToSet(array $setData):array {
        foreach ($this->setArrayProperties as $arrayProperty) {
            $value = $this->{'get' . $arrayProperty}();
            if (is_array($value)) {
                $setData[$arrayProperty] = $value;
            }
        }
        return $setData;
    }

    /**
     * =========================
     * RELATIONS
     * =========================
     */

    /**
     * @param array $setData
     */
    private function relationsFromSet(array $setData) {
        foreach ($this->setRelations as $relation) {
            if (array_key_exists($relation, $setData) && is_array($setData[$relation])) {
                $data = [];
                foreach ($setData[$relation] as $key => $value) {
                    if ($value) {
                        $data[] = $key;
                    }
                }
                $this->{'set'.ucfirst($relation)}($data);
            }
        }
    }

    /**
     * @param array $setData
     * @return array
     */
    private function relationsToSet(array $setData):array {
        foreach ($this->setRelations as $relation) {
            $value = $this->{'get' . $relation}();
            if (is_array($value)) {
                $data = [];
                foreach ($value as $item) {
                    $data[$item] = true;
                }
                $setData[$relation] = $data;
            }
        }
        return $setData;
    }

    /**
     * =========================
     * TIMESTAMPS
     * =========================
     */

    /**
     * Generates the current timestamp
     */
    protected function setCurrentTimestamp() {
        $date = new \DateTime('now');
        $this->timestampCurrent = $date->getTimestamp();
    }

    /**
     * sets the timestampCreated when times
     * @param array $setData
     */
    private function timestampsFromSet(array $setData) {
        if ($this->timestampsActive) {
            $this->setCurrentTimestamp();
            $this->timestampCreated = array_key_exists(self::SET_PROPERTY_TIMESTAMP_CREATED, $setData) ?
                $setData[self::SET_PROPERTY_TIMESTAMP_CREATED] : $this->timestampCurrent;
        }
    }

    /**
     * Adds the timestamp created and updated to the setData
     * @param array $setData
     * @return array
     */
    private function timestampsToSet(array $setData):array {
        if ($this->timestampsActive) {
            $this->setCurrentTimestamp();
            $setData[self::SET_PROPERTY_TIMESTAMP_UPDATED] = $this->timestampCurrent;
            $setData[self::SET_PROPERTY_TIMESTAMP_CREATED] = $this->timestampCreated === null ?
                $this->timestampCurrent : $this->timestampCreated;
        }
        return $setData;
    }

}