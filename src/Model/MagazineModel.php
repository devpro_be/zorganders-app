<?php

namespace App\Model;


class MagazineModel extends AbstractModel {

    protected $setProperties = [
        'name',
    ];

    protected $setArrayProperties = [
        'categories',
    ];

    /**
     * @var string
     */
    protected $name;

    /**
     * @var array
     */
    protected $categories = [];

    /**
     * ArticleCategoryModel constructor.
     * @param string $id
     * @param string $name
     */
    public function __construct($id, $name)
    {
        $this->id = $id;
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return MagazineModel
     */
    public function setName(string $name): MagazineModel
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return array
     */
    public function getCategories(): array
    {
        return $this->categories;
    }

    /**
     * @param array $categories
     * @return MagazineModel
     */
    public function setCategories(array $categories): MagazineModel
    {
        $this->categories = $categories;
        return $this;
    }

    /**
     * @param string $category
     * @return MagazineModel
     */
    public function addCategory(string $category): MagazineModel
    {
        $this->categories[] = $category;
        return $this;
    }

}