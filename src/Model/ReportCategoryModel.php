<?php

namespace App\Model;

class ReportCategoryModel extends AbstractModel {

    protected $setProperties = [
        'name',
        'source',
        'url',
        'numberOfPages',
    ];

    /**
     * @var string
     */
    protected $name;

    /**
     * @var string
     */
    protected $source;

    /**
     * @var string
     */
    protected $url;

    /**
     * @var int
     */
    protected $numberOfPages = 0;

    /**
     * ReportCategoryModel constructor.
     * @param string|null $id
     * @param string|null $name
     * @param string|null $url
     */
    public function __construct(string $id = null, string $name = null, string $url = null)
    {
        $this->id = $id;
        $this->name = $name;
        $this->url = $url;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return ReportCategoryModel
     */
    public function setName(string $name): ReportCategoryModel
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return string
     */
    public function getSource(): string
    {
        return $this->source;
    }

    /**
     * @param string $source
     * @return ReportCategoryModel
     */
    public function setSource(string $source): ReportCategoryModel
    {
        $this->source = $source;
        return $this;
    }

    /**
     * @return string
     */
    public function getUrl(): string
    {
        return $this->url;
    }

    /**
     * @param string $url
     * @return ReportCategoryModel
     */
    public function setUrl(string $url): ReportCategoryModel
    {
        $this->url = $url;
        return $this;
    }

    /**
     * @return int
     */
    public function getNumberOfPages(): int
    {
        return $this->numberOfPages;
    }

    /**
     * @param int $numberOfPages
     * @return ReportCategoryModel
     */
    public function setNumberOfPages(int $numberOfPages): ReportCategoryModel
    {
        $this->numberOfPages = $numberOfPages;
        return $this;
    }
}