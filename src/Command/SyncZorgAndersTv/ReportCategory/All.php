<?php
namespace App\Command\SyncZorgAndersTv\ReportCategory;

use App\Command\AbstractCommand;
use App\Google\Firestore\ReportCategoriesFirestore;
use App\Model\ReportCategoryModel;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputArgument;

class All extends AbstractCommand
{

    const COMMAND_NAME = 'sync-zorganders-tv:report-category:all';
    const COMMAND_DESCRIPTION = 'Sync number of pages and articles from a certain category of the zorganders.tv site.';
    const COMMAND_HELP = 'This command synchronises the number of pages and articles from a certain category of the zorganders.tv site.';

    const COMMAND_ARGUMENT_CATEGORY_ID = 'categoryId';
    const COMMAND_OPTION_SKIP_TO_PAGE = 'skip-to-page';


    /**
     * @var int
     */
    protected $startPage = 0;

    /**
     * @var ReportCategoriesFirestore
     */
    protected $reportCategoriesFirestore;

    protected function configure()
    {
        parent::setup(
            self::COMMAND_NAME,
            self::COMMAND_DESCRIPTION,
            self::COMMAND_HELP
        )
            ->addArgument(
                self::COMMAND_ARGUMENT_CATEGORY_ID,
                InputArgument::OPTIONAL,
                'Optional: the category id of a certain category that needs to be scraped.'
            )

            ->addOption(
                self::COMMAND_OPTION_SKIP_TO_PAGE,
                null,
                InputOption::VALUE_OPTIONAL,
                'Skip sync process to certain page, if that page exists...',
                1
            )
        ;

        $this->reportCategoriesFirestore = new ReportCategoriesFirestore();
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int
     */
    protected function execute(InputInterface $input, OutputInterface $output):int
    {
        parent::execute($input, $output);

        $categoryId = $input->getArgument(self::COMMAND_ARGUMENT_CATEGORY_ID);
        $this->startPage = (int)$input->getOption(self::COMMAND_OPTION_SKIP_TO_PAGE);

        if ($categoryId) {
            $this->executeForOneCategory($categoryId);
        } else {
            $this->executeForAllCategories();
        }

        return 0;
    }

    /**
     * @param string $categoryId
     */
    private function executeForOneCategory(string $categoryId)
    {
        $formatTitle = 'Sync all for category "%s"';
        $title = sprintf($formatTitle, $categoryId);
        $this->writelnTitle($title);

        $categoryModel = $this->getCategoryFromFirebase($categoryId);

        $this->syncForOneCategory($categoryModel);
    }

    private function executeForAllCategories()
    {
        $this->writelnTitle('Sync all for all categories stored in firestore.');

        $categoryModels = $this
            ->reportCategoriesFirestore
            ->getAllFromSource(ReportCategoriesFirestore::SOURCE_ZORGANDERS);

        foreach ($categoryModels as $categoryModel) {
            $formatMessage = 'Will try to sync for category "<green>%s</green>".';
            $message = sprintf($formatMessage, $categoryModel->getId());
            $this->writeln($message);
            $this->syncForOneCategory($categoryModel);
        }
    }

    /**
     * @param string $categoryId
     * @return ReportCategoryModel
     */
    private function getCategoryFromFirebase(string $categoryId):ReportCategoryModel
    {
        $formatMessage = 'article category "%s"';
        $firebaseContext = sprintf($formatMessage, $categoryId);

        $this->writelnFetchingFromFirebase($firebaseContext);

        /** @var ReportCategoryModel $categoryModel */
        $categoryModel = $this->reportCategoriesFirestore->getById($categoryId);

        $this->writelnFetchedFromFirebase($firebaseContext);

        return $categoryModel;
    }

    /**
     * @return ReportCategoryModel[]
     */
    private function getAllCategoriesFromFirebase():array
    {
        $firebaseContext = '<blue>all</blue> article category';
        $this->writelnFetchingFromFirebase($firebaseContext);
        $categoryModels = $this->reportCategoriesFirestore->getAllFromSource(ReportCategoriesFirestore::SOURCE_ZORGANDERS);
        $this->writelnFetchedFromFirebase($firebaseContext);
        $this->listAllFetchedCategoriesInConsole($categoryModels);
        return $categoryModels;
    }

    /**
     * @param ReportCategoryModel[] $categoryModels
     */
    private function listAllFetchedCategoriesInConsole(array $categoryModels) {
        $formatMessage = 'Found <green>%d</green> categories in firebase:';
        $message = sprintf($formatMessage, count($categoryModels));
        $this->writeln($message);

        $messageListOfCategories = [];
        $formatMessage = '- <green>%s</green>';
        foreach ($categoryModels as $categoryModel) {
            $message = sprintf($formatMessage, $categoryModel->getId());
            $messageListOfCategories[] = $message;
        }

        $this->writelns($messageListOfCategories);
    }

    /**
     * @param ReportCategoryModel $categoryModel
     */
    private function syncForOneCategory(ReportCategoryModel $categoryModel)
    {

        if ($categoryModel->getUrl() === null) {
            $this->writelnError('This category does not exists in firestore or has no valid URL set.');
        } else {

            $this
                ->executeNumberOfPages($categoryModel)
                ->executeReports($categoryModel);
        }
    }

    /**
     * @param ReportCategoryModel $categoryModel
     * @return All
     */
    protected function executeNumberOfPages(ReportCategoryModel $categoryModel):All
    {
        $commandName = NumberOfPages::COMMAND_NAME;
        $arguments = array(
            NumberOfPages::ARGUMENT_CATEGORY_ID => $categoryModel->getId(),
        );
        $this->runCommand($commandName, $arguments);
        return $this;
    }

    /**
     * @param ReportCategoryModel $categoryModel
     */
    protected function executeReports(ReportCategoryModel $categoryModel) {

        $commandName = ReportsOnPage::COMMAND_NAME;

        /** @var ReportCategoryModel $categoryModel */
        $categoryModel = $this
            ->reportCategoriesFirestore
            ->getById($categoryModel->getId())
        ;

        $numberOfPages = $categoryModel->getNumberOfPages();


        $pageOffset = ($numberOfPages > $this->startPage) ? $this->startPage : $numberOfPages;

        if ($numberOfPages === 0 || $numberOfPages === null) {
           $this->writeln('No pages for the category.');
        } else {
            for ($i=$pageOffset; $i <= $numberOfPages; $i++) {
                $arguments = array(
                    ReportsOnPage::COMMAND_ARGUMENT_CATEGORY_ID  => $categoryModel->getId(),
                    ReportsOnPage::COMMAND_ARGUMENT_PAGE => $i,
                );
                $this->runCommand($commandName, $arguments);
            }
        }


    }
}