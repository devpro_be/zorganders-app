<?php
namespace App\Command\SyncZorgAndersTv\ReportCategory;

use App\Command\AbstractCommand;
use App\Google\Firestore\ReportCategoriesFirestore;
use App\Model\ArticleCategoryModel;
use App\Model\ReportCategoryModel;
use App\Scraper\ZorgAndersTv\ReportZorgAndersTvScraper;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class NumberOfPages extends AbstractCommand
{
    const COMMAND_NAME = 'sync-zorganders-tv:report-category:number-of-pages';
    const ARGUMENT_CATEGORY_ID = 'categoryId';

    /**
     * @var ReportCategoriesFirestore
     */
    protected $reportCategoriesFirestore;

    protected function configure()
    {
        parent::setup(
            self::COMMAND_NAME,
            self::COMMAND_DESCRIPTION,
            self::COMMAND_HELP
        )
            ->addArgument(
                self::ARGUMENT_CATEGORY_ID,
                InputArgument::OPTIONAL,
                'Optional: add category for only updating for a certain category.'
            )
        ;

        $this->reportCategoriesFirestore = new ReportCategoriesFirestore();
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int
     */
    protected function execute(InputInterface $input, OutputInterface $output):int
    {
        parent::execute($input, $output);

        $categoryId = $input->getArgument(self::ARGUMENT_CATEGORY_ID);

        if ($categoryId) {
            $this->updateCategory($categoryId);
        } else {
            $this->updateAllCategories();
        }

        return 0;
    }

    /**
     * @param ReportCategoryModel $categoryModel
     * @return NumberOfPages
     */
    private function scrapeNumberOfPagesForCategory(ReportCategoryModel $categoryModel): NumberOfPages
    {

        $numberOfPages = ReportZorgAndersTvScraper::getNumberOfPagesForCategory($categoryModel);
        $categoryModel->setNumberOfPages($numberOfPages);

        $formatMessage = 'Found <blue>%d</blue> page(s) for the category "<blue>%s</blue>".';
        $message = sprintf($formatMessage, $numberOfPages, $categoryModel->getId());
        $this->writeln($message);

        $firebaseContext = 'the updated category';

        $this->writelnStoringIntoFirebase($firebaseContext);

        $this->reportCategoriesFirestore->save($categoryModel);

        $this->writelnStoredIntoFirebase($firebaseContext);

        return $this;
    }

    private function updateCategory(string $categoryId)
    {
        /** @var ArticleCategoryModel $categoryModel */
        $categoryModel = $this->reportCategoriesFirestore->getById($categoryId);

        if ($categoryModel->getUrl() === null) {
            $this->writelnError('The category doens\'t exists in firestore or hasn\'t got a valid value for the url.');
        } else {
            $this->scrapeNumberOfPagesForCategory($categoryModel);
        }
    }

    private function updateAllCategories() {

        $this->writelnTitle('Sync the number of pages for all articles categories on the actualcare site.');

        $categoryModels = $this->reportCategoriesFirestore->getAllFromSource(ReportCategoriesFirestore::SOURCE_ZORGANDERS);
        $numberOfCategories = count($categoryModels);
        $numberOfCategoriesFoundFormat = 'Found <green>%s</green> categories in firestore for zorganderstv.be';
        $message = sprintf($numberOfCategoriesFoundFormat, $numberOfCategories);
        $this->writeln($message);

        foreach ($categoryModels as $categoryModel) {

            $messageFormat = 'Scraping number of pages for "<green>%s</green>"';
            $message = sprintf($messageFormat, $categoryModel->getId());
            $this->writeln($message);

            $this->scrapeNumberOfPagesForCategory(
                $categoryModel
            );

        }

        $this->writelnSuccess('Done: completed all the categories from firestore.');
    }
}