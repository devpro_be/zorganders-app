<?php
namespace App\Command\SyncZorgAndersTv\ReportCategory;

use App\Command\AbstractCommand;
use App\Google\Firestore\ReportCategoriesFirestore;
use App\Google\Firestore\ReportsFirestore;
use App\Model\ArticleCategoryModel;
use App\Model\ReportModel;
use App\Scraper\ZorgAndersTv\ReportZorgAndersTvScraper;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputArgument;


class ReportsOnPage extends AbstractCommand
{

    const COMMAND_NAME = 'sync-zorganders-tv:report-category:reports';
    const COMMAND_DESCRIPTION = 'Sync the reports of each report category on the zorganderstv.be site.';
    const COMMAND_HELP = 'This command synchronises all the articles of a certain category and page';

    const COMMAND_ARGUMENT_CATEGORY_ID = 'categoryId';
    const COMMAND_ARGUMENT_PAGE = 'page';
    const COMMAND_ARGUMENT_SKIP_TO_PAGE = 'skip-to-page';

    /**
     * @var ReportCategoriesFirestore
     */
    protected $reportCategoriesFirestore;

    /**
     * @var ReportsFirestore
     */
    protected $reportsFirestore;

    protected function configure()
    {
        parent::setup(
            self::COMMAND_NAME,
            self::COMMAND_DESCRIPTION,
            self::COMMAND_HELP
        )
            ->addArgument(
                self::COMMAND_ARGUMENT_CATEGORY_ID,
                InputArgument::REQUIRED,
                'The id of the category where the articles need to be scraped from.')

            ->addArgument(
                self::COMMAND_ARGUMENT_PAGE,
                InputArgument::REQUIRED,
                'The page of the category that needs to be synced.'
            )
        ;

        $this->reportsFirestore = new ReportsFirestore();
        $this->reportCategoriesFirestore = new ReportCategoriesFirestore();
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int
     */
    protected function execute(InputInterface $input, OutputInterface $output):int
    {
        parent::execute($input, $output);


        $page = (int)$input->getArgument(self::COMMAND_ARGUMENT_PAGE);
        $categoryId = $input->getArgument(self::COMMAND_ARGUMENT_CATEGORY_ID);

        $this->writelnTitle('Going to sync the articles for a certain category');


        $messageFormat = 'Will scrape page <green>%d</green> of the category "<green>%s</green>"';
        $message = sprintf($messageFormat, $page, $categoryId);
        $this->writeln($message);


        $formatFetchContext = 'report category "<blue>%s</blue>"';
        $fetchContext = sprintf($formatFetchContext, $categoryId);

        $this->writelnFetchingFromFirebase($fetchContext);
        /** @var ArticleCategoryModel $categoryModel */
        $categoryModel = $this->reportCategoriesFirestore->getById($categoryId);
        $this->writelnFetchedFromFirebase($fetchContext);

        $this->writeln('Scraping the reports from the page...');
        try {
            $reportModels = ReportZorgAndersTvScraper::getReportsForCategoryOnPage($categoryModel, $page);
        } catch (\Exception $e) {
            $reportModels = [];
            $this->writelnError($e->getMessage());
        }

        $messageFormat = 'Found <green>%s</green> article(s) on the page.';
        $message = sprintf($messageFormat, count($reportModels));
        $this->writeln($message);

        $this->storeReportsIntoFirestore($reportModels);

        // $this->collectArticlesDetails($articleModels);

        $this->writelnSuccess('done completely');

        return 0;
    }

    /**
     * @param ReportModel[] $reportModels
     */
    private function storeReportsIntoFirestore(array $reportModels)
    {
       $formatStoreContext = 'report "<green>%s</green>"';

        foreach ($reportModels as $reportModel) {
            $storeContext = sprintf($formatStoreContext, $reportModel->getId());
            $this->writelnStoringIntoFirebase($storeContext);
            $this->reportsFirestore->save($reportModel);
            $this->writelnStoredIntoFirebase($storeContext);
        }
    }
}