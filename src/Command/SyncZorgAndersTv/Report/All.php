<?php
namespace App\Command\SyncZorgAndersTv\Report;

use App\Command\AbstractCommand;
use App\Firebase\ArticlesFirebase;
use App\Google\Firestore\ArticlesFirestore;
use App\Google\Firestore\ReportsFirestore;
use App\Model\ArticleModel;
use App\Model\ReportModel;
use App\Scraper\ZorgAndersTv\ReportZorgAndersTvScraper;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputArgument;

class All extends AbstractCommand
{

    const COMMAND_NAME = 'sync-zorganders-tv:report:all';
    const COMMAND_DESCRIPTION = 'Sync all the report details from the zorganderstv.be site.';
    const COMMAND_HELP = 'This command synchronises the details of the report on the zorganderstv.be site.';

    const COMMAND_ARGUMENT_REPORT_ID = 'reportId';

    /**
     * @var ReportsFirestore
     */
    protected $reportsFirestore;

    protected function configure()
    {
        parent::setup(
            self::COMMAND_NAME,
            self::COMMAND_DESCRIPTION,
            self::COMMAND_HELP
        )
            ->addArgument(
                self::COMMAND_ARGUMENT_REPORT_ID,
                InputArgument::OPTIONAL,
                'Optional: the report id of a certain report that needs to be synced.'
            )
        ;

        $this->reportsFirestore = new ReportsFirestore();
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int
     */
    protected function execute(InputInterface $input, OutputInterface $output):int
    {
        parent::execute($input, $output);

        $reportId = $input->getArgument(self::COMMAND_ARGUMENT_REPORT_ID);

        if ($reportId) {
            $this->executeForOneReport($reportId);
        } else {
            $this->executeForAllReports();
        }

        return 0;
    }

    /**
     * @param string $reportId
     */
    private function executeForOneReport(string $reportId)
    {
        $formatTitle = 'Sync details for report "%s"';
        $title = sprintf($formatTitle, $reportId);
        $this->writelnTitle($title);

        /** @var ReportModel $reportModel */
        $reportModel = $this->reportsFirebase->getById($reportId);

        $this->syncForOneReport($reportModel);
    }

    private function executeForAllReports()
    {
        $this->writelnTitle('Sync all for all articles stored on firestore.');

        $reportModels = $this->reportsFirestore->getAllFromSource(ArticlesFirestore::SOURCE_ZORGANDERS);

        foreach ($reportModels as $reportModel) {
            $formatMessage = 'Will try to sync report "<green>%s</green>".';
            $message = sprintf($formatMessage, $reportModel->getId());
            $this->writeln($message);
            $this->syncForOneReport($reportModel);
        }

        $this->writelnSuccess('done');
    }


    /**
     * @param ReportModel $reportModel
     */
    private function syncForOneReport(ReportModel $reportModel)
    {

        if ($reportModel->getType() === null) {
            $this->writelnError('This report does not exists in firestore or has no valid type set.');
        } else {
            $reportModel = ReportZorgAndersTvScraper::getReportDetails($reportModel);
            $this->writelnSavingIntoFirestore('article');
            $this->reportsFirestore->save($reportModel);
            $this->writelnSavedIntoFirestore('article');
            $this->writeln('done for article.');
        }
    }


}