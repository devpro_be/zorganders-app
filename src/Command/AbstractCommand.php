<?php
namespace App\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Formatter\OutputFormatterStyle;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

abstract class AbstractCommand extends Command {


    const COMMAND_NAME = null;
    const COMMAND_DESCRIPTION = 'No command description is set';
    const COMMAND_HELP = 'No command help is set';

    const COMMAND_OPTION_CHAINED = 'chained';
    const COMMAND_OPTION_QUITE_MODE = 'quite-modus';
    const COMMAND_OPTION_FORCE_MODE = 'force-mode';

    protected $isChained = false;

    protected $isQuiteMode = false;

    protected $isForceMode = false;

    /**
     * @var OutputInterface $output
     */
    protected $output;

    /**
     * @var int
     */
    protected $numberOfErrors = 0;

    /**
     * @param string $name
     * @param string $description
     * @param string|null $help
     * @return $this
     */
    protected function setup(
        string $name,
        string $description,
        string $help = null
    ) {
        return $this
            ->setName($name)
            ->setDescription($description)
            // the full command description shown when running the command with
            // the "--help" option
            ->setHelp($help)
            ->addOption(
            self::COMMAND_OPTION_CHAINED,
            null,
            InputOption::VALUE_OPTIONAL,
            'Indicate if the command is chained from other command.',
            false
            )
            ->addOption(
                self::COMMAND_OPTION_QUITE_MODE,
                null,
                InputOption::VALUE_OPTIONAL,
                'Indicate if no console output for this command my be written',
                false
            )
            ->addOption(
                self::COMMAND_OPTION_FORCE_MODE,
                null,
                InputOption::VALUE_OPTIONAL,
                'Indicate if the command needs to run in forced modus, e.g. overwrite already stored data on firebase storage.',
                false
            )
        ;
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int
     */
    protected function execute(InputInterface $input, OutputInterface $output):int
    {
        $this->setOutput($output);

        $this->isChained = (bool) $input->getOption(self::COMMAND_OPTION_CHAINED);
        $this->isQuiteMode = (bool) $input->getOption(self::COMMAND_OPTION_QUITE_MODE);
        $this->isForceMode = (bool) $input->getOption(self::COMMAND_OPTION_FORCE_MODE);

        return 0;
    }


    /**
     * @param string $firebaseContext
     * @return AbstractCommand
     */
    protected function writelnFetchingFromFirebase(string $firebaseContext): AbstractCommand {
        $formatMessage = 'Going to fetch %s from firebase...';
        $message = sprintf($formatMessage, $firebaseContext);
        return $this->writeln($message);
    }

    /**
     * @param string $firebaseContext
     * @return AbstractCommand
     */
    protected function writelnFetchedFromFirebase(string $firebaseContext): AbstractCommand {
        $formatMessage = 'Fetched %s from firebase.';
        $message = sprintf($formatMessage, $firebaseContext);
        return $this->writeln($message);
    }

    /**
     * @param string $firebaseContext
     * @return AbstractCommand
     */
    protected function writelnSavingIntoFirestore(string $firebaseContext): AbstractCommand {
        $formatMessage = 'Going to save %s into firestore...';
        $message = sprintf($formatMessage, $firebaseContext);
        return $this->writeln($message);
    }

    /**
     * @param string $firebaseContext
     * @return AbstractCommand
     */
    protected function writelnSavedIntoFirestore(string $firebaseContext): AbstractCommand {
        $formatMessage = 'Saved %s into firestore.';
        $message = sprintf($formatMessage, $firebaseContext);
        return $this->writeln($message);
    }

    /**
     * @param string $firebaseContext
     * @return AbstractCommand
     */
    protected function writelnStoringIntoFirebase(string $firebaseContext): AbstractCommand {
        $formatMessage = 'Going to store %s into firebase...';
        $message = sprintf($formatMessage, $firebaseContext);
        return $this->writeln($message);
    }

    /**
     * @param string $firebaseContext
     * @return AbstractCommand
     */
    protected function writelnStoredIntoFirebase(string $firebaseContext): AbstractCommand {
        $formatMessage = 'Stored %s into firebase.';
        $message = sprintf($formatMessage, $firebaseContext);
        return $this->writeln($message);
    }

    /**
     * @param string $styleName
     * @param OutputFormatterStyle $outputFormatterStyle
     * @return AbstractCommand
     */
    private function setOutputFormatterStyle(string $styleName, OutputFormatterStyle $outputFormatterStyle): AbstractCommand {
        $this->output->getFormatter()->setStyle($styleName, $outputFormatterStyle);
        return $this;
    }

    /**
     * @return AbstractCommand
     */
    private function addTitleStyle(): AbstractCommand {
        $outputStyle = new OutputFormatterStyle('blue', 'default');
        return $this->setOutputFormatterStyle('title', $outputStyle);
    }

    /**
     * @return AbstractCommand
     */
    private function addBlueStyle(): AbstractCommand {
        $outputStyle = new OutputFormatterStyle('blue', 'default');
        return $this->setOutputFormatterStyle('blue', $outputStyle);
    }

    /**
     * @return AbstractCommand
     */
    private function addGreenStyle(): AbstractCommand {
        $outputStyle = new OutputFormatterStyle('green', 'default');
        return $this->setOutputFormatterStyle('green', $outputStyle);
    }

    /**
     * @return AbstractCommand
     */
    private function addErrorStyle(): AbstractCommand {
        $outputStyle = new OutputFormatterStyle('red', 'default');
        return $this->setOutputFormatterStyle('error', $outputStyle);
    }

    private static function createLine(string $string, string $lineCharacter = '='):string {
        $numberOfCharacters = strlen($string);
        $line = '';
        for ($i=0; $i<$numberOfCharacters; $i++) {
            $line .= $lineCharacter;
        }
        return $line;
    }

    /**
     * @param OutputInterface $output
     * @return AbstractCommand
     */
    protected function setOutput(OutputInterface $output): AbstractCommand {
        $this->output = $output;
        return $this
            ->addTitleStyle()
            ->addErrorStyle()
            ->addGreenStyle()
            ->addBlueStyle();
    }

    /**
     * @param string $commandName
     * @return Command
     */
    protected function getCommand(string $commandName):Command
    {
        $command = null;

        try {
            $command = $this
                ->getApplication()
                ->find($commandName);

        } catch (\Exception $e) {
            $messageFormat = 'Try to get command "%s" but could not found it.';
            $message = sprintf($messageFormat, $commandName);
            $this->writelnError($message);
        }

        return $command;
    }

    /**
     * @param string $commandName
     * @param array $arguments
     */
    protected function runCommand(string $commandName, array $arguments)
    {
        $arguments = array_merge(
            array(
                'command' => $commandName,
                '--chained' => true,
            ),
            $arguments
        );

        $arrayInput = new ArrayInput($arguments);
        $this
            ->getCommand($commandName)
            ->run($arrayInput, $this->output);
    }


    /**
     * @param string $title
     * @return AbstractCommand
     */
    protected function writelnTitle(string $title): AbstractCommand
    {
        if ($this->isChained) {
            $titleFormat = 'EXECUTING COMMAND : <title>%s</title>';
            $titleMessage = sprintf($titleFormat, trim($title));
            $this->writeln($titleMessage);

        } else {
            $titleFormat = '<title> %s </title>';
            $titleMessage = sprintf($titleFormat, trim($title));
            $line = sprintf($titleFormat, self::createLine($title));
            $this->writelns([$line, $titleMessage, $line]);
        }

        return $this;
    }

    /**
     * @param string $successMessage
     * @return AbstractCommand
     */
    protected function writelnSuccess(string $successMessage): AbstractCommand
    {
        $errorFormat = '<green>%s</green>';
        $message = sprintf($errorFormat, $successMessage);
        return $this->writeln($message);
    }

    /**
     * @param string $errorMessage
     * @return AbstractCommand
     */
    protected function writelnError(string $errorMessage): AbstractCommand
    {
        $errorFormat = '<error>Error : %s</error>';
        $message = sprintf($errorFormat, $errorMessage);
        $this->writeln($message)->numberOfErrors++;
        return $this;
    }

    /**
     * @param array $messages
     * @return AbstractCommand
     */
    protected function writelns(array $messages):AbstractCommand
    {
        foreach ($messages as $message) {
            $this->writeln($message);
        }
        return $this;
    }

    /**
     * @param string $message
     * @param bool $timestamped
     * @return AbstractCommand
     */
    protected function writeln(string $message, bool $timestamped = true):AbstractCommand
    {
        if (!$this->isQuiteMode) {
            if ($timestamped) {
                $date = new \DateTime();
                $timestamp = $date->format('[d-m-Y H:i:s]');
                $message = $timestamp . ' ' . $message;
            }

            $this->output->writeln($message);
        }
        return $this;
    }

}