<?php
namespace App\Command;

use App\Service\FetchNews;
use App\Service\StoreNewsItemService;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class SyncNewsCommand extends Command
{
    /**
     * @var StoreNewsItemService
     */
    private $storeNewsItemService;

    public function __construct(StoreNewsItemService $storeNewsItemService)
    {
        $this->storeNewsItemService = $storeNewsItemService;

        parent::__construct();
    }

    protected function configure()
    {
        $this
            // the name of the command (the part after "bin/console")
            ->setName('app:sync-news')

            // the short description shown while running "php bin/console list"
            ->setDescription('Sync news items.')

            // the full command description shown when running the command with
            // the "--help" option
            ->setHelp('This command synchronises the news from zorganderstv.be')

            ->addArgument('page', InputArgument::REQUIRED, 'The news page that needs to be synced.')

        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output->writeln([
            'Sync News Command',
            '============',
            '',
        ]);

        $page = (int)$input->getArgument('page');

        $output->writeln('Syncing page ' . $page);

        $newsItems = FetchNews::fetchNewsPage($page);

        foreach ($newsItems as $newsItem) {
            $savedNewsItem = $this->storeNewsItemService->storeIfDoesNotExist($newsItem);
            $output->writeln([
                'Synced : ' . $savedNewsItem->getTitle(),
                'ID: ' . $savedNewsItem->getId(),
                '----'
            ]);
        }

        $output->writeln([
            'DONE --- DONE',
            '======'
        ]);
    }
}