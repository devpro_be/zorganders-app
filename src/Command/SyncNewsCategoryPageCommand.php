<?php
namespace App\Command;

use \Exception;
use App\Entity\NewsCategory;
use App\Entity\NewsItem;
use App\Service\FetchNews;
use App\Service\StoreNewsItemService;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class SyncNewsCategoryPageCommand extends Command
{
    /**
     * @var StoreNewsItemService
     */
    private $storeNewsItemService;

    const OUTPUT_BREAK = '------';

    public function __construct(StoreNewsItemService $storeNewsItemService)
    {
        $this->storeNewsItemService = $storeNewsItemService;

        parent::__construct();
    }

    protected function configure()
    {
        $this
            // the name of the command (the part after "bin/console")
            ->setName('app:sync-news-category-page')

            // the short description shown while running "php bin/console list"
            ->setDescription('Sync a certain page of a certain news category.')

            // the full command description shown when running the command with
            // the "--help" option
            ->setHelp('This command synchronises the news categories from zorganderstv.be')

            ->addArgument('page', InputArgument::REQUIRED, 'The category page that needs to be synced.')

            ->addArgument('categoryId', InputArgument::REQUIRED, 'The category id that needs to be synced.')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output->writeln([
            'Sync News Category Page Command',
            '====================================',
            '',
        ]);

        $page = (int)$input->getArgument('page');
        $categoryId = (int)$input->getArgument('categoryId');

        $newsCategory = $this->storeNewsItemService->getNewsCategory($categoryId);

        $output->writeln([
            'COMMAND INPUT:',
            '°°°°°°°°°°°°°°',
            'page : ' . $page,
            'categoryId : ' . $categoryId,
            self::OUTPUT_BREAK,
        ]);

        try {
            // Cancel if category could not be found.
            if (!$newsCategory instanceof NewsCategory) {
                $message = sprintf('No news category found with id %s', $categoryId);
                throw new Exception($message);
            }

            // Let console known we found the category.
            $message = sprintf('Found category "%s" going to scrape page %s', $newsCategory->getTitle(), $page);
            $output->writeln($message);


            // Fetch newsItems on zorganderstv.be for matched category.
            $newsItems = FetchNews::fetchTaxonomyTermPage(
                $newsCategory->getTaxonomyTerm(),
                $page
            );

            $index = 1;
            $countItems = count($newsItems);

            // Cancel when no news items where found.
            if ($countItems === 0) {
                $message = sprintf('No news items found for category "%s" on page %s', $newsCategory->getTitle(), $page);
                throw new Exception($message);
            }

            $message = sprintf('Found %s news items to sync', $countItems);
            $output->writeln([
                $message,
                self::OUTPUT_BREAK,
            ]);

            foreach ($newsItems as $newsItem) {
                // Let console know we have found a item
                $message = sprintf('%s of %s - Going to synchronise news item "%s"', $index, $countItems, $newsItem->getTitle());
                $output->writeln($message);

                // Check if we got this item already in our database or create a new one.
                $existingNewsItem = $this->storeNewsItemService->storeIfDoesNotExist($newsItem);

                // Check if news item isn'
                $existingNewsCategory = $existingNewsItem->getCategory();
                if (
                    $existingNewsCategory instanceof NewsCategory
                    &&
                    $existingNewsCategory->getId() === $newsCategory->getId()
                ) {
                    $output->writeln('News item already bound to current category.');
                } else {
                    $existingNewsItem->setCategory($newsCategory);
                    $this->storeNewsItemService->update($existingNewsItem);
                    $output->writeln('News item is now bound to the category.');
                }
                $output->writeln(self::OUTPUT_BREAK);

                $index++;
            }

        } catch (Exception $e) {
            $output->writeln([
                'Sync canceled : ' . $e->getMessage(),
                self::OUTPUT_BREAK,
            ]);
        }

        $output->writeln([
            'DONE --- DONE',
            '======'
        ]);
    }
}