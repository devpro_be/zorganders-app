<?php
namespace App\Command\SyncCategories\Articles;

use App\Command\AbstractCommand;
use App\Firebase\ArticlesFirebase;
use App\Google\Firestore\ArticleCategoriesFirestore;
use App\Google\Firestore\ArticleRootCategoriesFirestore;
use App\Google\Firestore\ArticlesFirestore;
use App\Google\Firestore\MagazinesFirestore;
use App\Model\ArticleModel;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputArgument;

class SyncMagazinesArticlesCommand extends AbstractCommand
{

    const COMMAND_NAME = 'sync-magazines:articles:all';
    const COMMAND_DESCRIPTION = 'Sync all the articles with their magazines.';
    const COMMAND_HELP = 'This command synchronises the magazines of all the articles in firestore.';

    /**
     * @var MagazinesFirestore
     */
    protected $magazinesFirestore;

    /**
     * @var ArticleCategoriesFirestore
     */
    protected $articlesCategoriesFirestore;

    /**
     * @var ArticlesFirestore
     */
    protected $articlesFirestore;

    protected function configure()
    {
        parent::setup(
            self::COMMAND_NAME,
            self::COMMAND_DESCRIPTION,
            self::COMMAND_HELP
        );

        $this->magazinesFirestore = new MagazinesFirestore();
        $this->articlesCategoriesFirestore = new ArticleCategoriesFirestore();
        $this->articlesFirestore = new ArticlesFirestore();
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int
     */
    protected function execute(InputInterface $input, OutputInterface $output):int
    {
        parent::execute($input, $output);


        $magazine = 'ZORG-MAGAZINE';

        $categories = $this->magazinesFirestore->getAllCategories($magazine);

        $formatMessage = 'Found <green>%d</green> categories for magazine "<blue>%s</blue>"';
        $message = sprintf($formatMessage, count($categories), $magazine);
        $this->writeln($message);

        $totalCount = 0;

        foreach ($categories as $category) {

            $formatMessage = 'Going to add magazine for articles with category "<blue>%s</blue>"';
            $message = sprintf($formatMessage, $category);
            $this->writeln($message);

            $articleModels = $this->articlesFirestore->getAllWithCategory($category);

            $formatMessage = 'Found <green>%d</green> articles for the category';
            $message = sprintf($formatMessage, count($articleModels));
            $this->writeln($message);

            foreach ($articleModels as $articleModel) {
                $totalCount++;
                $formatMessage = 'Got article "<blue>%s</blue>"';
                $message = sprintf($formatMessage, $articleModel->getId());
                $this->writeln($message);

                if (!$articleModel->hasMagazine($magazine)) {
                    $articleModel->addMagazine($magazine);
                    $this->articlesFirestore->save($articleModel);
                    $this->writeln('added magazine');
                } else {
                    $this->writeln('article has already magazine');
                }

            }
        }

        $this->writelnSuccess('done');

        return 0;
    }
}