<?php
namespace App\Command\SyncCategories\Articles;

use App\Command\AbstractCommand;
use App\Firebase\ArticlesFirebase;
use App\Google\Firestore\ArticleCategoriesFirestore;
use App\Google\Firestore\ArticleRootCategoriesFirestore;
use App\Google\Firestore\ArticlesFirestore;
use App\Model\ArticleModel;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputArgument;

class SyncCategoriesArticlesCommand extends AbstractCommand
{

    const COMMAND_NAME = 'sync-categories:articles:all';
    const COMMAND_DESCRIPTION = 'Sync all the article their root categories.';
    const COMMAND_HELP = 'This command synchronises the root categories of all the articles in firestore.';

    /**
     * @var ArticleRootCategoriesFirestore
     */
    protected $articlesRootCategoriesFirestore;

    /**
     * @var ArticleCategoriesFirestore
     */
    protected $articlesCategoriesFirestore;

    /**
     * @var ArticlesFirestore
     */
    protected $articlesFirestore;

    protected function configure()
    {
        parent::setup(
            self::COMMAND_NAME,
            self::COMMAND_DESCRIPTION,
            self::COMMAND_HELP
        );

        $this->articlesRootCategoriesFirestore = new ArticleRootCategoriesFirestore();
        $this->articlesCategoriesFirestore = new ArticleCategoriesFirestore();
        $this->articlesFirestore = new ArticlesFirestore();
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int
     */
    protected function execute(InputInterface $input, OutputInterface $output):int
    {
        parent::execute($input, $output);


        $rootCategory = 'MANAGEMENT-EN-FINANCE';

        $categories = $this->articlesRootCategoriesFirestore->getAllCategories($rootCategory);

        $formatMessage = 'Found <green>%d</green> categories for root category "<blue>%s</blue>"';
        $message = sprintf($formatMessage, count($categories), $rootCategory);
        $this->writeln($message);

        $totalCount = 0;

        foreach ($categories as $category) {

            $formatMessage = 'Going to add root category for articles with category "<blue>%s</blue>"';
            $message = sprintf($formatMessage, $category);
            $this->writeln($message);

            $articleModels = $this->articlesFirestore->getAllWithCategory($category);

            $formatMessage = 'Found <green>%d</green> articles for the category';
            $message = sprintf($formatMessage, count($articleModels));
            $this->writeln($message);

            foreach ($articleModels as $articleModel) {
                $totalCount++;
                $formatMessage = 'Got article "<blue>%s</blue>"';
                $message = sprintf($formatMessage, $articleModel->getId());
                $this->writeln($message);

                if (!$articleModel->hasRootCategory($rootCategory)) {
                    $articleModel->addRootCategory($rootCategory);
                    $this->articlesFirestore->save($articleModel);
                    $this->writeln('added root category');
                } else {
                    $this->writeln('article has already root category');
                }

            }
        }


        $this->writelnSuccess('done');

        return 0;
    }
}