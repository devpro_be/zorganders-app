<?php
namespace App\Command\SyncActualcare\Article;

use App\Command\AbstractCommand;
use App\Firebase\ArticlesFirebase;
use App\Google\Firestore\ArticlesFirestore;
use App\Model\ArticleModel;
use App\Scraper\ActualCare\ArticleActualCareScraper;
use App\Scraper\ActualCare\ArticleCategoryActualCareScraper;
use Symfony\Bundle\FrameworkBundle\Tests\Fixtures\Validation\Article;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputArgument;

class Body extends AbstractCommand
{

    const COMMAND_NAME = 'sync-actualcare:article:body';
    const COMMAND_DESCRIPTION = 'Sync the body of the article from the actualcare site.';
    const COMMAND_HELP = 'This command synchronises the body of the article from the actualcare site.';

    const COMMAND_ARGUMENT_ARTICLE_ID = 'articleId';

    /**
     * @var ArticlesFirebase
     */
    protected $articlesFirebase;

    /**
     * @var ArticlesFirestore
     */
    protected $articlesFirestore;

    protected function configure()
    {
        parent::setup(
            self::COMMAND_NAME,
            self::COMMAND_DESCRIPTION,
            self::COMMAND_HELP
        )
            ->addArgument(
                self::COMMAND_ARGUMENT_ARTICLE_ID,
                InputArgument::REQUIRED,
                'The id of the category where the articles need to be scraped from.'
            )
        ;
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        parent::execute($input, $output);

        $this->articlesFirebase = new ArticlesFirebase();
        $this->articlesFirestore = new ArticlesFirestore();

        $articleId = $input->getArgument(self::COMMAND_ARGUMENT_ARTICLE_ID);

        $titleFormat = 'Sync the body content of article "%s"';
        $title = sprintf($titleFormat, $articleId);
        $this->writelnTitle($title);

        $formatContext = 'article "<blue>%s</blue>"';
        $context = sprintf($formatContext, $articleId);

        $this->writelnFetchingFromFirebase($context);
        /** @var ArticleModel $articleModel */
        $articleModel = $this->articlesFirestore->getById($articleId);
        $this->writelnFetchedFromFirebase($context);


        if ($articleModel->getHasBody()) {
            $this->writeln('Body of article already scraped...');
        } else {
            try {
                $this->writeln('Scraping article content from actualcare site...');
                $pageContent = ArticleActualCareScraper::getArticleBody($articleModel);
                $this->writeln('Scraped the article content.');

                $this->writelnStoringIntoFirebase($context);
                $this->articlesFirebase->storeIntoBucket($articleModel->getId(), $pageContent);
                $this->writelnStoredIntoFirebase($context);

                $this->writelnSavingIntoFirestore($context);
                $articleModel->setHasBody(true);
                $this->articlesFirestore->save($articleModel);
                $this->writelnSavedIntoFirestore($context);
            } catch (\Exception $e) {
                // Swallow errors, e.g. url not reachable...
                $this->writelnError($e->getMessage());
            }
        }

        $this->writelnSuccess('done completely');

        return 0;
    }
}