<?php
namespace App\Command\SyncActualcare\Article;

use App\Command\AbstractCommand;
use App\Firebase\ArticlesFirebase;
use App\Google\Firestore\ArticlesFirestore;
use App\Model\ArticleModel;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputArgument;

class All extends AbstractCommand
{

    const COMMAND_NAME = 'sync-actualcare:article:all';
    const COMMAND_DESCRIPTION = 'Sync all the article details from the actualcare environment.';
    const COMMAND_HELP = 'This command synchronises the details of the articles on the actualcare site.';

    const COMMAND_ARGUMENT_ARTICLE_ID = 'articleId';

    /**
     * @var ArticlesFirebase
     */
    protected $articleFirebase;

    /**
     * @var ArticlesFirestore
     */
    protected $articlesFirestore;

    protected function configure()
    {
        parent::setup(
            self::COMMAND_NAME,
            self::COMMAND_DESCRIPTION,
            self::COMMAND_HELP
        )
            ->addArgument(
                self::COMMAND_ARGUMENT_ARTICLE_ID,
                InputArgument::OPTIONAL,
                'Optional: the article id of a certain article that needs to be synced.'
            )
        ;

        $this->articlesFirestore = new ArticlesFirestore();
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int
     */
    protected function execute(InputInterface $input, OutputInterface $output):int
    {
        parent::execute($input, $output);

        $articleId = $input->getArgument(self::COMMAND_ARGUMENT_ARTICLE_ID);

        if ($articleId) {
            $this->executeForOneArticle($articleId);
        } else {
            $this->executeForAllArticles();
        }

        return 0;
    }

    /**
     * @param string $articleId
     */
    private function executeForOneArticle(string $articleId)
    {
        $formatTitle = 'Sync details for article "%s"';
        $title = sprintf($formatTitle, $articleId);
        $this->writelnTitle($title);

        /** @var ArticleModel $articleModel */
        $articleModel = $this->articlesFirestore->getById($articleId);

        $this->syncForOneArticle($articleModel);
    }

    private function executeForAllArticles()
    {
        $this->writelnTitle('Sync all for all articles stored on firestore.');

        $articleModels = $this->articlesFirestore->getAllFromSource(ArticlesFirestore::SOURCE_ACTUALCARE);

        foreach ($articleModels as $articleModel) {
            $formatMessage = 'Will try to sync article "<green>%s</green>".';
            $message = sprintf($formatMessage, $articleModel->getId());
            $this->writeln($message);
            $this->syncForOneArticle($articleModel);
        }
    }


    /**
     * @param ArticleModel $articleModel
     */
    private function syncForOneArticle(ArticleModel $articleModel)
    {

        if ($articleModel->getId() === null) {
            $this->writelnError('This category does not exists in firebase or has no valid URL set.');
        } else {
            $this
                ->executeImage($articleModel)
                ->executeBody($articleModel);
        }
    }

    /**
     * @param ArticleModel $articleModel
     * @return All
     */
    protected function executeBody(ArticleModel $articleModel):All
    {
        $commandName = Body::COMMAND_NAME;
        $arguments = array(
            Body::COMMAND_ARGUMENT_ARTICLE_ID => $articleModel->getId(),
        );
        $this->runCommand($commandName, $arguments);
        return $this;
    }

    /**
     * @param ArticleModel $articleModel
     * @return All
     */
    protected function executeImage(ArticleModel $articleModel):All
    {
        $commandName = Image::COMMAND_NAME;
        $arguments = array(
            Image::COMMAND_ARGUMENT_ARTICLE_ID => $articleModel->getId(),
        );
        $this->runCommand($commandName, $arguments);
        return $this;
    }
}