<?php

namespace App\Command\SyncActualcare\Article;

use App\Command\AbstractCommand;
use App\Firebase\ArticlesFirebase;
use App\Google\Firestore\ArticlesFirestore;
use App\Model\ArticleModel;
use App\Scraper\ActualCare\ArticleActualCareScraper;
use App\Scraper\ActualCare\ArticleCategoryActualCareScraper;
use Symfony\Bundle\FrameworkBundle\Tests\Fixtures\Validation\Article;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputArgument;

class Image extends AbstractCommand
{

    const COMMAND_NAME = 'sync-actualcare:article:image';
    const COMMAND_DESCRIPTION = 'Sync the image of the article from the actualcare site.';
    const COMMAND_HELP = 'This command synchronises the image of the article from the actualcare site.';

    const COMMAND_ARGUMENT_ARTICLE_ID = 'articleId';

    /**
     * @var ArticlesFirebase
     */
    protected $articlesFirebase;

    /**
     * @var ArticlesFirestore
     */
    protected $articlesFirestore;

    protected function configure()
    {
        parent::setup(
            self::COMMAND_NAME,
            self::COMMAND_DESCRIPTION,
            self::COMMAND_HELP
        )
            ->addArgument(
                self::COMMAND_ARGUMENT_ARTICLE_ID,
                InputArgument::REQUIRED,
                'The id of the category where the articles need to be scraped from.'
            );
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        parent::execute($input, $output);

        // Large images need more than the normal memory limit.
        ini_set('memory_limit', '250M');

        $this->articlesFirebase = new ArticlesFirebase();
        $this->articlesFirestore = new ArticlesFirestore();

        $articleId = $input->getArgument(self::COMMAND_ARGUMENT_ARTICLE_ID);

        $titleFormat = 'Sync the image of article "%s"';
        $title = sprintf($titleFormat, $articleId);
        $this->writelnTitle($title);

        $formatContext = 'article "<blue>%s</blue>"';
        $context = sprintf($formatContext, $articleId);

        $this->writelnFetchingFromFirebase($context);
        /** @var ArticleModel $articleModel */
        $articleModel = $this->articlesFirestore->getById($articleId);
        $this->writelnFetchedFromFirebase($context);

        if (!$this->isForceMode && $articleModel->getHasImage()) {
            $this->writeln('Skipping scraping article image, already stored.');
        } else {

            try {
                $this->writeln('Scraping article image from actualcare site and storing it on firebase...');
                ArticleActualCareScraper::scrapeArticleImage($articleModel);
                $this->writeln('Scraped the article image and stored on firebase.');

                $this->writelnSavingIntoFirestore($context);
                $this->articlesFirestore->save($articleModel);
                $this->writelnSavedIntoFirestore($context);
            } catch (\Exception $e) {
                // Swallow errors, e.g. page isn't reachable.
                $this->writelnError($e->getMessage());
            }
        }

        $this->writelnSuccess('done completely');

        return 0;
    }
}