<?php
namespace App\Command\SyncActualcare\ArticleCategory;

use App\Command\AbstractCommand;
use App\Firebase\ArticleCategoriesFirebase;
use App\Google\Firestore\ArticleCategoriesFirestore;
use App\Model\ArticleCategoryModel;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputArgument;

class All extends AbstractCommand
{

    const COMMAND_NAME = 'sync-actualcare:article-category:all';
    const COMMAND_DESCRIPTION = 'Sync number of pages and articles from a certain category of the actualcare site.';
    const COMMAND_HELP = 'This command synchronises the number of pages and articles from a certain category of the actualcare site.';

    const COMMAND_ARGUMENT_CATEGORY_ID = 'categoryId';
    const COMMAND_OPTION_SKIP_TO_PAGE = 'skip-to-page';


    /**
     * @var int
     */
    protected $startPage = 1;

    /**
     * @var ArticleCategoriesFirebase
     */
    protected $articleCategoriesFirebase;


    /**
     * @var ArticleCategoriesFirestore
     */
    protected $articleCategoriesFirestore;

    protected function configure()
    {
        parent::setup(
            self::COMMAND_NAME,
            self::COMMAND_DESCRIPTION,
            self::COMMAND_HELP
        )
            ->addArgument(
                self::COMMAND_ARGUMENT_CATEGORY_ID,
                InputArgument::OPTIONAL,
                'Optional: the category id of a certain category that needs to be scraped.'
            )

            ->addOption(
                self::COMMAND_OPTION_SKIP_TO_PAGE,
                null,
                InputOption::VALUE_OPTIONAL,
                'Skip sync process to certain page, if that page exists...',
                1
            )
        ;

        $this->articleCategoriesFirestore = new ArticleCategoriesFirestore();
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int
     */
    protected function execute(InputInterface $input, OutputInterface $output):int
    {
        parent::execute($input, $output);

        $categoryId = $input->getArgument(self::COMMAND_ARGUMENT_CATEGORY_ID);
        $this->startPage = (int)$input->getOption(self::COMMAND_OPTION_SKIP_TO_PAGE);

        if ($categoryId) {
            $this->executeForOneCategory($categoryId);
        } else {
            $this->executeForAllCategories();
        }

        return 0;
    }

    /**
     * @param string $categoryId
     */
    private function executeForOneCategory(string $categoryId)
    {
        $formatTitle = 'Sync all for category "%s"';
        $title = sprintf($formatTitle, $categoryId);
        $this->writelnTitle($title);

        $categoryModel = $this->getCategoryFromFirebase($categoryId);

        $this->syncForOneCategory($categoryModel);
    }

    private function executeForAllCategories()
    {
        $this->writelnTitle('Sync all for all categories stored in firebase.');

        $categoryModels = $this
            ->articleCategoriesFirestore
            ->getAllFromSource(ArticleCategoriesFirestore::SOURCE_ACTUALCARE);

        foreach ($categoryModels as $categoryModel) {
            $formatMessage = 'Will try to sync for category "<green>%s</green>".';
            $message = sprintf($formatMessage, $categoryModel->getId());
            $this->writeln($message);
            $this->syncForOneCategory($categoryModel);
        }
    }

    /**
     * @param string $categoryId
     * @return ArticleCategoryModel
     */
    private function getCategoryFromFirebase(string $categoryId):ArticleCategoryModel
    {
        $formatMessage = 'article category "%s"';
        $firebaseContext = sprintf($formatMessage, $categoryId);

        $this->writelnFetchingFromFirebase($firebaseContext);

        /** @var ArticleCategoryModel $categoryModel */
        $categoryModel = $this->articleCategoriesFirestore->getById($categoryId);

        $this->writelnFetchedFromFirebase($firebaseContext);

        return $categoryModel;
    }

    /**
     * @return ArticleCategoryModel[]
     */
    private function getAllCategoriesFromFirebase():array
    {
        $firebaseContext = '<blue>all</blue> article category';
        $this->writelnFetchingFromFirebase($firebaseContext);
        $categoryModels = $this->articleCategoriesFirebase->getAllCategoriesFromActualcare();
        $this->writelnFetchedFromFirebase($firebaseContext);
        $this->listAllFetchedCategoriesInConsole($categoryModels);
        return $categoryModels;
    }

    /**
     * @param ArticleCategoryModel[] $categoryModels
     */
    private function listAllFetchedCategoriesInConsole(array $categoryModels) {
        $formatMessage = 'Found <green>%d</green> categories in firebase:';
        $message = sprintf($formatMessage, count($categoryModels));
        $this->writeln($message);

        $messageListOfCategories = [];
        $formatMessage = '- <green>%s</green>';
        foreach ($categoryModels as $categoryModel) {
            $message = sprintf($formatMessage, $categoryModel->getId());
            $messageListOfCategories[] = $message;
        }

        $this->writelns($messageListOfCategories);
    }

    /**
     * @param ArticleCategoryModel $categoryModel
     */
    private function syncForOneCategory(ArticleCategoryModel $categoryModel)
    {

        if ($categoryModel->getUrl() === null) {
            $this->writelnError('This category does not exists in firebase or has no valid URL set.');
        } else {

            $this
                ->executeNumberOfPages($categoryModel)
                ->executeArticles($categoryModel);
        }
    }

    /**
     * @param ArticleCategoryModel $categoryModel
     * @return All
     */
    protected function executeNumberOfPages(ArticleCategoryModel $categoryModel):All
    {
        $commandName = NumberOfPages::COMMAND_NAME;
        $arguments = array(
            NumberOfPages::ARGUMENT_CATEGORY_ID => $categoryModel->getId(),
        );
        $this->runCommand($commandName, $arguments);
        return $this;
    }

    /**
     * @param ArticleCategoryModel $categoryModel
     */
    protected function executeArticles(ArticleCategoryModel $categoryModel) {

        $commandName = ArticlesOnPage::COMMAND_NAME;

        /** @var ArticleCategoryModel $categoryModel */
        $categoryModel = $this
            ->articleCategoriesFirestore
            ->getById($categoryModel->getId())
        ;

        $numberOfPages = $categoryModel->getNumberOfPages();


        $pageOffset = ($numberOfPages > $this->startPage) ? $this->startPage : $numberOfPages;

        if ($numberOfPages === 0 || $numberOfPages === null) {
           $this->writeln('No pages for the category.');
        } else {
            for ($i=$pageOffset; $i <= $numberOfPages; $i++) {
                $arguments = array(
                    ArticlesOnPage::COMMAND_ARGUMENT_CATEGORY_ID  => $categoryModel->getId(),
                    ArticlesOnPage::COMMAND_ARGUMENT_PAGE => $i,
                );
                $this->runCommand($commandName, $arguments);
            }
        }


    }
}