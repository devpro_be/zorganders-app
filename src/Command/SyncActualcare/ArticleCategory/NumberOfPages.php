<?php
namespace App\Command\SyncActualcare\ArticleCategory;

use App\Command\AbstractCommand;
use App\Firebase\ArticleCategoriesFirebase;
use App\Google\Firestore\ArticleCategoriesFirestore;
use App\Model\ArticleCategoryModel;
use App\Scraper\ActualCare\ArticleCategoryActualCareScraper;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class NumberOfPages extends AbstractCommand
{
    const OUTPUT_BREAK = '------';

    const COMMAND_NAME = 'sync-actualcare:article-category:number-of-pages';
    const ARGUMENT_CATEGORY_ID = 'categoryId';

    /**
     * @var ArticleCategoriesFirestore
     */
    protected $articleCategoriesFirestore;

    protected function configure()
    {
        parent::setup(
            self::COMMAND_NAME,
            self::COMMAND_DESCRIPTION,
            self::COMMAND_HELP
        )
            ->addArgument(
                self::ARGUMENT_CATEGORY_ID,
                InputArgument::OPTIONAL,
                'Optional: add category for only updating for a certain category.'
            )
        ;

        $this->articleCategoriesFirestore = new ArticleCategoriesFirestore();
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int
     */
    protected function execute(InputInterface $input, OutputInterface $output):int
    {
        parent::execute($input, $output);

        $categoryId = $input->getArgument(self::ARGUMENT_CATEGORY_ID);

        if ($categoryId) {
            $this->updateCategory($categoryId);
        } else {
            $this->updateAllCategories();
        }

        return 0;
    }

    /**
     * @param ArticleCategoryModel $categoryModel
     * @return NumberOfPages
     */
    private function scrapeNumberOfPagesForCategory(ArticleCategoryModel $categoryModel): NumberOfPages
    {

        $numberOfPages = ArticleCategoryActualCareScraper::getNumberOfPagesForCategory($categoryModel);
        $categoryModel->setNumberOfPages($numberOfPages);

        $formatMessage = 'Found <blue>%d</blue> page(s) for the category "<blue>%s</blue>".';
        $message = sprintf($formatMessage, $numberOfPages, $categoryModel->getId());
        $this->writeln($message);

        $firebaseContext = 'the updated category';

        $this->writelnStoringIntoFirebase($firebaseContext);

        $this->articleCategoriesFirestore->save($categoryModel);

        $this->writelnStoredIntoFirebase($firebaseContext);

        return $this;
    }

    private function updateCategory(string $categoryId)
    {
        /** @var ArticleCategoryModel $categoryModel */
        $categoryModel = $this->articleCategoriesFirestore->getById($categoryId);

        if ($categoryModel->getUrl() === null) {
            $this->writelnError('The category doens\'t exists in firebase or hasn\'t got a valid value for the url.');
        } else {
            $this->scrapeNumberOfPagesForCategory($categoryModel);
        }
    }

    private function updateAllCategories() {

        $this->writelnTitle('Sync the number of pages for all articles categories on the actualcare site.');

        $articleCategoriesFirebase = new ArticleCategoriesFirebase();
        $categoryModels = $articleCategoriesFirebase->getAllCategoriesFromActualcare();
        $numberOfCategories = count($categoryModels);
        $numberOfCategoriesFoundFormat = 'Found <green>%s</green> categories on firebase for actual care';
        $message = sprintf($numberOfCategoriesFoundFormat, $numberOfCategories);
        $this->writeln($message);

        foreach ($categoryModels as $categoryModel) {

            $messageFormat = 'Scraping number of pages for "<green>%s</green>"';
            $message = sprintf($messageFormat, $categoryModel->getId());
            $this->writeln($message);

            $this->scrapeNumberOfPagesForCategory(
                $categoryModel,
                $articleCategoriesFirebase
            );

        }

        $this->writelnSuccess('Done: completed all the categories from firebase.');
    }
}