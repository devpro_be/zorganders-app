<?php
namespace App\Command\SyncActualcare\ArticleCategory;

use App\Command\AbstractCommand;
use App\Google\Firestore\ArticleCategoriesFirestore;
use App\Google\Firestore\ArticlesFirestore;
use App\Model\ArticleCategoryModel;
use App\Model\ArticleModel;
use App\Scraper\ActualCare\ArticleCategoryActualCareScraper;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputArgument;
use App\Command\SyncActualcare\Article\All as CommandArticleAll;


class ArticlesOnPage extends AbstractCommand
{

    const COMMAND_NAME = 'sync-actualcare:article-category:articles';
    const COMMAND_DESCRIPTION = 'Sync the articles of each article category on the actualcare site.';
    const COMMAND_HELP = 'This command synchronises all the articles of a certain category and page';

    const COMMAND_ARGUMENT_CATEGORY_ID = 'categoryId';
    const COMMAND_ARGUMENT_PAGE = 'page';
    const COMMAND_ARGUMENT_SKIP_TO_PAGE = 'skip-to-page';

    /**
     * @var ArticlesFirestore
     */
    protected $articlesFirestore;

    /**
     * @var ArticleCategoriesFirestore
     */
    protected $articleCategoriesFirestore;

    protected function configure()
    {
        parent::setup(
            self::COMMAND_NAME,
            self::COMMAND_DESCRIPTION,
            self::COMMAND_HELP
        )
            ->addArgument(
                self::COMMAND_ARGUMENT_CATEGORY_ID,
                InputArgument::REQUIRED,
                'The id of the category where the articles need to be scraped from.')

            ->addArgument(
                self::COMMAND_ARGUMENT_PAGE,
                InputArgument::REQUIRED,
                'The page of the category that needs to be synced.'
            )
        ;

        $this->articlesFirestore = new ArticlesFirestore();
        $this->articleCategoriesFirestore = new ArticleCategoriesFirestore();
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int
     */
    protected function execute(InputInterface $input, OutputInterface $output):int
    {
        parent::execute($input, $output);


        $page = (int)$input->getArgument(self::COMMAND_ARGUMENT_PAGE);
        $categoryId = $input->getArgument(self::COMMAND_ARGUMENT_CATEGORY_ID);

        $this->writelnTitle('Going to sync the articles for a certain category');


        $messageFormat = 'Will scrape page <green>%d</green> of the category "<green>%s</green>"';
        $message = sprintf($messageFormat, $page, $categoryId);
        $this->writeln($message);


        $formatFetchContext = 'article category "<blue>%s</blue>"';
        $fetchContext = sprintf($formatFetchContext, $categoryId);

        $this->writelnFetchingFromFirebase($fetchContext);
        /** @var ArticleCategoryModel $categoryModel */
        $categoryModel = $this->articleCategoriesFirestore->getById($categoryId);
        $this->writelnFetchedFromFirebase($fetchContext);

        $this->writeln('Scraping the articles from the page...');
        $articleModels = ArticleCategoryActualCareScraper::getArticlesForCategoryOnPage($categoryModel, $page);

        $messageFormat = 'Found <green>%s</green> article(s) on the page.';
        $message = sprintf($messageFormat, count($articleModels));
        $this->writeln($message);

        $this->storeArticleModelsIntoFirebase($articleModels);

        $this->collectArticlesDetails($articleModels);

        $this->writelnSuccess('done completely');

        return 0;
    }

    /**
     * @param ArticleModel[] $articleModels
     */
    private function collectArticlesDetails(array $articleModels) {
        foreach ($articleModels as $articleModel) {
            $this->collectArticleDetails($articleModel);
        }
    }

    /**
     * @param ArticleModel[] $articleModels
     */
    private function storeArticleModelsIntoFirebase(array $articleModels)
    {
       $formatStoreContext = 'article "<green>%s</green>"';

        foreach ($articleModels as $articleModel) {
            $storeContext = sprintf($formatStoreContext, $articleModel->getId());
            $this->writelnStoringIntoFirebase($storeContext);
            $this->articlesFirestore->save($articleModel);
            $this->writelnStoredIntoFirebase($storeContext);
        }
    }

    private function collectArticleDetails(ArticleModel $articleModel) {
        $commandName = CommandArticleAll::COMMAND_NAME;
        $arguments = array(
            CommandArticleAll::COMMAND_ARGUMENT_ARTICLE_ID => $articleModel->getId(),
        );
        $this->runCommand($commandName, $arguments);
        return $this;
    }
}