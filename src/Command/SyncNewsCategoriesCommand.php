<?php
namespace App\Command;

use App\Service\NewsCategoriesService;
use \Exception;
use App\Entity\NewsCategory;
use App\Entity\NewsItem;
use App\Service\FetchNews;
use App\Service\StoreNewsItemService;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class SyncNewsCategoriesCommand extends Command
{
    /**
     * @var NewsCategoriesService
     */
    private $newsCategoriesService;

    const OUTPUT_BREAK = '------';

    public function __construct(NewsCategoriesService $newsCategoriesService)
    {
        $this->newsCategoriesService = $newsCategoriesService;

        parent::__construct();
    }

    protected function configure()
    {
        $this
            // the name of the command (the part after "bin/console")
            ->setName('app:sync-news-categories')

            // the short description shown while running "php bin/console list"
            ->setDescription('Sync all news categories.')

            // the full command description shown when running the command with
            // the "--help" option
            ->setHelp('This command synchronises the news categories from zorganderstv.be')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output->writeln([
            'Sync News Categories Page Command',
            '====================================',
            '',
        ]);

        $newsCategories = $this->newsCategoriesService->getCategories();

        $message = sprintf('Found %s categories in database', count($newsCategories));
        $output->writeln($message);

        foreach ($newsCategories as $newsCategory) {
            $message = sprintf('Going to sync category "%s"', $newsCategory->getTitle());
            $output->writeln($message);
        }

    }
}