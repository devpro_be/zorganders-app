<?php
namespace App\Command;

use App\Service\FetchReports;
use App\Service\StoreReportsItemService;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class SyncReportsCommand extends Command
{
    /**
     * @var StoreReportsItemService
     */
    private $storeReportsItemService;

    public function __construct(StoreReportsItemService $storeReportsItemService)
    {
        $this->storeReportsItemService = $storeReportsItemService;

        parent::__construct();
    }

    protected function configure()
    {
        $this
            // the name of the command (the part after "bin/console")
            ->setName('app:sync-reports')

            // the short description shown while running "php bin/console list"
            ->setDescription('Sync reports items.')

            // the full command description shown when running the command with
            // the "--help" option
            ->setHelp('This command synchronises the reports from zorganderstv.be')

            ->addArgument('page', InputArgument::REQUIRED, 'The news page that needs to be synced.')

        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output->writeln([
            'Sync Reports Command',
            '============',
            '',
        ]);

        $page = (int)$input->getArgument('page');

        $output->writeln('Syncing page ' . $page);

        $newsItems = FetchReports::fetchReportsPage($page);

        foreach ($newsItems as $newsItem) {
            $savedReportsItem = $this->storeReportsItemService->storeIfDoesNotExist($newsItem);
            $output->writeln([
                'Synced : ' . $savedReportsItem->getTitle(),
                'ID: ' . $savedReportsItem->getId(),
                '----'
            ]);
        }

        $output->writeln([
            'DONE --- DONE',
            '======'
        ]);
    }
}