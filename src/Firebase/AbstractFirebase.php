<?php

namespace App\Firebase;

use App\Model\AbstractModel;
use Google\Cloud\Storage\Bucket;
use Google\Cloud\Storage\StorageObject;
use Kreait\Firebase\Database;
use Kreait\Firebase\Factory;
use Kreait\Firebase\ServiceAccount;
use Kreait\Firebase\Storage;

/**
 * Class AbstractFirebase
 * @package App\Firebase
 */
abstract class AbstractFirebase
{

    const PATH_SERVICE_ACCOUNT = __DIR__ . '/../../zorganders-google-service-account.json';

    /**
     * @var \Kreait\Firebase
     */
    protected $firebase;

    /**
     * @var string|null
     */
    protected $defaultBucketName = null;

    /**
     * @var string|null
     */
    protected $defaultReference = null;

    /**
     * @var string
     */
    protected $modelClass = AbstractModel::class;

    /**
     * Test constructor.
     */
    public function __construct()
    {
        $serviceAccount = ServiceAccount::fromJsonFile(self::PATH_SERVICE_ACCOUNT);
        $this->firebase = (new Factory)
            ->withServiceAccount($serviceAccount)
            ->create();

    }

    /**
     * @param string|null $name
     * @return \Google\Cloud\Storage\Bucket
     */
    protected function getBucket(string $name = null): Bucket
    {
        $storage = $this->firebase->getStorage();
        return $storage->getBucket($name ? $name : $this->defaultBucketName);
    }

    /**
     * @param string|null $name
     * @return Database\Reference
     */
    protected function getReference(string $name = null): Database\Reference
    {
        return $this
            ->firebase
            ->getDatabase()
            ->getReference($name ? $name : $this->defaultReference);
    }

    /**
     * @param AbstractModel $abstractModel
     */
    public function storeIntoDatabase(AbstractModel $abstractModel)
    {
        $this
            ->getReference()
            ->getChild($abstractModel->getId())
            ->set($abstractModel->toSet());
    }

    /**
     * @param string $id
     * @return bool
     */
    public function hasId(string $id): bool
    {
        $value = $this->getReference()
            ->getChild($id)
            ->getValue();

        return $value !== null;
    }

    /**
     * @param string $id
     * @return AbstractModel
     */
    public function getById(string $id): AbstractModel
    {
        /** @var AbstractModel $model */
        $model = new $this->modelClass($id);

        if ($this->hasId($id)) {
            $dataSet = $this
                ->getReference()
                ->getChild($id)
                ->getValue();

            $model->fromSet($dataSet);
        }

        return $model;
    }

    /**
     * @param string $fileName
     * @return bool
     */
    protected function existsInBucket(string $fileName) {
        return $this->getBucket()->object($fileName)->exists();
    }

    /**
     * @param Bucket $bucket
     * @param string $fileName
     * @param string $content
     * @return StorageObject
     */
    static protected function writeToBucket(Bucket $bucket, string $fileName, string $content): StorageObject
    {
        $stream = fopen('php://memory', 'r+');

        fwrite($stream, $content);
        rewind($stream);

        return $bucket->upload(
            $stream,
            [
                'name' => sprintf('%s.html', $fileName),
            ]
        );
    }

    /**
     * @param Bucket $bucket
     * @param string $fileName
     * @param resource $resource
     * @throws \Exception when no resource is provided
     * @return StorageObject
     */
    static protected function writeJpgToBucket(Bucket $bucket, string $fileName, $resource): StorageObject
    {
        if (!is_resource($resource)) {
            $formatException = 'Try to write non-resource for fileName "%s" in the writeJpgToBucket method';
            $messageException = sprintf($formatException, $fileName);
            throw new \Exception($messageException);
        }

        ob_start();
        imagejpeg($resource);
        $jpeg_image = ob_get_clean();

        imagedestroy($resource);

        return $bucket->upload(
            $jpeg_image,
            [
                'name' => sprintf('%s.jpg', $fileName),
            ]
        );
    }
}