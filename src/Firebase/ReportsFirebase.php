<?php
namespace App\Firebase;
use App\Model\ReportModel;
use Google\Cloud\Storage\StorageObject;

/**
 * Class ArticlesFirebase
 * @package App\Firebase
 */
class ReportsFirebase extends AbstractFirebase {

    protected $defaultReference = 'reports';
    protected $defaultBucketName = 'zorganders-reports';
    protected $modelClass = ReportModel::class;

    /**
     * @param string $name
     * @param string $content
     */
    public function storeIntoBucket(string $name, string $content) {
        $bucket = $this->getBucket();
        parent::writeToBucket($bucket, $name, $content);
    }


    /**
     * @param string $name
     * @param resource $resource
     * @throws
     * @return StorageObject
     */
    public function storeImage(string $name, $resource): StorageObject
    {
        $bucket = $this->getBucket();
        return parent::writeJpgToBucket($bucket, $name, $resource);
    }
}