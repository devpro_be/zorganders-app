<?php
namespace App\Firebase;
use App\Model\ArticleModel;
use Google\Cloud\Storage\StorageObject;

/**
 * Class ArticlesFirebase
 * @package App\Firebase
 */
class ArticlesFirebase extends AbstractFirebase {

    protected $defaultReference = 'articles';
    protected $defaultBucketName = 'zorganders-articles';
    protected $modelClass = ArticleModel::class;

    const FIELD_DATE = 'date';

    /**
     * @param string $name
     * @param string $content
     */
    public function storeIntoBucket(string $name, string $content) {
        $bucket = $this->getBucket();
        parent::writeToBucket($bucket, $name, $content);
    }


    /**
     * @param string $name
     * @param resource $resource
     * @throws
     * @return StorageObject
     */
    public function storeImage(string $name, $resource): StorageObject
    {
        $bucket = $this->getBucket();
        return parent::writeJpgToBucket($bucket, $name, $resource);
    }

    /**
     * @return ArticleModel[]
     */
    public function getAllArticles():array
    {
        $dataSets = $this
            ->getReference()
            ->orderByChild(self::FIELD_DATE)
            ->getSnapshot()
            ->getValue();

        $articleModels = [];

        foreach ($dataSets as $articleId => $dataSet) {
            $articleModel = new ArticleModel($articleId);
            $articleModel->fromSet($dataSet);
            array_push($articleModels, $articleModel);
        }

        return $articleModels;
    }
}