<?php
namespace App\Firebase;

use App\Model\ArticleCategoryModel;

/**
 * Class ArticleCategoriesFirebase
 * @package App\Firebase
 */
class ArticleCategoriesFirebase extends AbstractFirebase {

    const FIELD_SOURCE = 'source';

    const SOURCE_ACTUALCARE = 'ACTUALCARE';
    const SOURCE_ZORGANDERS = 'ZORGANDERS';

    protected $modelClass = ArticleCategoryModel::class;
    protected $defaultReference = 'articles-categories';

    /**
     * @param string $source
     * @return ArticleCategoryModel[]
     */
    public function getCategoryModelsOfSource(string $source):array {

        $categoryModels = [];

        $categories = $this
            ->getReference()
            ->orderByChild(self::FIELD_SOURCE)
            ->equalTo($source)
            ->getSnapshot()
            ->getValue();

        foreach ($categories as $id => $setData) {
            $categoryModel = new ArticleCategoryModel($id);
            $categoryModel->fromSet($setData);
            array_push($categoryModels, $categoryModel);
        }

        return $categoryModels;
    }

    /**
     *  @return ArticleCategoryModel[]
     */
    public function getAllCategoriesFromActualcare():array {
        return $this->getCategoryModelsOfSource(self::SOURCE_ACTUALCARE);
    }

    /**
     *  @return ArticleCategoryModel[]
     */
    public function getAllCategoriesFromZorgAnders():array {
        return $this->getCategoryModelsOfSource(self::SOURCE_ZORGANDERS);
    }
}