<?php
namespace App\Firebase;

use App\Model\ArticleRootCategoryModel;

/**
 * Class ArticleRootCategoriesFirebase
 * @package App\Firebase
 */
class ArticleRootCategoriesFirebase extends AbstractFirebase {

    protected $modelClass = ArticleRootCategoryModel::class;
    protected $defaultReference = 'article-root-categories';

}