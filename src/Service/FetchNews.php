<?php
namespace App\Service;

use App\Helper\NewsCrawlerHelper;
use Symfony\Component\DomCrawler\Crawler;

class FetchNews {

    public const TAXONOMY_WELZIJN_EN_ZORG = 'Welzijn & Zorg';



    /**
     * Hardcoded list of taxonomy values of zorganderstv.be
     */
    public const ZORGANDERS_TV_NEWS_CATEGORIES = [
        self::TAXONOMY_WELZIJN_EN_ZORG => 158,
        'Beleid' => 157,
        'Technologie' => 204,
        'Onderzoek' => 205,
        'Facilitair' => 206,
        'Onderwijs' => 207,
    ];

    /**
     * @param int $page
     * @return \App\Entity\NewsItem[]
     */
    static public function fetchNewsPage($page = 1) {
        $pageContent = self::fetchPageContent($page);
        $jsonData = json_decode($pageContent);
        $items = $jsonData[1]->data;
        $crawler = new Crawler($items);
        return NewsCrawlerHelper::crawlNewsItems($crawler);
    }

    /**
     * @param int $taxonomyTerm
     * @param int $page
     * @return \App\Entity\NewsItem[]
     */
    static public function fetchTaxonomyTermPage($taxonomyTerm, $page = 1) {
        $pageContent = self::fetchTaxonomyTermContent($taxonomyTerm, $page);
        $jsonData = json_decode($pageContent);
        $items = $jsonData[1]->data;
        $crawler = new Crawler($items);
        return NewsCrawlerHelper::crawlNewsItems($crawler);
    }


    static protected function createStreamContextOptions($page, $viewPath, $view_display_id, $viewArgs=null) {
        $data = array(
            'view_args' => $viewArgs,
            'view_path  ' => $viewPath,
            'view_name' => 'nieuws_recent_viewed',
            'view_display_id' => $view_display_id,
            'page' => $page,
        );
        return array(
            'http' => array(
                'header'  => "Content-type: application/x-www-form-urlencoded\r\n",
                'method'  => 'POST',
                'content' => http_build_query($data),
            )
        );
    }

    static protected function fetchPageContent($page) {
        $url = 'http://www.zorganderstv.be/views/ajax';
        $streamConextOptions = self::createStreamContextOptions(
            $page,
            'nieuws_recent_viewed',
            'panel_pane_1'
        );
        $context  = stream_context_create($streamConextOptions);
        return file_get_contents($url, false, $context);
    }

    static protected function fetchTaxonomyTermContent($taxonomyTerm, $page) {
        $viewPath = sprintf('taxonomy/term/%s', $taxonomyTerm);
        $url = 'http://www.zorganderstv.be/views/ajax';
        $streamConextOptions = self::createStreamContextOptions(
            $page,
            $viewPath,
            'panel_pane_3',
            $taxonomyTerm
        );
        $context  = stream_context_create($streamConextOptions);
        return file_get_contents($url, false, $context);
    }

}