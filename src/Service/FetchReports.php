<?php
namespace App\Service;

use App\Helper\NewsCrawlerHelper;
use App\Helper\ReportsCrawlerHelper;
use App\Helper\ReportsItemCrawlerHelper;
use Symfony\Component\DomCrawler\Crawler;

class FetchReports {

    /**
     * @param int $page
     * @return \App\Entity\ReportItem[]
     */
    static public function fetchReportsPage($page = 1) {
        $pageContent = self::fetchPageContent($page);
        $crawler = new Crawler($pageContent);
        $crawler = $crawler->filter('#quicktabs-tabpage-reportages-0');
        return ReportsCrawlerHelper::crawlReportsItems($crawler);
    }


    static protected function createStreamContextOptions($page) {
        $data = array(
            'view_path  ' => 'node/2',
            'view_name' => 'reportages_recent_viewed',
            'view_display_id' => 'panel_pane_1',
            'page' => $page,
        );
        return array(
            'http' => array(
                'header'  => "Content-type: application/x-www-form-urlencoded\r\n",
                'method'  => 'GET',
                //'content' => http_build_query($data),
            )
        );
    }

    static protected function fetchPageContent($page) {
        $url = 'http://www.zorganderstv.be/reportages?page=' . $page;
        $streamConextOptions = self::createStreamContextOptions($page);
        $context  = stream_context_create($streamConextOptions);
        return file_get_contents($url, false, $context);
    }

}