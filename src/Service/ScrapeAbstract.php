<?php

namespace App\Service;

use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bridge\Doctrine\RegistryInterface;

class ScrapeAbstract extends ServiceEntityRepository {

    const BASE_URL = 'http://staging.zorganders.be/';

    const ENTITY_CLASS = '';

    /**
     * @var EntityManagerInterface
     */
    protected $entityManager;

    /**
     * ScrapeAbstract constructor.
     * @param RegistryInterface $registry
     */
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, static::ENTITY_CLASS);
    }

    /**
     * @return array
     */
    static protected function getContextOptions() {
        return array(
            'http' => array(
                'Accept'  => "Content-type: text/html,application/xhtml+xml,application/xml;\r\n",
                'method'  => 'GET',
            )
        );
    }

    /**
     * @return resource
     */
    static protected function getContext() {
        return stream_context_create(self::getContextOptions());
    }

    /**
     * @param string $url
     * @return bool|string
     */
    static protected function getContentUrl($url) {
        $context = self::getContext();
        return file_get_contents($url, false, $context);
    }
}