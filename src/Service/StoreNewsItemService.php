<?php

namespace App\Service;

use App\Entity\NewsCategory;
use App\Entity\NewsItem;
use Doctrine\ORM\EntityManagerInterface;

class StoreNewsItemService {

    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * StoreNewsItem constructor.
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * Stores the newsItem if it doesn't exists already, otherwise return the already existing one.
     * @param NewsItem $newsItem
     * @return NewsItem
     */
    public function storeIfDoesNotExist(NewsItem $newsItem) {
        $potentialExistingNewsItem = self::fetchNewsItem($newsItem);

        if ($potentialExistingNewsItem instanceof NewsItem) {
            $newsItem = $potentialExistingNewsItem;
        } else {
            $this->entityManager->persist($newsItem);
            $this->entityManager->flush();
        }

        return $newsItem;
    }

    /**
     * @param NewsItem $newsItem
     * @return NewsItem
     */
    public function update(NewsItem $newsItem) {
        $this->entityManager->persist($newsItem);
        $this->entityManager->flush();
        return $newsItem;
    }


    /**
     * @param NewsItem $newsItem
     * @return null|object
     */
    public function fetchNewsItem(NewsItem $newsItem) {
        return $this
            ->entityManager
            ->getRepository(NewsItem::class)
            ->findOneBy(
                array(
                    'url' => $newsItem->getUrl(),
                )
            );
    }

    /**
     * @param $categoryId
     * @return null|NewsCategory|Object
     */
    public function getNewsCategory($categoryId) {
        return $this
            ->entityManager
            ->getRepository(NewsCategory::class)
            ->find($categoryId);
    }
}