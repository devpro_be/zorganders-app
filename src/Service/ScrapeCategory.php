<?php

namespace App\Service;

use App\Entity\Post;
use App\Model\ArticleCategoryModel;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bridge\Doctrine\RegistryInterface;

use Symfony\Component\DomCrawler\Crawler;

class ScrapeCategory extends ScrapeAbstract {


    const BASE_URL_CATEGORY = self::BASE_URL . 'category/nl/';

    const ENTITY_CLASS =  Post::class;

    public function getContent() {
        $categoryUrl = new ArticleCategoryModel(self::BASE_URL_CATEGORY . 'nl-management/nl-management-ict/');

        $pageContent = self::getContentOfPage($categoryUrl->getUrl(), 15);

        $postsUrls = $this->getPostsUrlsFromPageContent($pageContent);


    }

    static protected function getContentOfPage($categoryUrl, $page) {
        return self::getContentUrl($categoryUrl . 'page/' . $page);
    }

    static protected function getContentOfPost($postUrl) {
        return self::getContentUrl($postUrl);
    }

    /**
     * @param $pageContent
     * @return string[]
     */
    protected function getPostsUrlsFromPageContent($pageContent) {
        $postUrls = [];

        $crawler = new Crawler($pageContent);
        $crawler
            ->filter('.tt-blog-masonry')
            ->first()
            ->filter('.tt-post')
            ->each(
                function (Crawler $postNode, $i) use (&$postUrls) {
                    $url = $postNode
                        ->filter('.tt-post-title')
                        ->attr('href');
                    array_push($postUrls, $url);
                }
            );



        foreach ($postUrls as $postsUrl) {
            $postContent = self::getContentOfPost($postsUrl);

            $postCrawler = new Crawler($postContent);
            $postCrawler = $postCrawler
                ->filter('article')
                ->first();

            $title = self::getTitleFromPostContent($postCrawler);
            $date = self::getDateFromPostContent($postCrawler);
            $content = $postCrawler->filter('.tt-content')->first()->html();

            $postEntity = new Post();
            $postEntity->setTitle($title);
            $postEntity->setDate($date);
            $postEntity->setContent($content);

            $this->entityManager->persist($postEntity);
        }

        $this->entityManager->flush();


        exit();

        return $postUrls;
    }

    /**
     * @param Crawler $postCrawler
     * @return string
     */
    static protected function getTitleFromPostContent(Crawler $postCrawler) {
        return $postCrawler
            ->filter('h1')
            ->first()
            ->text();
    }

    /**
     * @param Crawler $postCrawler
     * @return \DateTime
     */
    static protected function getDateFromPostContent(Crawler $postCrawler) {
        $dateText = $postCrawler
            ->filter('.tt-post-date-single')
            ->first()
            ->text();

        $months = [
            'januari' => 'January',
            'februari' => 'February',
            'maart' => 'March',
            'april' => 'April',
            'mei' => 'May',
            'juni' => 'June',
            'juli' => 'July',
            'augustus' => 'August',
            'september' => 'September',
            'oktober' => 'October',
            'november' => 'November',
            'december' => 'December',
        ];

        foreach ($months as $month => $replace) {
            $dateText = str_replace($month, $replace, $dateText);
        }

        $date = \DateTime::createFromFormat('F j, Y', $dateText);


        return $date;
    }

}