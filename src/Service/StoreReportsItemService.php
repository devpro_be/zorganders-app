<?php

namespace App\Service;

use App\Entity\ReportItem;
use Doctrine\ORM\EntityManagerInterface;

class StoreReportsItemService {

    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * StoreReportsItem constructor.
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * Stores the newsItem if it doesn't exists already, otherwise return the already existing one.
     * @param ReportItem $reportItem
     * @return ReportItem
     */
    public function storeIfDoesNotExist(ReportItem $reportItem) {
        $potentialExistingNewsItem = self::fetchReportItem($reportItem);

        if ($potentialExistingNewsItem instanceof ReportItem) {
            $reportItem = $potentialExistingNewsItem;
        } else {
            $this->entityManager->persist($reportItem);
            $this->entityManager->flush();
        }

        return $reportItem;
    }


    /**
     * @param ReportItem $reportItem
     * @return null|object
     */
    protected function fetchReportItem(ReportItem $reportItem) {
        return $this
            ->entityManager
            ->getRepository(ReportItem::class)
            ->findOneBy(
                array(
                    'url' => $reportItem->getUrl(),
                )
            );
    }
}