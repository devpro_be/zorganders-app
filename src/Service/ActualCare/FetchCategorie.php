<?php
/**
 * Created by IntelliJ IDEA.
 * User: jonasverhaert
 * Date: 18/03/2018
 * Time: 21:03
 */

namespace App\Service\ActualCare;

class FetchCategorie {

    const CURE = 'cure';
    const CURE_ZIEKENHUIZEN = 'ziekenhuizen';
    const CURE_PATIENTEN = 'patienten';

    const LIST = [
        self::CURE_ZIEKENHUIZEN => '/nl/nl-cure/nl-cure-ziekenhuizen',

    ];

    protected static function createURL($parent, $child) {
        $format = 'http://www.actualcare.be/nl/nl-%s/nl-%s-%$';
        return sprintf($format, $parent, $parent, $child);
    }

    protected function getContent() {

    }
}