<?php

namespace App\Service;

use App\Entity\NewsCategory;
use Doctrine\ORM\EntityManagerInterface;

class NewsCategoriesService {

    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * StoreNewsItem constructor.
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * Collect all existing categories
     * @return NewsCategory[]
     */
    public function getCategories() {
        $categories = $this
            ->entityManager
            ->getRepository(NewsCategory::class)
            ->findAll();

        return $categories;
    }
}