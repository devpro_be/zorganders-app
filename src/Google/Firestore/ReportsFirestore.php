<?php

namespace App\Google\Firestore;

use App\Model\ReportModel;

class ReportsFirestore extends AbstractFirestore
{
    const COLLECTION_NAME = 'reports';

    const SOURCE_ACTUALCARE = 'ACTUALCARE';
    const SOURCE_ZORGANDERS = 'ZORGANDERS';

    protected $defaultCollectionName = self::COLLECTION_NAME;
    protected $modelClass = ReportModel::class;

    /**
     * @param string $source
     * @return ReportModel[]
     */
    public function getAllFromSource(string $source):array
    {
        $documents = $this
            ->getCollectionRef()
            ->where('source', '=', $source)
            ->documents();

        return $this->castToModels($documents);
    }

    /**
     * @param string $category
     * @return ReportModel[]
     */
    public function getAllWithCategory(string $category): array {
        $documents = $this
            ->getCollectionRef()
            ->where('category', '=', $category)
            ->documents();

        return $this->castToModels($documents);
    }

}