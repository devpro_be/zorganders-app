<?php

namespace App\Google\Firestore;

use App\Model\ArticleCategoryModel;

class ArticleRootCategoriesFirestore extends AbstractFirestore
{
    const COLLECTION_NAME = 'article-root-categories';

    const SOURCE_ACTUALCARE = 'ACTUALCARE';
    const SOURCE_ZORGANDERS = 'ZORGANDERS';

    protected $defaultCollectionName = self::COLLECTION_NAME;
    protected $modelClass = ArticleCategoryModel::class;

    /**
     * @param string $rootCategory
     * @return ArticleCategoryModel[]
     */
    public function getAllCategories(string $rootCategory):array
    {
        $rootCategorySnapshot = $this
            ->getCollectionRef()
            ->document($rootCategory)
            ->snapshot();

        return $rootCategorySnapshot->data()['categories'];

    }

}