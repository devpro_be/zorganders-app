<?php

namespace App\Google\Firestore;

use App\Model\MagazineModel;

class MagazinesFirestore extends AbstractFirestore
{
    const COLLECTION_NAME = 'magazines';

    const SOURCE_ACTUALCARE = 'ACTUALCARE';
    const SOURCE_ZORGANDERS = 'ZORGANDERS';

    protected $defaultCollectionName = self::COLLECTION_NAME;
    protected $modelClass = MagazineModel::class;

    /**
     * @param string $magazine
     * @return string[]
     */
    public function getAllCategories(string $magazine):array
    {
        $rootCategorySnapshot = $this
            ->getCollectionRef()
            ->document($magazine)
            ->snapshot();

        return $rootCategorySnapshot->data()['categories'];

    }

}