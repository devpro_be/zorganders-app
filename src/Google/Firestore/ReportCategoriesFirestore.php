<?php

namespace App\Google\Firestore;

use App\Model\ReportCategoryModel;

class ReportCategoriesFirestore extends AbstractFirestore
{
    const COLLECTION_NAME = 'reports-categories';

    const SOURCE_ACTUALCARE = 'ACTUALCARE';
    const SOURCE_ZORGANDERS = 'ZORGANDERS';

    protected $defaultCollectionName = self::COLLECTION_NAME;
    protected $modelClass = ReportCategoryModel::class;

    /**
     * @param string $source
     * @return ReportCategoryModel[]
     */
    public function getAllFromSource(string $source):array
    {
        $documents = $this
            ->getCollectionRef()
            ->where('source', '=', $source)
            ->documents();

        return $this->castToModels($documents);
    }

}