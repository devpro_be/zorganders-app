<?php

namespace App\Google\Firestore;

use App\Model\ArticleModel;

class ArticlesFirestore extends AbstractFirestore
{
    const COLLECTION_NAME = 'articles';

    const SOURCE_ACTUALCARE = 'ACTUALCARE';
    const SOURCE_ZORGANDERS = 'ZORGANDERS';

    protected $defaultCollectionName = self::COLLECTION_NAME;
    protected $modelClass = ArticleModel::class;

    /**
     * @param string $source
     * @return ArticleModel[]
     */
    public function getAllFromSource(string $source):array {
        $documents = $this
            ->getCollectionRef()
            ->where('source', '=', $source)
            ->documents();

        return $this->castToModels($documents);
    }

    /**
     * @param string $category
     * @return ArticleModel[]
     */
    public function getAllWithCategory(string $category): array {
        $documents = $this
            ->getCollectionRef()
            ->where('category', '=', $category)
            ->documents();

        return $this->castToModels($documents);
    }

}