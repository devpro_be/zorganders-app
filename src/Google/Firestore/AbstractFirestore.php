<?php

namespace App\Google\Firestore;

use App\Model\AbstractModel;
use Doctrine\ORM\Query;
use Google\Cloud\Firestore\CollectionReference;
use Google\Cloud\Firestore\DocumentReference;
use Google\Cloud\Firestore\DocumentSnapshot;
use Google\Cloud\Firestore\FirestoreClient;
use Google\Cloud\Firestore\QuerySnapshot;
use League\Flysystem\Exception;

abstract class AbstractFirestore {

    /**
     * @var FirestoreClient
     */
    protected $firestoreClient;

    /**
     * @var string
     */
    protected $defaultCollectionName;

    /**
     * @var string
     */
    protected $modelClass;

    /**
     * AbstractFirestore constructor.
     */
    public function __construct()
    {
        $this->checkRequiredClassProperties();
        $this->firestoreClient = new FirestoreClient();
    }

    /**
     * Throws an exception when a required class property isn't set when an instance of it gets initiated.
     * @throws Exception
     */
    private function checkRequiredClassProperties()
    {
        $errorMessageFormat = 'Missing definition of required class property : %s';

        if ($this->defaultCollectionName === null) {
            $errorMessage = sprintf($errorMessageFormat, 'defaultCollectionName');
            throw new Exception($errorMessage);
        }

        if ($this->modelClass === null) {
            $errorMessage = sprintf($errorMessageFormat, 'modelClass');
            throw new Exception($errorMessage);
        }
    }

    /**
     * @return CollectionReference
     */
    public function getCollectionRef():CollectionReference
    {
        return $this
            ->firestoreClient
            ->collection($this->defaultCollectionName);
    }

    /**
     * Shortcut method for retrieving a document by its id.
     * @param string $documentId
     * @return DocumentReference
     */
    public function getDocumentRef(string $documentId):DocumentReference
    {
        $document = $this
            ->getCollectionRef()
            ->document($documentId);

        return $document;
    }

    /**
     * @param string $id
     * @return bool
     */
    public function hasId(string $id): bool
    {
        return $this
            ->getDocumentRef($id)
            ->snapshot()
            ->exists()
        ;
    }

    /**
     * Will return a model class instance from the modelClass.
     * If the document exists in the Firestore collection the values will be set on the model.
     * @param string $id
     * @return AbstractModel
     */
    public function getById(string $id):AbstractModel
    {
        $snapshot = $this->getDocumentRef($id)->snapshot();

        /** @var AbstractModel $abstractModel */
        $abstractModel = new $this->modelClass($id);

        if ($snapshot->exists()) {
            $abstractModel->fromSet($snapshot->data());
        }

        return $abstractModel;
    }

    /**
     * @param AbstractModel $abstractModel
     */
    public function save(AbstractModel $abstractModel)
    {
        $this
            ->getDocumentRef($abstractModel->getId())
            ->set($abstractModel->toSet())
        ;
    }

    /**
     * @return AbstractModel[]
     */
    public function getAll():array
    {
        $documents = $this
            ->getCollectionRef()
            ->documents();

        return $this->castToModels($documents);
    }

    /**
     * @param DocumentSnapshot $documentSnapshot
     * @return string
     */
    public static function getDocumentId(DocumentSnapshot $documentSnapshot): string
    {
        $nameParts = explode('/', $documentSnapshot->name());
        $documentId = end($nameParts);
        return $documentId;
    }

    /**
     * @param QuerySnapshot $documents
     * @return AbstractModel[]
     */
    protected function castToModels(QuerySnapshot $documents):array
    {
        $abstractModels = [];

        foreach ($documents as $document) {
            $id = self::getDocumentId($document);
            /** @var AbstractModel $abstractModel */
            $abstractModel = new $this->modelClass($id);
            $abstractModel->fromSet($document->data());
            array_push($abstractModels, $abstractModel);
        }

        return $abstractModels;
    }

}