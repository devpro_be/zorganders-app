#!/bin/bash
echo "Setup iOS cordova build";

FONTS_FILE=$PWD/app/www/css/fonts.css
FONTS_IOS_FILE=$PWD/app/res/ios/css/fonts.css

LOGO_FILE=$PWD/app/www/css/logo.css
LOGO_IOS_FILE=$PWD/app/res/ios/css/logo.css

echo "Going to overwrite '${FONTS_FILE}' with '${FONTS_IOS_FILE}'"

cp -f $FONTS_IOS_FILE $FONTS_FILE

echo "Going to overwrite '${LOGO_FILE}' with '${LOGO_IOS_FILE}'"

cp -f $LOGO_IOS_FILE $LOGO_FILE

echo "Files are overwritten."