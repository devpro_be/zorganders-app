#!/bin/bash
echo "Setup android cordova build";

FONTS_FILE=$PWD/app/www/css/fonts.css
FONTS_ANDROID_FILE=$PWD/app/res/android/css/fonts.css

LOGO_FILE=$PWD/app/www/css/logo.css
LOGO_ANDROID_FILE=$PWD/app/res/android/css/logo.css

echo "Going to overwrite '${FONTS_FILE}' with '${FONTS_ANDROID_FILE}'"

cp -f $FONTS_ANDROID_FILE $FONTS_FILE

echo "Going to overwrite '${LOGO_FILE}' with '${LOGO_ANDROID_FILE}'"

cp -f $LOGO_ANDROID_FILE $LOGO_FILE

echo "Files are overwritten."