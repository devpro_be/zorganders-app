import React, { Component } from 'react';
import { Link } from 'react-router-dom'

const Navigation = props => {
    const { items, name } = props;
    const NavigationItems = items.map( (item) => renderNavigationItem(name, item) )

    return (
      <ul className={`navigation navigation-${name}`}>
          { NavigationItems }
      </ul>
    )
};

const renderNavigationItem = (navigationName, item) => {
  const { id, isActive, label, path } = item;
  return (
      <li key={`navigation-${navigationName}-item-${item.id}`} className={isActive ? 'active' : 'link'}>
          {isActive ? <span>{label}</span> : <Link to={path}>{label}</Link>}
      </li>
  )
}

export default Navigation;
