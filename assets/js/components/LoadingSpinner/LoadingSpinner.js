import React from 'react'

export const LoadingSpinner = () => (
    <div className='loading'>
        <div className='spinner'>
        </div>
    </div>
);

export default LoadingSpinner