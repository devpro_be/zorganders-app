import React, { Component } from 'react';

class NewsItems extends Component {
    render() {

        const { newsItems } = this.props;


        return (
            <div className="news-items">
                {
                    newsItems.map(
                        (item) => <p key={item.id} >{item.id}</p>
                    )
                }
            </div>
        );
    }
}

export default NewsItems;
