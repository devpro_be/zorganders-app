import React, { Component } from 'react';

const Categories = [
    'ICT/medische technologie',
    'Management en Finance',
    'HR',
    'Bouw en Technieken',
    'Facility',
    'Onderzoek en onderwijs',
    'Bewoners en patiëntenzorg',
];

const NewsItem = props => {
    const { item, openArticle } = props;

    const goToArticle = () => openArticle(item.id);
    const imgSrc = 'http://localhost:8000/img/test.png';


    const cat = Categories[Math.floor(Math.random()*Categories.length)];

    return (
        <div className='article' onClick={goToArticle}>
            <div className='image'>
                <img src={imgSrc} />
            </div>
            <div className='details'>
                <div className='title'>{item.title}</div>
                <div className='date'>{item.date}</div>
                <div className='category'>{cat}</div>
            </div>
        </div>
    )
};


const renderNewsItem = (item, openArticle) =>
    <NewsItem key={`news-item-${item.id}`} item={item} openArticle={openArticle} />;


export default renderNewsItem;
