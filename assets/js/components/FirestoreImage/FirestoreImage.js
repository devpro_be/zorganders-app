import React from 'react'
import PropTypes from "prop-types";
import {lifecycle} from "recompose";
import { compose } from 'redux'
import { withHandlers, withState, pure } from 'recompose'
import { withFirebase } from 'react-redux-firebase'

export const FirestoreImage = ({
    articleId,
    imageSrc,
    imageSrcDefault,
}) => (
    <div className='image'>
        {
            imageSrc
            &&
            (
                <img src={imageSrc} />
            )
        }
        {
            !imageSrc
            &&
            (
                <img src={imageSrcDefault} />
            )
        }
    </div>
);

FirestoreImage.propTypes = {
    articleId: PropTypes.string,
    imageSrcDefault: PropTypes.string.isRequired,
    imageSrc: PropTypes.string,
};

const enhancer = compose(
    withFirebase,
    withState('imageSrc', 'setImageSrc', undefined),
    // Create listeners based on current users UID
    // Add handlers as props
    withHandlers({
        getImage: ({firebase, setImageSrc}) => articleId => {
            const storage = firebase.app().storage('gs://zorganders-articles');
            const imageArticleRef = storage.refFromURL(`gs://zorganders-articles/${articleId}__thumbnail.jpg`)
            imageArticleRef.getDownloadURL().then(function(url) {
                setImageSrc(url);
            }).catch(function(error) {
                console.log(error.code);
            });

        }
    }),
    lifecycle({
        // Load data when component mounts
        componentWillMount() {
            const {getImage, articleId} = this.props;
            getImage(articleId);
        }
    }),

    pure // shallow equals comparison on props (prevent unessesary re-renders)
);

export default enhancer(FirestoreImage)

