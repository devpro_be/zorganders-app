import {createContext} from "react";

export const AppConfigContext = createContext();

export const AppConfigContextConsumer = AppConfigContext.Consumer;

