/* eslint-disable no-console */
import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { pick, some } from 'lodash'
import { isLoaded } from 'react-redux-firebase'
import {  branch, renderComponent } from 'recompose'
import LoadingSpinner from '../components/LoadingSpinner'

/**
 * Show a loading spinner when a condition is truthy. Used within
 * spinnerWhileLoading. Accepts a test function and a higher-order component.
 * @param  {Function} condition - Condition function for when to show spinner
 * @return {HigherOrderComponent}
 */
export const spinnerWhile = condition =>
    branch(condition, renderComponent(LoadingSpinner));

/**
 * Show a loading spinner while props are loading . Checks
 * for undefined, null, or a value (as well as handling `auth.isLoaded` and
 * `profile.isLoaded`). **NOTE:** Meant to be used with props which are passed
 * as props from state.firebase using connect (from react-redux), which means
 * it could have unexpected results for other props
 * @example Spinner While Data Loading
 * import { compose } from 'redux'
 * import { connect } from 'react-redux'
 * import { firebaseConnect } from 'react-redux-firebase'
 *
 * const enhance = compose(
 *   firebaseConnect(['projects']),
 *   connect(({ firebase: { data: { projects } } })),
 *   spinnerWhileLoading(['projects'])
 * )
 *
 * export default enhance(SomeComponent)
 * @param  {Array} propNames - List of prop names to check loading for
 * @return {HigherOrderComponent}
 */
export const spinnerWhileLoading = propNames =>
    spinnerWhile(props => some(propNames, name => !isLoaded(props[name])));

export const createWithFromContext = withVar => WrappedComponent => {
    class WithFromContext extends Component {
        render() {
            const props = { [withVar]: this.context[withVar] };
            if (this.context.store && this.context.store.dispatch) {
                props.dispatch = this.context.store.dispatch
            }
            return <WrappedComponent {...this.props} {...props} />
        }
    }

    WithFromContext.contextTypes = {
        [withVar]: PropTypes.object.isRequired
    };

    return WithFromContext
};

export const withRouter = createWithFromContext('router');