import React from 'react'
import ReactDOM from 'react-dom'
import createStore from './store/createStore'
import { initScripts } from './utils'
import { version } from '../../package.json'
import { env } from './config'
import App from './containers/App';
import Routes from './routes';

// Window Variables
// ------------------------------------
window.version = version;
window.env = env;
initScripts();

// Store Initialization
// ------------------------------------
const initialState = window.___INITIAL_STATE__ || {
    firebase: { authError: null }
};
const store = createStore(initialState);

const appConfig = {};

// Render Setup
// ------------------------------------
const MOUNT_NODE = document.getElementById('root');

let render = () => {
    ReactDOM.render(<App appConfig={appConfig} store={store} routes={Routes} />, MOUNT_NODE)
};

const __DEV__ = true;
const __TEST__ = false;

// Development Tools
// ------------------------------------
if (__DEV__) {
    if (module.hot) {
        const renderApp = render;
        const renderError = error => {
            const RedBox = require('redbox-react').default;

            ReactDOM.render(<RedBox error={error} />, MOUNT_NODE)
        };

        render = () => {
            try {
                renderApp()
            } catch (e) {
                console.error(e); // eslint-disable-line no-console
                renderError(e);
            }
        };

        // Setup hot module replacement
        module.hot.accept(['./containers/App', './routes/index'], () =>
            setImmediate(() => {
                ReactDOM.unmountComponentAtNode(MOUNT_NODE);
                render();
            })
        )
    }
}

// Let's Go!
// ------------------------------------
if (!__TEST__) render();

// ADD Sticky effect header
// ------------------------------------
const startApp = () => {
    const sticky = 115;

    const onWindowScroll = () => {
        if (window.pageYOffset >= sticky) {
            const header = document.getElementById("header");
            header.classList.add('sticky');
        } else {
            const header = document.getElementById("header");
            header.classList.remove('sticky');
        }
    };

    window.onscroll = () => onWindowScroll();

};

if(!window.cordova) {
    startApp()
} else {
    document.addEventListener('deviceready', startApp, false)
}
