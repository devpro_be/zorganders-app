import React from 'react';
import { withRouter, Route, Link } from 'react-router-dom'
import { Article, Bundles, Categories, Category, News, Settings, Reports, Report } from '../pages'

import {
  NavigationMain,
  NavigationSectionNews,
  NavigationSectionReports,
  NavigationSectionBundles,
} from '../navigations';

import {
    PATH_ARTICLE,
    PATH_BUNDLES,
    PATH_CATEGORIES,
    PATH_CATEGORY,
    PATH_NEWS,
    PATH_SETTINGS,
    PATH_REPORTS,
    PATH_REPORT,
    PATH_BASE,
} from "../constants/paths";

import {
  SECTION_NEWS,
  SECTION_REPORTS,
  SECTION_BUNDLES,
} from '../constants/sections';

import { getTheme } from "../helpers/theme";
import { getSectionFromPath } from "../helpers/section";

const App_old = withRouter(props => {
    const { location } = props;
    const currentPath = location.pathname;
    const currentTheme = getTheme(currentPath);
    const currentSection = getSectionFromPath(currentPath);

    return (
        <div id='zorganders-app' className={currentTheme}>
            <header id='header'>
                <div id="zorganders-app-logo" />
                <NavigationMain currentSection={currentSection} />
                { currentSection === SECTION_NEWS && <NavigationSectionNews currentPath={currentPath} /> }
                { currentSection === SECTION_REPORTS && <NavigationSectionReports currentPath={currentPath} /> }
                { currentSection === SECTION_BUNDLES && <NavigationSectionBundles currentPath={currentPath} /> }
            </header>

            <main className="main-content">
                <Route exact path={PATH_BASE} component={News} />
                <Route exact path={PATH_NEWS} component={News} />
                <Route exact path={PATH_BUNDLES} component={Bundles} />
                <Route exact path={PATH_ARTICLE} component={Article} />
                <Route exact path={PATH_CATEGORIES} component={Categories} />
                <Route exact path={PATH_CATEGORY} component={Category} />
                <Route exact path={PATH_SETTINGS} component={Settings} />
                <Route exact path={PATH_REPORTS} component={Reports} />
                <Route exact path={PATH_REPORT} component={Report} />
            </main>
        </div>
    );
});

export default App_old;

const startApp = () => {
    const sticky = 115;

    const onWindowScroll = () => {
        if (window.pageYOffset >= sticky) {
            const header = document.getElementById("header");
            header.classList.add('sticky');
        } else {
            const header = document.getElementById("header");
            header.classList.remove('sticky');
        }
    };

    window.onscroll = () => onWindowScroll();

};

if(!window.cordova) {
    startApp()
} else {
    document.addEventListener('deviceready', startApp, false)
}
