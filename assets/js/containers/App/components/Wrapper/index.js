import enhance from './Wrapper.enhancer';
import Wrapper from './Wrapper';

export default enhance(Wrapper);