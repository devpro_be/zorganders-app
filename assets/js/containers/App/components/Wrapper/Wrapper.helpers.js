import {
    SECTION__UNDEFINED,
    SECTION__ARTICLES,
    SECTION__REPORTS,
    SECTION__BUNDLES,
    SUBSECTION__ITEM,
    SUBSECTION__MOST_READ,
    SUBSECTION__OVERVIEW,
    FIRST_PATH_PART__BASE,
    FIRST_PATH_PART__ARTICLES,
    FIRST_PATH_PART__REPORTS,
    FIRST_PATH_PART__BUNDLES,
    SECOND_PATH_PART__OVERVIEW,
    SECOND_PATH_PART__ITEM,
    SECOND_PATH_PART__MOST_READ,
    SECOND_PATH_PART__CATEGORIES, SUBSECTION__CATEGORIES,
} from '~/constants/sections';

import navigationConfig from '~/constants/navigation';

const getSectionFromPath = (path) => {
    let result = SECTION__UNDEFINED;
    // Minimize the path to the first section
    const firstSection = path.split('/')[1];

    switch (firstSection.toLowerCase()) {
        case FIRST_PATH_PART__BASE:
        case FIRST_PATH_PART__ARTICLES:
        case 'article':
            result = SECTION__ARTICLES;
            break;
        case FIRST_PATH_PART__REPORTS:
        case 'report':
            result = SECTION__REPORTS;
            break;
        case FIRST_PATH_PART__BUNDLES:
            result = SECTION__BUNDLES;
            break;
    }

    return result;
};

const getSubsectionFromPath = (path, parentSection) => {
    let result = undefined;

    const parentSectionConfig = navigationConfig.find(
        (navigationItem) => navigationItem.section === parentSection

    );

    if (parentSectionConfig) {
        let secondSection = path.split('/')[2];
        // Check if the second subsection exists in parent
        if (secondSection) {
            secondSection = `subsection-${secondSection.toLowerCase()}`;
            const child = parentSectionConfig.subsections.find(
                (subsection) => subsection.section === secondSection
            );
            if (child) {
                result = secondSection;
            }
        } else {
            result = parentSectionConfig.defaultSubsection;
        }
    }
    return result;
};

const THEME_UNDEFINED = 'app-theme--undefined';
const THEME_LIGHT_GREEN = 'app-theme--light-green';
const THEME_GREEN = 'app-theme--green';
const THEME_DARK_GREEN = 'app-theme--dark-green';

const getThemeFromPath = (path) => {
    let theme = THEME_UNDEFINED;
    const currentSection = getSectionFromPath(path);

    switch (currentSection) {
        case SECTION__ARTICLES:
            theme = THEME_LIGHT_GREEN;
            break;
        case SECTION__REPORTS:
            theme = THEME_GREEN;
            break;
        case SECTION__BUNDLES:
            theme = THEME_DARK_GREEN;
            break;
    }

    return theme;
};

export {
    getThemeFromPath,
    getSectionFromPath,
    getSubsectionFromPath,
}

