import React from 'react'
import PropTypes from 'prop-types'
import {AppConfigContext} from '~/context/AppConfig';
import {Content, Header} from "./components";

const CLASSNAME = 'app-wrapper';

const Wrapper = ({ wrapperConfig, routes }) => {
    if (!wrapperConfig) {
        return <div>Loading</div>;
    } else {
        const {theme, section, subsection} = wrapperConfig;
        const subsectionModifier = subsection ? '--with-subsection' : '--without-subsection';
        const className = `${CLASSNAME} ${theme} ${CLASSNAME + subsectionModifier}`;
        return (
            <div className={className}>
                <Header section={section} subsection={subsection} />
                <Content routes={routes}/>
            </div>
        );
    }
};

Wrapper.propTypes = {
    routes: PropTypes.array.isRequired,
};

export default Wrapper;

