import {compose} from 'redux'
import {lifecycle, withState} from 'recompose'
import {withHandlers, pure} from 'recompose'
import {withRouter} from 'react-router-dom'
import {getThemeFromPath, getSectionFromPath, getSubsectionFromPath} from "./Wrapper.helpers";

// noinspection JSUnusedGlobalSymbols
export default compose(
    withRouter,

    withState('wrapperConfig', 'setWrapperConfig', undefined),

    withHandlers({
        configWrapper: ({location, wrapperConfig, setWrapperConfig}) => () => {
            const {pathname} = location;
            if (!wrapperConfig || wrapperConfig.pathname !== pathname) {
                const theme = getThemeFromPath(pathname);
                const section = getSectionFromPath(pathname);
                const subsection = getSubsectionFromPath(pathname, section);
                const newWrapperConfig = {
                    pathname,
                    theme,
                    section,
                    subsection,
                };
                setWrapperConfig(newWrapperConfig);
            }
        }
    }),

    lifecycle({
        componentDidMount() {
            const {configWrapper} = this.props;
            configWrapper();
        },
        componentWillUpdate() {
            const {configWrapper} = this.props;
            configWrapper();
        },
        shouldComponentUpdate(nextProps) {
            const {location, wrapperConfig} = this.props;
            const {location: nextLocation, wrapperConfig: nextWrapperConfig} = nextProps;
            const samePath = location.pathname === nextLocation.pathname;
            const sameWrapperConfig = wrapperConfig && wrapperConfig.pathname === nextWrapperConfig.pathname;
            return !samePath || !sameWrapperConfig;
        }
    }),

    pure
)

