import React from 'react'
import PropTypes from 'prop-types'
import {Redirect, Route} from "react-router-dom";

const RouteWithSubRoutes = route => (
    <Route
        path={route.path}
        exact={route.exact}
        render={props => (
            // pass the sub-routes down to keep nesting
            <route.component {...props} routes={route.routes} />
        )}
    />
);

const Content = ({ routes }) => {
    const className = 'app-content';
    return (
        <div className={className}>
            <Route exact path="/" component={() => <Redirect to="/articles" />}/>
            {routes.map((route, i) => <RouteWithSubRoutes key={i} {...route} />)}
        </div>

    );
};

Content.propTypes = {
    routes: PropTypes.array.isRequired,
};

export default Content;