import React from 'react';
import PropTypes from 'prop-types'
import {Item} from './components'

const Navigation = ({name, items}) => {

    return (
        <ul className={`navigation-menu navigation-menu--${name}`}>
            {
                items.map(
                    (item, idx) => <Item key={`navigation-${name}-item-${idx}`} item={item}/>
                )
            }
        </ul>
    )
};

Navigation.propTypes = {
    name: PropTypes.string.isRequired,
    items: PropTypes.arrayOf(PropTypes.object).isRequired,
};


export default Navigation;
