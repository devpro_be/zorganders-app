import React from 'react'
import PropTypes from 'prop-types';
import {NavigationBar} from './components'

const Header = ({section, subsection}) =>
    <header id='header' className='heade noselect'>
        <div id="zorganders-app-logo"/>
        <NavigationBar section={section} subsection={subsection}/>
        <div id="hide-top-notch" />
    </header>
;


Header.propTypes = {
    section: PropTypes.string.isRequired,
    subsection: PropTypes.string
};

export default Header;