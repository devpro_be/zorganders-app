import React from 'react'
import PropTypes from 'prop-types'
import {Navigation} from './components';

const NavigationBar = ({menuConfig}) => {
    const rootMenu = menuConfig ? menuConfig.rootMenu : undefined;
    const subMenu = menuConfig ? menuConfig.subMenu : undefined;
    return (
        <div id="app-navigation" className='navigation'>
            { rootMenu && <Navigation name="main" items={rootMenu}/> }
            { subMenu && subMenu.length && <Navigation name="sub" items={subMenu}/> }
        </div>
    );
};

NavigationBar.propTypes = {
    section: PropTypes.string.isRequired,
    subsection: PropTypes.string,
    menuConfig: PropTypes.object,
};

export default NavigationBar;