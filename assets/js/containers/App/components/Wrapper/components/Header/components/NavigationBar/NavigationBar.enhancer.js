import {compose} from 'redux'
import {lifecycle, withState} from 'recompose'
import {withHandlers, pure} from 'recompose'
import {getSectionFromPath, getSubsectionFromPath} from "~/helpers/section";
import navigationConfig from '~/constants/navigation';


export default compose(
    withState('menuConfig', 'setMenuConfig', undefined),

    withHandlers({
        configureMenu: ({section, subsection, setMenuConfig}) => () => {

            const rootMenu = navigationConfig.map(
                (item) => {
                    item.isActive = item.section === section;
                    return item;
                }
            );

            const activeRoot = rootMenu.find((item) => item.isActive);

            const subMenu = activeRoot && subsection ?
                activeRoot.subsections.map(
                    (item) => {
                        item.isActive = item.section === subsection;
                        return item;
                    }
                )
                :
                undefined;


            const menuConfig = {
                section,
                subsection,
                rootMenu,
                subMenu
            };

            setMenuConfig(menuConfig);
        },
    }),

    lifecycle({
        componentDidMount() {
            // noinspection JSCheckFunctionSignatures
            this.props.configureMenu();
        },
        componentWillUpdate() {
            // noinspection JSCheckFunctionSignatures
            this.props.configureMenu();
        },
        shouldComponentUpdate(nextProps) {
            const {section, subsection, menuConfig} = this.props;
            const {section: nextSection, subsection: nextSubsection, menuConfig: nextMenuConfig} = nextProps;

            return !menuConfig
                || section !== nextSection
                || subsection !== nextSubsection
                || menuConfig.section !== nextMenuConfig.section
                || menuConfig.subsection !== nextMenuConfig.subsection
                ;
        }
    }),

    pure
)

