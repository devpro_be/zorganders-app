import React from 'react';
import PropTypes from 'prop-types'
import {Link} from 'react-router-dom'

const Item = ({item}) => {
    const {isActive, label, path} = item;
    const className = `menu-item menu-item--${isActive ? 'active' : 'link'}`;
    return (
        <li className={className}>
            {isActive ? <span>{label}</span> : <Link to={path}>{label}</Link>}
        </li>
    )
};

Item.propTypes = {
    item: PropTypes.object.isRequired,
};

export default Item;