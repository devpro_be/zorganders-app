import enhance from './NavigationBar.enhancer';
import NavigationBar from './NavigationBar';

export default enhance(NavigationBar);