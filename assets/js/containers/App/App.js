import React from 'react'
import PropTypes from 'prop-types'
import { HashRouter as Router, Route, Redirect } from "react-router-dom";
import { Provider } from 'react-redux'
import { Wrapper} from './components';

const App = ({ routes, store}) => {
    return (
        <Provider store={store}>
            <Router>
                <Wrapper routes={routes} />
            </Router>
        </Provider>
    );
};

App.propTypes = {
    routes: PropTypes.array.isRequired,
    store: PropTypes.object.isRequired
};

export default App;