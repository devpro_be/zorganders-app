import {compose} from 'redux'
import {withHandlers, pure} from 'recompose'
import {withRouter} from 'react-router-dom'

export default compose(
    withRouter,
    withHandlers({
        goToArticle: ({history}) => articleId => {
            history.push(`/article/${articleId}`)
        },
    }),
    pure
)