import React from 'react'
import PropTypes from 'prop-types'
import {ArticleItemImage} from "./components";

const MAX_LENGTH_TITLE = 100;

export const ArticleItem = ({article, categories, goToArticle}) => {

    const {id, title, date, category} = article;

    const timestamp = Number(date);
    const dateObject = new Date(timestamp);
    const dateFormatted = `${dateObject.getDate()}/${dateObject.getMonth()+1}/${dateObject.getFullYear()}`;

    const titleFormatted = title.length > MAX_LENGTH_TITLE ? `${title.substr(0, MAX_LENGTH_TITLE)}...` : title;

    const formattedCategory = categories.find((cat) => cat.id === category);

    const onClick = () => goToArticle(id);

    return (
        <div
            key={`article-${id}`}
            className='article-item'
            onClick={onClick}
        >
            <ArticleItemImage
                imageSrcDefault='images/articles/article_thumbnail_default.jpg'
                article={article}
            />
            <div className='details'>
                <div className='title'>{titleFormatted}</div>
                <div className='date'>{dateFormatted}</div>
                <div className='category'>{formattedCategory && formattedCategory.name}</div>
            </div>
        </div>
    )
};

ArticleItem.propTypes = {
    article: PropTypes.object.isRequired,
    categories: PropTypes.array.isRequired,
    goToArticle: PropTypes.func.isRequired,
};

export default ArticleItem