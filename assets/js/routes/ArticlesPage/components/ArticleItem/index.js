import ArticleItem from './ArticleItem'
import enhance from './ArticleItem.enhancer'

export default enhance(ArticleItem)