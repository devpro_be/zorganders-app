import {compose} from "redux";
import {withFirebase} from "react-redux-firebase";
import {lifecycle, pure, withHandlers, withState} from "recompose";

const STORAGE_URL = 'gs://zorganders-articles';

let isMounted = false;

const ArticleItemImageEnhancer = compose(
    withFirebase,

    withState('imageSrc', 'setImageSrc', undefined),

    withHandlers({
        getImage: ({firebase, setImageSrc}) =>
            articleId => {
                firebase
                    .app()
                    .storage(STORAGE_URL)
                    .refFromURL(`${STORAGE_URL}/${articleId}__thumbnail.jpg`)
                    .getDownloadURL()
                    .then((url) => isMounted ? setImageSrc(url) : void(0))
                    .catch((err) => {});
            }
    }),

    lifecycle({
        componentWillMount() {
            isMounted = true;
            const {getImage, article} = this.props;
            const {hasImage, id} = article;
            if (hasImage) {
                getImage(id);
            }
        },
        componentWillUnmount() {
            isMounted = false;
        },
    }),

    pure
);

export default ArticleItemImageEnhancer;