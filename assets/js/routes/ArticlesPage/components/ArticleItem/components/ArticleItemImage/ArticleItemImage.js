import React from 'react'
import PropTypes from "prop-types";

const ArticleItemImage = ({imageSrc, imageSrcDefault}) => (
    <div className='image'>
        <img src={imageSrc ? imageSrc : imageSrcDefault}/>
    </div>
);

ArticleItemImage.propTypes = {
    article: PropTypes.object.isRequired,
    imageSrcDefault: PropTypes.string.isRequired,
    imageSrc: PropTypes.string,
};

export default ArticleItemImage

