import ArticleItemImage from './ArticleItemImage'
import enhance from './ArticleItemImage.enhancer'

export default enhance(ArticleItemImage)