import ArticlesPage from './ArticlesPage'
import enhance from './ArticlesPage.enhancer'

export default enhance(ArticlesPage)