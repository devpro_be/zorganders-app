import React from 'react'
import PropTypes from 'prop-types'
import {ArticleItem} from './components';
import Waypoint from 'react-waypoint';

export const ArticlesPage = ({articles, categories, hasMore, isLoading, loadMore}) => {
    return (
        <div className="articles-list">
            {categories && articles.map((article, ind) => <ArticleItem key={ind} article={article} categories={categories}/>)}
            {isLoading && (
                <div className='loading-articles'>
                    <div className="spinner">
                        <div className="bounce1"></div>
                        <div className="bounce2"></div>
                        <div className="bounce3"></div>
                    </div>
                </div>
            )}
            <Waypoint onEnter={loadMore} />
        </div>

    )
};

ArticlesPage.propTypes = {
    articles: PropTypes.array,
    categories: PropTypes.array,
    loadMore: PropTypes.func.isRequired,
    hasMore: PropTypes.bool.isRequired,
    isLoading: PropTypes.bool.isRequired,
};

export default ArticlesPage