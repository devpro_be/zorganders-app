import {compose} from 'redux'
import {lifecycle, withState} from 'recompose'
import {withHandlers, pure} from 'recompose'
import {withFirestore} from 'react-redux-firebase'
import {withRouter} from 'react-router-dom'


export default compose(
    withFirestore,
    withRouter,

    withState('categories', 'setCategories', undefined),
    withState('hasMore', 'setHasMore', true),
    withState('articles', 'setArticles', []),
    withState('startAfter', 'setStartAfter', undefined),
    withState('isLoading', 'setIsLoading', false),

    withHandlers({
        loadCategories: ({firestore, setCategories}) => () => {
            firestore
                .collection("article-categories")
                .onSnapshot(
                    (snapshot) => {
                        const categories = [];
                        snapshot.forEach(
                            (category) => {
                                categories.push({
                                    id: category.id,
                                    ...category.data(),
                                });
                            }
                        );
                        setCategories(categories);
                    }
                )
        },
        loadMore: ({firestore, articles, startAfter, setStartAfter, setArticles, setIsLoading}) => () => {

            setIsLoading(true);
            let articlesQuery = firestore
                .collection("articles")
                .orderBy("date", "desc")
                .limit(10);

            if (startAfter) {
                articlesQuery = articlesQuery.startAfter(startAfter);
            }

            articlesQuery.onSnapshot(
                (querySnapshot) => {
                    const newArticles = [];
                    let lastArticle = undefined;
                    querySnapshot.forEach(
                        (article) => {
                            newArticles.push({
                                id: article.id,
                                ...article.data()
                            });
                            lastArticle = article;
                        });

                    setStartAfter(lastArticle);

                    setArticles(articles.concat(newArticles));
                    setIsLoading(true);
                }
            );

        }
    }),

    lifecycle({
        componentDidMount() {
            // noinspection JSCheckFunctionSignatures
            this.props.loadCategories();
        },
    }),

    pure
)

