import React from 'react'
import ArticlesPage from './ArticlesPage'
import ArticlePage from './ArticlePage'
import ReportsPage from './ReportsPage'
import ReportPage from './ReportPage'
import ArticlesCategories from "./ArticlesCategories";
import ArticlesMostReadPage from "./ArticlesMostReadPage";
import {ArticlesCategory} from "./ArticlesCategories/routes";
import ReportsMostViewedPage from "./ReportsMostViewedPage";
import ReportsCategories from "./ReportsCategories";
import {ReportsCategory} from "./ReportsCategories/routes";
import BundlesPage from "./BundlesPage";

export const routes = [{
    path: '/articles',
    exact: true,
    component: ArticlesPage,
},{
    path: '/articles/categories',
    exact: true,
    component: ArticlesCategories
},{
    path: '/articles/categories/:categoryId',
    exact: true,
    component: ArticlesCategory
},{
    path: '/articles/most_read',
    exact: true,
    component: ArticlesMostReadPage
},{
    path: '/article/:articleId',
    exact: true,
    component: ArticlePage
},{
    path: '/reports',
    exact: true,
    component: ReportsPage
},{
    path: '/reports/categories',
    exact: true,
    component: ReportsCategories
},{
    path: '/reports/categories/:categoryId',
    exact: true,
    component: ReportsCategory
},{
    path: '/reports/most_viewed',
    exact: true,
    component: ReportsMostViewedPage
},{
    path: '/report/:reportId',
    component: ReportPage
},{
    path: '/reports/category/:categoryId',
    component: ReportPage
},{
    path: '/bundles/:bundleId',
    component: BundlesPage
}];

export default routes