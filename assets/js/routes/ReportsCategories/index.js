import ReportsCategories from './ReportsCategories'
import enhance from './ReportsCategories.enhancer'

export default enhance(ReportsCategories)