import {compose} from 'redux'
import {lifecycle, withState} from 'recompose'
import {withHandlers, pure} from 'recompose'
import {withFirestore} from 'react-redux-firebase'
import {withRouter} from 'react-router-dom'


export default compose(
    withFirestore,
    withRouter,

    withState('isLoading', 'setIsLoading', false),
    withState('reports', 'setReports', []),
    withState('categories', 'setCategories', undefined),
    withState('hasMore', 'setHasMore', true),
    withState('rootCategory', 'setRootCategory', undefined),
    withState('startAfter', 'setStartAfter', undefined),

    withHandlers({
        goBack: ({history}) => () => {
            history.goBack();
        },
        initRootCategory: ({firestore, rootCategory, location, setRootCategory}) => () => {
            if (rootCategory === undefined) {
                const pathParts = location.pathname.split('/');
                const rootCategoryId = pathParts[pathParts.length - 1];
                firestore
                    .collection('reports-categories')
                    .doc(rootCategoryId)
                    .get()
                    .then(
                        (doc) => {
                            if (doc.exists) {
                                const rootCategory = doc.data();
                                rootCategory.id = rootCategoryId;
                                setRootCategory(rootCategory);
                            }
                        }
                    )
            }
        },
        loadCategories: ({firestore, setCategories}) => () => {
            firestore
                .collection("reports-categories")
                .onSnapshot(
                    (snapshot) => {
                        const categories = [];
                        snapshot.forEach(
                            (category) => {
                                categories.push({
                                    id: category.id,
                                    ...category.data(),
                                });
                            }
                        );
                        setCategories(categories);
                    }
                )
        },
        loadMore: ({firestore, reports, rootCategory, startAfter, setStartAfter, setHasMore, setReports, setIsLoading}) => () => {

            const {id:rootCategoryId} = rootCategory;

            setIsLoading(true);
            let reportsQuery = firestore
                .collection("reports")
                .where('category', '==', rootCategoryId)
                .orderBy("date", "desc")
                .limit(10);

            if (startAfter) {
                reportsQuery = reportsQuery.startAfter(startAfter);
            }

            reportsQuery.onSnapshot(
                (querySnapshot) => {
                    const newReports = [];
                    let lastReport = undefined;
                    if (querySnapshot.size < 10) {
                        setHasMore(false);
                    }
                    querySnapshot.forEach(
                        (report) => {
                            newReports.push({
                                id: report.id,
                                ...report.data()
                            });
                            lastReport = report;
                        });

                    setStartAfter(lastReport);

                    setReports(reports.concat(newReports));
                    setIsLoading(false);
                }
            );

        }
    }),

    lifecycle({
        componentDidMount() {
            // noinspection JSCheckFunctionSignatures
            this.props.loadCategories();
            this.props.initRootCategory();
        },
    }),

    pure
)

