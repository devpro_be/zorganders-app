import ReportsCategory from './ReportsCategory'
import enhance from './ReportsCategory.enhancer'

export default enhance(ReportsCategory)