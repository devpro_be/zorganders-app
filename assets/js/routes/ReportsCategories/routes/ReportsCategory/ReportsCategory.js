import React from 'react'
import PropTypes from 'prop-types'
import {ReportItem} from './../../../ReportsPage/components';
import Waypoint from 'react-waypoint';

export const ReportsCategory = ({rootCategory, categories, hasMore, reports, isLoading, loadMore, goBack}) => {
    return (
        <div className="articles-list" style={{marginBottom: 50}}>
            {!categories && !rootCategory && (
                <div className='loading-articles'>
                    <div className="spinner">
                        <div className="bounce1"/>
                        <div className="bounce2"/>
                        <div className="bounce3"/>
                    </div>
                </div>
            )}
            {categories && reports.map((report, ind) => <ReportItem key={ind} report={report} categories={categories}/>)}
            {!isLoading && hasMore && categories && rootCategory && <Waypoint onEnter={loadMore}/> }
            {isLoading && (
                <div className='loading-articles'>
                    <div className="spinner">
                        <div className="bounce1"/>
                        <div className="bounce2"/>
                        <div className="bounce3"/>
                    </div>
                </div>
            )}
            <div className='app-footer'>
                <span className='back' onClick={() => goBack()}>&lt; TERUG NAAR CATEGORIEËN</span>
            </div>
            <div id="hide-bottom-notch" />
        </div>

    )
};

ReportsCategory.propTypes = {
    isLoading: PropTypes.bool.isRequired,
    rootCategory: PropTypes.object,
};

export default ReportsCategory