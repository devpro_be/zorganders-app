import {compose} from 'redux'
import {lifecycle, withState} from 'recompose'
import {withHandlers, pure} from 'recompose'
import {withFirestore} from 'react-redux-firebase'
import {withRouter} from 'react-router-dom'


export default compose(
    withFirestore,
    withRouter,

    withState('categories', 'setCategories', undefined),
    withState('isLoading', 'setIsLoading', true),

    withHandlers({
        loadCategories: ({firestore, setCategories, setIsLoading}) => () => {
            firestore
                .collection("reports-categories")
                .onSnapshot(
                    (snapshot) => {
                        const categories = [];
                        snapshot.forEach(
                            (category) => {
                                categories.push({
                                    id: category.id,
                                    ...category.data(),
                                });
                            }
                        );
                        setCategories(categories);
                        setIsLoading(false);
                    }
                )
        },
        openCategory: ({history}) => (category) => {
            const {id} = category;
            history.push(`/reports/categories/${id}`)
        },
    }),

    lifecycle({
        componentDidMount() {
            // noinspection JSCheckFunctionSignatures
            this.props.loadCategories();
        },
    }),

    pure
)

