import ReportsMostViewedPage from './ReportsMostViewedPage'
import enhance from './ReportsMostViewedPage.enhancer'

export default enhance(ReportsMostViewedPage)