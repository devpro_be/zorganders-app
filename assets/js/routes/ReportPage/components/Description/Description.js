import React from 'react'
import PropTypes from "prop-types";

const Description = ({reportId, reportContent}) => {
    return (
        <div className='content' dangerouslySetInnerHTML={{__html: reportContent}} />
    )};

Description.propTypes = {
    repportId: PropTypes.string,
    reportContent: PropTypes.string,
    getContent: PropTypes.func.isRequired,
};

export default Description

