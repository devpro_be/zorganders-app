import Description from './Description'
import enhance from './Description.enhancer'

export default enhance(Description)