import {compose} from "redux";
import {withFirebase} from "react-redux-firebase";
import {lifecycle, pure, withHandlers, withState} from "recompose";

let isMounted = false;
let loading = undefined;
let content = {};
const STORAGE_URL = 'gs://zorganders-reports';

const ArticleContentEnhancer = compose(
    withFirebase,

    withState('reportContent', 'setReportContent', undefined),

    withHandlers({
        getContent: ({firebase, setReportContent}) =>
            reportId => {
                if (loading === reportId || content[reportId]) {
                    setReportContent(content[reportId]);
                } else {
                    loading = reportId;
                    firebase
                        .app()
                        .storage(STORAGE_URL)
                        .refFromURL(`${STORAGE_URL}/${reportId}.html`)
                        .getDownloadURL()
                        .then(
                            (url) => {
                                fetch(url)
                                    .then(response => response.text())
                                    .then(html => {
                                        if (isMounted) {
                                            content[reportId] = html;
                                            setReportContent(html);
                                        }
                                    });
                            }
                        )
                }
            }
    }),

    lifecycle({
        componentDidMount() {
            isMounted = true;
            const {getContent, reportId} = this.props;
            getContent(reportId);
        },
        componentWillUnmount() {
            isMounted = false;
        },
    }),

    pure
);

export default ArticleContentEnhancer;