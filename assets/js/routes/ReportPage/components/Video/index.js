import Video from './Video'
import enhance from './Video.enhancer'

export default enhance(Video)