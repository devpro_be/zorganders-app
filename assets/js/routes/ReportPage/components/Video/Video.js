import React from 'react'
import PropTypes from 'prop-types'

const Video = ({report, height}) => {

    const video = report && report.video ? report.video : null;
    const formatVideoUrl = (videoId) => `https://player.vimeo.com/video/${videoId}?title=1&byline=1&portait=0`;
    return (
        <div className='video'>
            {
                !video
                &&
                (<div style={{width:"100%", height:height}}>Loading...</div>)
            }
            {
                video
                &&
                <iframe
                    title={report.title}
                    width="100%"
                    height={height}
                    src={formatVideoUrl(report.video)}
                    frameBorder="0"
                    allowFullScreen={true}
                />
            }
        </div>
    )
}

Video.propTypes = {
    report: PropTypes.object,
    height: PropTypes.string
};

export default Video;