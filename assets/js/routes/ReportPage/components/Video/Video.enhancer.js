import {compose} from 'redux'
import {lifecycle, withState} from 'recompose'
import {withHandlers, pure} from 'recompose'

// noinspection JSUnusedGlobalSymbols
export default compose(

    withState('height', 'setHeight', '0px'),

    withHandlers({
        calculateHeight: ({setHeight}) =>
            () => {
                const ratio = 16/9;
                const height = `${window.innerWidth / ratio}px`;
                setHeight(height);
            }
    }),

    lifecycle({
        componentDidMount() {
            const {calculateHeight} = this.props;
            window.addEventListener('resize', calculateHeight);
            calculateHeight();
        },
        componentWillUnmount() {
            const {calculateHeight} = this.props;
            window.removeEventListener('resize', calculateHeight);
        }
    }),

    pure
)
