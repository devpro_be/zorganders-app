import ReportPage from './ReportPage'
import enhance from './ReportPage.enhancer'

export default enhance(ReportPage);