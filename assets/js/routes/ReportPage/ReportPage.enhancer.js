import {compose} from 'redux'
import {lifecycle, withState} from 'recompose'
import {withHandlers, pure} from 'recompose'
import {withFirestore} from "react-redux-firebase";
import {withRouter} from "react-router-dom";

// noinspection JSUnusedGlobalSymbols
export default compose(
    withFirestore,
    withRouter,
    withState('report', 'setReport', undefined),

    withHandlers({
        initReport: ({firestore, report, location, setReport}) => () => {
            if (report === undefined) {
                const pathParts = location.pathname.split('/');
                const reportId = pathParts[pathParts.length - 1];
                const reportRef = firestore
                    .collection('reports')
                    .doc(reportId);

                reportRef.get()
                    .then(
                        (doc) => {
                            if (doc.exists) {
                                const report = doc.data();
                                report.id = reportId;
                                setReport(report);
                                const count = report.count ? report.count + 1 : 1;
                                reportRef.update({
                                    count
                                });
                            }
                        }
                    )
            }
        },
        goBack: ({history}) => () => {
            history.goBack();
        },
    }),


    lifecycle({
        componentDidMount() {
            const {initReport} = this.props;
            initReport();
        }
    }),

    pure
)

