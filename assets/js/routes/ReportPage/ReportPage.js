import React from 'react'
import {Description, Video} from './components'


const ReportPage = ({report = {}, goBack}) => {
    const {id: reportId, title} = report;

    return (
        <div className='report'>

            <Video
                report={report}
            />
            <h1 className='title'>{title}</h1>
            {
                reportId
                &&
                <Description reportId={reportId}>
                    Reportage
                </Description>
            }
            <div className='app-footer'>
                <span className='back' onClick={() => goBack()}>&lt; TERUG NAAR OVERZICHT</span>
            </div>
            <div id="hide-bottom-notch" />
        </div>
    )
};

export default ReportPage