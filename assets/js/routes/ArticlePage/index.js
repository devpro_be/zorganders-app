import ArticlesPage from './ArticlePage'
import enhance from './ArticlePage.enhancer'

export default enhance(ArticlesPage)