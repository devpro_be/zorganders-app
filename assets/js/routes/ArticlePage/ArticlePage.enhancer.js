import {compose} from 'redux'
import {lifecycle, withState} from 'recompose'
import {withHandlers, pure} from 'recompose'
import {withFirestore} from 'react-redux-firebase'
import {withRouter} from 'react-router-dom'

export default compose(
    pure,
    withFirestore,
    withRouter,

    withState('article', 'setArticle', undefined),

    withHandlers({
        goBack: ({history}) => () => {
            history.goBack();
        },
        initArticle: ({firestore, article, location, setArticle}) => err => {
            if (article == undefined) {
                const pathParts = location.pathname.split('/');
                const articleId = pathParts[pathParts.length - 1];
                const articleRef = firestore.collection('articles').doc(articleId);
                articleRef.get().then(
                    (doc) => {
                        if (doc.exists) {
                            const article = doc.data();
                            article.id = articleId;
                            setArticle(article);
                            const count = article.count ? article.count + 1 : 1;
                            // Update the count
                            articleRef
                                .update({
                                    count
                                });
                        }
                    }
                )
            }
        }
    }),

    lifecycle({
        componentDidMount() {
            this.props.initArticle();
        }
    }),

    pure
)

