import React from 'react'
import PropTypes from 'prop-types'
import {ArticleImage,ArticleContent} from './components';
import {formatDate} from "../../helpers/date";


export const ArticlePage = ({article, goBack}) => {
    return (
        <div className='article'>
            {article && <ArticleImage articleId={article.id} defaultImgSrc='images/articles/article_large_default.jpg' /> }
            {article && <h1 className='title'>{article.title}</h1>}
            {article && <div className='date'>{formatDate(article.date)}</div>}
            {article && <ArticleContent articleId={article.id} /> }
            <div className='app-footer'>
                <span className='back' onClick={() => goBack()}>&lt; TERUG NAAR OVERZICHT</span>
            </div>
            <div id="hide-bottom-notch" />
        </div>
    )
};

ArticlePage.propTypes = {
    article: PropTypes.object,
    goBack: PropTypes.func.isRequired,
};

export default ArticlePage