import {compose} from "redux";
import {withFirebase} from "react-redux-firebase";
import {lifecycle, pure, withHandlers, withState} from "recompose";

let isMounted = false;
let loading = undefined;
let content = {};
const STORAGE_URL = 'gs://zorganders-articles';

const ArticleContentEnhancer = compose(
    withFirebase,

    withState('articleContent', 'setArticleContent', undefined),

    withHandlers({
        getContent: ({firebase, setArticleContent}) =>
            articleId => {
                if (loading === articleId || content[articleId]) {
                    setArticleContent(content[articleId]);
                } else {
                    loading = articleId;
                    firebase
                        .app()
                        .storage(STORAGE_URL)
                        .refFromURL(`${STORAGE_URL}/${articleId}.html`)
                        .getDownloadURL()
                        .then(
                            (url) => {
                                fetch(url)
                                    .then(response => response.text())
                                    .then(html => {
                                        if (isMounted) {
                                            content[articleId] = html;
                                            setArticleContent(html);
                                        }
                                    });
                            }
                        )
                }
            }
    }),

    lifecycle({
        componentDidMount() {
            isMounted = true;
            const {getContent, articleId} = this.props;
            getContent(articleId);
        },
        componentWillUnmount() {
            isMounted = false;
        },
    }),

    pure
);

export default ArticleContentEnhancer;