import ArticleContent from './ArticleContent';
import enhancer from './ArticleContent.enhancer';

export default enhancer(ArticleContent);