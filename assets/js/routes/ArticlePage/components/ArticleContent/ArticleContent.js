import React from 'react'
import PropTypes from "prop-types";
import ArticleContentEnhancer from './ArticleContent.enhancer';

const ArticleContent = ({articleId, articleContent}) => {
    return (
    <div className='content' dangerouslySetInnerHTML={{__html: articleContent}} />
)};

ArticleContent.propTypes = {
    articleId: PropTypes.string,
    articleContent: PropTypes.string,
    getContent: PropTypes.func.isRequired,
};

export default ArticleContentEnhancer(ArticleContent)

