import {compose} from "redux";
import {withFirebase} from "react-redux-firebase";
import {lifecycle, pure, withHandlers, withState} from "recompose";

let isMounted = false;
let loadingArticleImage = undefined;
let loadedArticleImages = {};
const STORAGE_URL = 'gs://zorganders-articles';

const noopFunc = () => {
};

const ArticleImageEnhancer = compose(
    withFirebase,

    withState('articleImgSrc', 'setArticleImgSrc', undefined),

    withHandlers({
        getImage: ({firebase, setArticleImgSrc}) =>
            articleId => {
                if (loadedArticleImages[articleId]) {
                    setArticleImgSrc(loadedArticleImages[articleId]);
                } else if (loadingArticleImage !== articleId) {
                    loadingArticleImage = articleId;
                    firebase
                        .app()
                        .storage(STORAGE_URL)
                        .refFromURL(`${STORAGE_URL}/${articleId}__large.jpg`)
                        .getDownloadURL()
                        .then(url => {
                            if (isMounted) {
                                loadingArticleImage = undefined;
                                loadedArticleImages[articleId] = url;
                                setArticleImgSrc(url);
                            }
                        }).catch(noopFunc);
                }
            }
    }),

    lifecycle({
        componentDidMount() {
            isMounted = true;
            const {getImage, articleId} = this.props;
            getImage(articleId);
        },
        componentWillUnmount() {
            isMounted = false;
        },
    }),

    pure
);

export default ArticleImageEnhancer;