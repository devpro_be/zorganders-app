import ArticleImage from './ArticleImage';
import enhancer from './ArticleImage.enhancer';

export default enhancer(ArticleImage);