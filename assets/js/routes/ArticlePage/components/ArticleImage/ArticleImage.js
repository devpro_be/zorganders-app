import React from 'react'
import PropTypes from "prop-types";
import ArticleImageEnhancer from './ArticleImage.enhancer';

const ArticleImage = ({articleId, articleImgSrc, defaultImgSrc}) => {
    return (
        <div className='image'>
            <img src={articleImgSrc ? articleImgSrc : defaultImgSrc}/>
        </div>
    );
};

ArticleImage.propTypes = {
    articleId: PropTypes.string,
    defaultImgSrc: PropTypes.string.isRequired,
    articleImgSrc: PropTypes.string,
    getImage: PropTypes.func.isRequired,
};

export default ArticleImageEnhancer(ArticleImage)

