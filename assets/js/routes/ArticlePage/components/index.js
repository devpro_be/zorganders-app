import ArticleContent from './ArticleContent'
import ArticleImage from './ArticleImage'

export {
    ArticleContent,
    ArticleImage
};