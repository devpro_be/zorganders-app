import React from 'react'
import PropTypes from 'prop-types'
import {ArticleItem} from './../ArticlesPage/components';
import Waypoint from 'react-waypoint';

export const ArticlesMostReadPage = ({articles, categories, hasMore, isLoading, loadMore}) => {
    return (
        <div className="articles-list">
            {categories && articles.map((article, ind) => <ArticleItem key={ind} article={article} categories={categories}/>)}
            {isLoading && (
                <div className='loading-articles'>
                    <div className="spinner">
                        <div className="bounce1"/>
                        <div className="bounce2"/>
                        <div className="bounce3"/>
                    </div>
                </div>
            )}
            {!isLoading && hasMore && <Waypoint onEnter={loadMore} />}
        </div>

    )
};

ArticlesMostReadPage.propTypes = {
    articles: PropTypes.array,
    categories: PropTypes.array,
    loadMore: PropTypes.func.isRequired,
    hasMore: PropTypes.bool.isRequired,
    isLoading: PropTypes.bool.isRequired,
};

export default ArticlesMostReadPage