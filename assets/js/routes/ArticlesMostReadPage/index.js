import ArticlesMostReadPage from './ArticlesMostReadPage'
import enhance from './ArticlesMostReadPage.enhancer'

export default enhance(ArticlesMostReadPage)