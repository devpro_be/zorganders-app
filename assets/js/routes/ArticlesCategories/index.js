import ArticlesCategories from './ArticlesCategories'
import enhance from './ArticlesCategories.enhancer'

export default enhance(ArticlesCategories)