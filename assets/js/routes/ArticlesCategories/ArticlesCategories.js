import React from 'react'
import PropTypes from 'prop-types'

export const ArticlesCategories = ({categories, isLoading, openCategory}) => {
    return (
        <div>
            {isLoading && (
                <div className='loading-articles'>
                    <div className="spinner">
                        <div className="bounce1"/>
                        <div className="bounce2"/>
                        <div className="bounce3"/>
                    </div>
                </div>
            )}
            {!isLoading && categories &&
            <ul className='category-list'>
                {
                    categories.map(
                        (category, idx) => {
                            const {name} = category;
                            const onClick = () => openCategory(category);
                            return (
                                <li key={`category-${idx}`} className='item' onClick={onClick}>{name}</li>
                            );
                        }
                    )
                }
            </ul>
            }
        </div>

    )
};

ArticlesCategories.propTypes = {
    categories: PropTypes.array,
    isLoading: PropTypes.bool.isRequired,
    openCategory: PropTypes.func.isRequired,
};

export default ArticlesCategories