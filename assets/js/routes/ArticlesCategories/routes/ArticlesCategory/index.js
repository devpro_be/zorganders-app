import ArticlesCategory from './ArticlesCategory'
import enhance from './ArticlesCategory.enhancer'

export default enhance(ArticlesCategory)