import {compose} from 'redux'
import {withState} from 'recompose'
import {lifecycle, withHandlers, pure} from 'recompose'
import {withFirestore} from 'react-redux-firebase'
import {withRouter} from 'react-router-dom'

let isMounted = false;

export default compose(
    withFirestore,
    withRouter,

    withState('categories', 'setCategories', undefined),
    withState('hasMore', 'setHasMore', true),
    withState('reports', 'setReports', []),
    withState('startAfter', 'setStartAfter', undefined),
    withState('isLoading', 'setIsLoading', false),

    withHandlers({
        loadCategories: ({firestore, setCategories}) => () => {
            firestore
                .collection("reports-categories")
                .onSnapshot(
                    (snapshot) => {
                        const categories = [];

                        snapshot.forEach(
                            (category) => {
                                categories.push({
                                    id: category.id,
                                    ...category.data(),
                                });
                            }
                        );
                        if (isMounted) {
                            setCategories(categories);
                        }
                    }
                )
        },
        loadMore: ({firestore, reports, startAfter, setStartAfter, setReports, setHasMore, setIsLoading}) => () => {

            setIsLoading(true);
            let articlesQuery = firestore
                .collection("reports")
                .orderBy("date", "desc")
                .limit(10);

            if (startAfter) {
                articlesQuery = articlesQuery.startAfter(startAfter);
            }

            articlesQuery.onSnapshot(
                (querySnapshot) => {
                    const newReports = [];
                    let lastReport = undefined;
                    if (querySnapshot.size < 10) {
                        setHasMore(false);
                    }
                    querySnapshot.forEach(
                        (report) => {
                            newReports.push({
                                id: report.id,
                                ...report.data()
                            });
                            lastReport = report;
                        });

                    if (isMounted) {
                        setStartAfter(lastReport);

                        setReports(reports.concat(newReports));
                        setIsLoading(false);
                    }
                }
            );

        }
    }),

    lifecycle({
        componentDidMount() {
            isMounted = true;
            // noinspection JSCheckFunctionSignatures
            this.props.loadCategories();
        },
        componentWillUnmount() {
            isMounted = false;
        },
    }),
    pure
)

