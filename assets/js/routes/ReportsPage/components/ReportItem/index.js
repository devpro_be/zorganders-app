import ReportItem from './ReportItem'
import enhance from './ReportItem.enhancer'

export default enhance(ReportItem)