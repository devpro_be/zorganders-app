import {compose} from 'redux'
import {withHandlers, pure} from 'recompose'
import {withRouter} from 'react-router-dom'

export default compose(
    withRouter,
    withHandlers({
        goToReport: ({history}) => reportId => {
            history.push(`/report/${reportId}`)
        },
    }),
    pure
)