import ReportItemImage from './ReportItemImage'
import enhance from './ReportItemImage.enhancer'

export default enhance(ReportItemImage);