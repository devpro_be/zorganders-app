import React from 'react'
import PropTypes from "prop-types";

const ReportItemImage = ({imageSrc, imageSrcDefault}) => (
    <div className='image'>
        <img src={imageSrc ? imageSrc : imageSrcDefault}/>
    </div>
);

ReportItemImage.propTypes = {
    report: PropTypes.object.isRequired,
    imageSrcDefault: PropTypes.string.isRequired,
    imageSrc: PropTypes.string,
};

export default ReportItemImage

