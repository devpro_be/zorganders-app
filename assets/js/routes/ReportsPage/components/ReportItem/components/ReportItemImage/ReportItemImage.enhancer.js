import {compose} from "redux";
import {withFirebase} from "react-redux-firebase";
import {lifecycle, pure, withHandlers, withState} from "recompose";

const STORAGE_URL = 'gs://zorganders-reports';

const ReportItemImageEnhancer = compose(
    withFirebase,

    withState('imageSrc', 'setImageSrc', undefined),

    withHandlers({
        getImage: ({firebase, setImageSrc}) =>
            reportId => {
                firebase
                    .app()
                    .storage(STORAGE_URL)
                    .refFromURL(`${STORAGE_URL}/${reportId}.jpg`)
                    .getDownloadURL()
                    .then((url) => setImageSrc(url))
                    .catch((err) => {});
            }
    }),

    lifecycle({
        componentWillMount() {
            const {getImage, report} = this.props;
            const {hasImage, id} = report;
            if (hasImage) {
                getImage(id);
            }
        }
    }),

    pure
);

export default ReportItemImageEnhancer;