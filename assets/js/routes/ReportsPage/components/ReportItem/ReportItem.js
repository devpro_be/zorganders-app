import React from 'react'
import PropTypes from 'prop-types'
import {ReportItemImage} from "./components";

export const ReportItem = ({report, categories, goToReport}) => {

    const {id, title, category} = report;

    const timestamp = Number(report.date);
    const date = new Date(timestamp);
    const dateFormatted = `${date.getDate()}/${date.getMonth()+1}/${date.getFullYear()}`;

    const formattedCategory = categories.find((cat) => cat.id === category);

    const onClick = () => {
        goToReport(id);
    };

    return (
        <div
            key={`report-${id}`}
            className='report-item'
            onClick={onClick}
        >
            <ReportItemImage
                imageSrcDefault='images/articles/article_thumbnail_default.jpg'
                report={report}
            />
            <div className='details'>
                <div className='title'>{title}</div>
                <div className='date'>{dateFormatted}</div>
                <div className='category'>{formattedCategory && formattedCategory.name}</div>
            </div>
        </div>
    )
};

ReportItem.propTypes = {
    report: PropTypes.object.isRequired,
    categories: PropTypes.array.isRequired,
    goToReport: PropTypes.func.isRequired,
};

export default ReportItem