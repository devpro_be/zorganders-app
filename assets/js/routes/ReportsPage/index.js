import ReportsPage from './ReportsPage'
import enhance from './ReportsPage.enhancer'

export default enhance(ReportsPage)