import React from 'react'
import PropTypes from 'prop-types'
import {ReportItem} from './components';
import Waypoint from 'react-waypoint';

export const ReportsPage = ({reports, hasMore, categories, isLoading, loadMore}) => {
    return (
        <div className="articles-list">
            {categories && reports.map((report, ind) => <ReportItem key={ind} report={report} categories={categories}/>)}
            {isLoading && (
                <div className='loading-articles'>
                    <div className="spinner">
                        <div className="bounce1"/>
                        <div className="bounce2"/>
                        <div className="bounce3"/>
                    </div>
                </div>
            )}
            {!isLoading && hasMore && <Waypoint onEnter={loadMore} />}
        </div>

    )
};

ReportsPage.propTypes = {
    articles: PropTypes.array,
    categories: PropTypes.array,
    loadMore: PropTypes.func.isRequired,
    hasMore: PropTypes.bool.isRequired,
    isLoading: PropTypes.bool.isRequired,
};

export default ReportsPage