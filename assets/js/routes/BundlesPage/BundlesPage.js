import React from 'react'
import PropTypes from 'prop-types'
import {ArticleItem} from './../ArticlesPage/components';
import Waypoint from 'react-waypoint';

export const BundlesPage = ({rootCategory, categories, articles, hasMore, isLoading, loadMore, goBack}) => {
    return (
        <div className="articles-list" style={{marginBottom: 50}}>
            {!categories && !rootCategory && (
                <div className='loading-articles'>
                    <div className="spinner">
                        <div className="bounce1"/>
                        <div className="bounce2"/>
                        <div className="bounce3"/>
                    </div>
                </div>
            )}
            {categories && articles.map((article, ind) => <ArticleItem key={ind} article={article} categories={categories}/>)}
            {!isLoading && hasMore && categories && rootCategory && <Waypoint onEnter={loadMore}/> }
            {isLoading && (
                <div className='loading-articles'>
                    <div className="spinner">
                        <div className="bounce1"/>
                        <div className="bounce2"/>
                        <div className="bounce3"/>
                    </div>
                </div>
            )}
        </div>

    )
};

BundlesPage.propTypes = {
    isLoading: PropTypes.bool.isRequired,
    rootCategory: PropTypes.object,
};

export default BundlesPage