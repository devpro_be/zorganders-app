import {compose} from 'redux'
import {lifecycle, withState} from 'recompose'
import {withHandlers, pure} from 'recompose'
import {withFirestore} from 'react-redux-firebase'
import {withRouter} from 'react-router-dom'


export default compose(
    withFirestore,
    withRouter,

    withState('isLoading', 'setIsLoading', false),
    withState('articles', 'setArticles', []),
    withState('categories', 'setCategories', undefined),
    withState('hasMore', 'setHasMore', true),
    withState('rootCategory', 'setRootCategory', undefined),
    withState('startAfter', 'setStartAfter', undefined),

    withHandlers({
        goBack: ({history}) => () => {
            history.goBack();
        },
        initRootCategory: ({firestore, rootCategory, articles, startAfter, location, setRootCategory, setStartAfter, setArticles}) => () => {
                const pathParts = location.pathname.split('/');
                const rootCategoryId = pathParts[pathParts.length - 1];
                firestore
                    .collection('magazines')
                    .doc(rootCategoryId)
                    .get()
                    .then(
                        (doc) => {
                            if (doc.exists) {
                                const rootCategory = doc.data();
                                rootCategory.id = rootCategoryId;
                                setRootCategory(rootCategory);
                                if (startAfter) {
                                    setStartAfter(undefined);
                                }
                                if (articles.length){
                                    setArticles([]);
                                }
                            }
                        }
                    )
        },
        loadCategories: ({firestore, setCategories}) => () => {
            firestore
                .collection("article-categories")
                .onSnapshot(
                    (snapshot) => {
                        const categories = [];
                        snapshot.forEach(
                            (category) => {
                                categories.push({
                                    id: category.id,
                                    ...category.data(),
                                });
                            }
                        );
                        setCategories(categories);
                    }
                )
        },
        loadMore: ({firestore, articles, rootCategory, startAfter, setStartAfter, setHasMore, setArticles, setIsLoading}) => () => {

            const {id:rootCategoryId} = rootCategory;

            setIsLoading(true);
            let articlesQuery = firestore
                .collection("articles")
                .where('magazines', 'array-contains', rootCategoryId)
                .orderBy("date", "desc")
                .limit(10);

            if (startAfter) {
                articlesQuery = articlesQuery.startAfter(startAfter);
            }

            articlesQuery.onSnapshot(
                (querySnapshot) => {
                    const newArticles = [];
                    let lastArticle = undefined;
                    if (querySnapshot.size < 10) {
                        setHasMore(false);
                    }
                    querySnapshot.forEach(
                        (article) => {
                            newArticles.push({
                                id: article.id,
                                ...article.data()
                            });
                            lastArticle = article;
                        });

                    setStartAfter(lastArticle);

                    setArticles(articles.concat(newArticles));
                    setIsLoading(false);
                }
            );

        }
    }),

    lifecycle({
        componentDidMount() {
            // noinspection JSCheckFunctionSignatures
            this.props.loadCategories();
            this.props.initRootCategory();
        },
        componentWillReceiveProps(nextProps) {
            const {location} = this.props;
            const {location: nextLocation} = nextProps;
            const samePath = location && nextLocation && location.pathname === nextLocation.pathname;
            if (!samePath) {
                this.props.initRootCategory();
            }
        }
    }),

    pure
)

