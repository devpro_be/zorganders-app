import BundlesPage from './BundlesPage'
import enhance from './BundlesPage.enhancer'

export default enhance(BundlesPage)