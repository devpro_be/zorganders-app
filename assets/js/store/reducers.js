import { combineReducers } from 'redux'
import { firebaseReducer as firebase } from 'react-redux-firebase'
import { reducer as firestore } from 'redux-firestore'

export const makeRootReducer = asyncReducers => {
    return combineReducers({
        // Add sync reducers here
        firebase,
        firestore,
        ...asyncReducers
    })
};

export const injectReducer = (store, { key, reducer }) => {
    store.asyncReducers[key] = reducer;
    store.replaceReducer(makeRootReducer(store.asyncReducers))
};

export default makeRootReducer