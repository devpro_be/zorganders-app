import {
    SECTION__ARTICLES,
    SECTION__REPORTS,
    SECTION__BUNDLES,
} from "../constants/sections";

import { getSectionFromPath } from './section';

const THEME_UNDEFINED = 'app-theme--undefined';
const THEME_LIGHT_GREEN = 'app-theme--light-green';
const THEME_GREEN = 'app-theme--green';
const THEME_DARK_GREEN = 'app-theme--dark-green';

const getThemeFromPath = (path) => {
    let theme = THEME_UNDEFINED;
    const currentSection = getSectionFromPath(path);

    switch (currentSection) {
        case SECTION__ARTICLES:
            theme = THEME_LIGHT_GREEN;
            break;
        case SECTION__REPORTS:
            theme = THEME_GREEN;
            break;
        case SECTION__BUNDLES:
            theme = THEME_DARK_GREEN;
            break;
    }

    return theme;
};

export {
    getThemeFromPath,
}

