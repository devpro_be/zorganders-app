import {
    SECTION__UNDEFINED,
    SECTION__ARTICLES,
    SECTION__REPORTS,
    SECTION__BUNDLES,
    SUBSECTION__ITEM,
    SUBSECTION__MOST_READ,
    SUBSECTION__OVERVIEW,
    FIRST_PATH_PART__BASE,
    FIRST_PATH_PART__ARTICLES,
    FIRST_PATH_PART__REPORTS,
    FIRST_PATH_PART__BUNDLES,
    SECOND_PATH_PART__OVERVIEW,
    SECOND_PATH_PART__ITEM,
    SECOND_PATH_PART__MOST_READ,
    SECOND_PATH_PART__CATEGORIES, SUBSECTION__CATEGORIES,
} from '~/constants/sections';

const getSectionFromPath = (path) => {
    let result = SECTION__UNDEFINED;
    // Minimize the path to the first section
    const firstSection = path.split('/')[1];

    switch (firstSection.toLowerCase()) {
        case FIRST_PATH_PART__BASE:
        case FIRST_PATH_PART__ARTICLES:
            result = SECTION__ARTICLES;
            break;
        case FIRST_PATH_PART__REPORTS:
            result = SECTION__REPORTS;
            break;
        case FIRST_PATH_PART__BUNDLES:
            result = SECTION__BUNDLES;
            break;
    }

    return result;
};

const getSubsectionFromPath = (path, parent) => {
    let result = undefined;

    if (parent) {
        let secondSection = path.split('/')[2];
        // Check if the second subsection exists in parent
        if (secondSection) {
            secondSection = `subsection-${secondSection.toLowerCase()}`;
            const child = parent.subsections.find(
                (subsection) => subsection.section === secondSection
            );
            if (child) {
                result = secondSection;
            }
        } else {
            result = parent.defaultSubsection;
        }
    }
    return result;
};


export {
    getSectionFromPath,
    getSubsectionFromPath,
};