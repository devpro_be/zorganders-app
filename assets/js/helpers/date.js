const addLeadingZero = (number) => {
    return number < 10 ? `0${number}` : number;
};

const formatDate = (firebaseTimestamp) => {
    const timestamp = Number(firebaseTimestamp);
    const date = new Date(timestamp);
    const day = addLeadingZero(date.getDate());
    const month = addLeadingZero(date.getMonth());
    const year = date.getFullYear();
    return `${day}/${month}/${year}`;
};

export {
    formatDate
}
