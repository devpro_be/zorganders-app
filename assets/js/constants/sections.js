const SECTION__UNDEFINED = 'section-undefined';
const SECTION__ARTICLES = 'section-articles';
const SECTION__REPORTS = 'section-reports';
const SECTION__BUNDLES = 'section-bundles';

const SUBSECTION__CATEGORIES = 'subsection-categories';
const SUBSECTION__ITEM = 'subsection-item';
const SUBSECTION__OVERVIEW = 'subsection-overview';
const SUBSECTION__MOST_READ = 'subsection-most-read';

const FIRST_PATH_PART__BASE = '';
const FIRST_PATH_PART__ARTICLES = 'articles';
const FIRST_PATH_PART__REPORTS = 'reports';
const FIRST_PATH_PART__BUNDLES = 'bundles';

const SECOND_PATH_PART__OVERVIEW = 'overview';
const SECOND_PATH_PART__ITEM = 'id';
const SECOND_PATH_PART__MOST_READ = 'most-read';
const SECOND_PATH_PART__CATEGORIES = 'categories';

export {
    SECTION__BUNDLES,
    SECTION__UNDEFINED,
    SECTION__ARTICLES,
    SECTION__REPORTS,
    SUBSECTION__CATEGORIES,
    SUBSECTION__ITEM,
    SUBSECTION__OVERVIEW,
    SUBSECTION__MOST_READ,

    FIRST_PATH_PART__BASE,
    FIRST_PATH_PART__ARTICLES,
    FIRST_PATH_PART__REPORTS,
    FIRST_PATH_PART__BUNDLES,

    SECOND_PATH_PART__OVERVIEW,
    SECOND_PATH_PART__ITEM,
    SECOND_PATH_PART__MOST_READ,
    SECOND_PATH_PART__CATEGORIES,
}
