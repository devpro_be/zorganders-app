const PATH_BASE = '/';

const PATH_ARTICLES = '/articles';
const PATH_ARTICLES__CATEGORIES = '/articles/categories';
const PATH_ARTICLES__MOST_READ = '/articles/most_read';

const PATH_ARTICLE = '/article/:id';

const PATH_BUNDLES = '/bundles';
const PATH_BUNDLES__ZORG_MAGAZINE = '/bundles/ZORG-MAGAZINE';
const PATH_BUNDLES__ACTUAL_CARE = '/bundles/ACTUAL-CARE';
const PATH_BUNDLES__ZORG_TECHNIEK = '/bundles/ZORG-EN-TECHNIEK';
const PATH_BUNDLES__UROBEL = '/bundles/UROBEL';

const PATH_REPORTS = '/reports';
const PATH_REPORTS__CATEGORIES = '/reports/categories';
const PATH_REPORTS__MOST_VIEWS = '/reports/most_viewed';

const PATH_REPORT = '/report/:id';


const SUBSECTION__CATEGORIES = 'subsection-categories';
const SUBSECTION__ITEM = 'subsection-item';
const SUBSECTION__OVERVIEW = 'subsection-overview';
const SUBSECTION__MOST_READ = 'subsection-most_read';
const SUBSECTION__MOST_VIEWED = 'subsection-most_viewed';

import {
    SECTION__ARTICLES,
    SECTION__REPORTS,
    SECTION__BUNDLES,
} from "./sections";


const NAVIGATION_CONFIG = [
    {
        path: PATH_ARTICLES,
        section: SECTION__ARTICLES,
        label: 'Nieuws',
        defaultSubsection: SUBSECTION__OVERVIEW,
        subsections: [
            {
                path: PATH_ARTICLES,
                section: SUBSECTION__OVERVIEW,
                label: 'Overzicht'
            }, {
                path: PATH_ARTICLES__MOST_READ,
                section: SUBSECTION__MOST_READ,
                label: 'Meest gelezen'
            }, {
                path: PATH_ARTICLES__CATEGORIES,
                section: SUBSECTION__CATEGORIES,
                label: 'Categorieën'
            },
        ]
    }, {
        path: PATH_REPORTS,
        section: SECTION__REPORTS,
        label: 'Reportages',
        defaultSubsection: SUBSECTION__OVERVIEW,
        subsections: [
            {
                path: PATH_REPORTS,
                section: SUBSECTION__OVERVIEW,
                label: 'Overzicht'
            }, {
                path: PATH_REPORTS__MOST_VIEWS,
                section: SUBSECTION__MOST_VIEWED,
                label: 'Meest bekeken'
            }, {
                path: PATH_REPORTS__CATEGORIES,
                section: SUBSECTION__CATEGORIES,
                label: 'Categorieën'
            },
        ]
    }, {
        path: PATH_BUNDLES__ZORG_MAGAZINE,
        section: SECTION__BUNDLES,
        label: 'Magazines',
        defaultSubsection: 'subsection-ZORG-MAGAZINE',
        subsections: [
            {
                path: PATH_BUNDLES__ZORG_MAGAZINE,
                section: 'subsection-zorg-magazine',
                label: 'Zorg Magazine'
            }, {
                path: PATH_BUNDLES__ACTUAL_CARE,
                section: 'subsection-actual-care',
                label: 'Actual Care'
            }, {
                path: PATH_BUNDLES__UROBEL,
                section: 'subsection-urobel',
                label: 'Urobel'
            }, {
                path: PATH_BUNDLES__ZORG_TECHNIEK,
                section: 'subsection-zorg-en-techniek',
                label: 'Zorg techniek'
            },
        ]
    },
];

export default NAVIGATION_CONFIG;