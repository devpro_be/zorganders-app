const PATH_BASE = '/';

const PATH_ARTICLES = '/articles';
const PATH_ARTICLES__CATEGORIES = '/articles/categories';
const PATH_ARTICLES__MOST_READ = '/articles/most_read';

const PATH_BUNDLES = '/bundles';
const PATH_BUNDLES_ACTUAL_CARE = '/bundles/actual-care';
const PATH_REPORTS_ZORG_TECHNIEK = '/bundles/zorg-techniek';
const PATH_REPORTS_UROBEL = '/bundles/urobel';

const PATH_ARTICLE = '/article/:id';
const PATH_CATEGORIES = '/categories';
const PATH_CATEGORY = '/category/:id';
const PATH_SETTINGS = '/settings';
const PATH_REPORTS = '/reports';
const PATH_REPORTS_CATEGORIES = '/reports/categories';
const PATH_REPORTS_MOST_VIEWS = '/reports/most_views';
const PATH_REPORT = '/report/:id';

export {
    PATH_ARTICLE,
    PATH_BASE,
    PATH_BUNDLES,
    PATH_BUNDLES_ACTUAL_CARE,
    PATH_REPORTS_ZORG_TECHNIEK,
    PATH_REPORTS_UROBEL,
    PATH_CATEGORIES,
    PATH_CATEGORY,
    PATH_ARTICLES,
    PATH_ARTICLES__CATEGORIES,
    PATH_ARTICLES__MOST_READ,
    PATH_SETTINGS,
    PATH_REPORTS,
    PATH_REPORTS_CATEGORIES,
    PATH_REPORTS_MOST_VIEWS,
    PATH_REPORT,
}
