import firebaseConfig from './config.local'

export const env = 'production';

export const reduxFirebase = {
    userProfile: 'users', // root that user profiles are written to
    enableLogging: true, // enable/disable Firebase Database Logging
    useFirestoreForProfile: true // Save profile to Firestore instead of Real Time Database
    // updateProfileOnLogin: false // enable/disable updating of profile on login
    // profileDecorator: (userData) => ({ email: userData.email }) // customize format of user profile
};

export const firebase = firebaseConfig;

export const analyticsTrackingId = 'UA-127699708-1';

export default { env, firebase, reduxFirebase, analyticsTrackingId }