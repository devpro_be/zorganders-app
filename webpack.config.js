// webpack.config.js
const Encore = require('@symfony/webpack-encore');
const webpack = require('webpack');
const path = require('path');

Encore
// the project directory where all compiled assets will be stored
    .setOutputPath('public/build/')

    // the public path used by the web server to access the previous directory
    .setPublicPath('/build')

    .addPlugin(new webpack.ProvidePlugin({
        Popper: ['popper.js', 'default'],
    }))

    .createSharedEntry('vendor', [ 'jquery', 'popper.js', 'bootstrap', 'bootstrap/scss/bootstrap.scss' ])

    .enableReactPreset()

    // will create public/build/app.js and public/build/app.css
    .addEntry('app', ['./assets/css/app.scss', './assets/js/app.js'])
    .addEntry('fonts', ['./assets/css/fonts.scss'])
    .addEntry('logo', ['./assets/css/logo.scss'])

    // allow sass/scss files to be processed
    .enableSassLoader()

    // allow legacy applications to use $/jQuery as a global variable
    .autoProvidejQuery()

    .enableSourceMaps(!Encore.isProduction())

    // empty the outputPath dir before each build
    .cleanupOutputBeforeBuild()

    // show OS notifications when builds finish/fail
    .enableBuildNotifications()
;

// export the final configuration
let config = Encore.getWebpackConfig();
// Add correct home directory
config.resolve.alias = {
    '~': path.resolve(__dirname + '/assets/js')
};


module.exports = config;