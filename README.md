ZORGANDERS APP
==============

Introduction
------------

This is the repository containing all the code for the zorganders app.

Before you can start you will need to add the correct credentials for the google firebase services.

- zorganders-google-service-account.json
- assets/js/config.local.js

The PHP environment needs the google service account to be exported to run correctly.

Also don't forget for adding the correct platforms to cordova. 


How it all works
------------

- Scraping happens using symfony cli. 
- Scraped data is scraped from Vimeo, zorganderstv.be and actualcare.be.
- The data is stored on google firebase.
- App is build in react in combination with redux fetching the data from firebase
- App can be developed within the browser (using symfony encore)
- Cordova is used for packaging the app for iOS & Android.


Starting the development environment
------------------------------------

When everything is configured correctly start the dev environment with:

```
yarn dev:start
```

For building / emulating check `package.json` script section.

>
> Happy coding!
>
> 
> Jonas Verhaert - jonas@devpro.be
